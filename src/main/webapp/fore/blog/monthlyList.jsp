<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- About me -->
<div class="tile">
    <h2 class="tile-title">最近几个月博客</h2>
    <div class="listview icon-list">
    <c:forEach items="${mothlyBlogs }" var="month">
        <div class="media">
            <div class="media-body" style="text-align: center;"><a href="/DJSBlog/blog/mothlyList?mothly=${month.mothly}">${month.mothly}(${month.blogCount })</a></div>
        </div>
    </c:forEach>
    </div>
</div>