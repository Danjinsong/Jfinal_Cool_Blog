<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--  Recent Postings -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   <div class="tab-container tile">
     <ul class="nav tab nav-tabs">
     	<c:forEach items="${typeTop }" var="type" varStatus="status">
         <li class="<c:if test='${status.index==0}'>active</c:if>"><a href="#_${type.name}"> ${type.name} </a></li>
     	</c:forEach>
     </ul>
     <div class="tab-content">
     	<c:forEach items="${typeList }" var="type" varStatus="status">
   		 <div class="tab-pane <c:if test='${status.index==0}'>active</c:if>" id="_${typeTop[status.index].name}">
			<c:forEach items="${type }" var="t" varStatus="s">
			<div class="btn-group btn-group-justified m-b-10">
                  <a href="/DJSBlog/blog/typeList/${t.id}" class="btn btn-sm">${t.name }</a>
			</div>                  
     		</c:forEach>
         </div>
     	</c:forEach>
         
     </div>
 </div>
 <hr class="whiter">