<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <!-- Jumbotron -->
 <div class = "col-sm-8">
 <div class="tile">
 	<!-- 前置标题 -->
	<div class="block-area" id="jumbotron" style="background: url(/DJSBlog/${blog.src }) no-repeat 0px 0px;">
	     <div class="jumbotron tile-light">
	          <div class="container">
	               <h1>${blog.title}</h1>
	               <p>作者:<a href="/DJSBlog/heCenter/${blog.userId}">${blog.nickName }</a> &nbsp;&nbsp;&nbsp;&nbsp;『 <fmt:formatDate value="${blog.created }" type="date" pattern="yyyy-MM-dd HH:mm:ss"/>』</p>
	          </div>
	     </div>
	</div>
	<!-- 歌曲  -->
	<c:if test="${!empty blog.mp3}">
		<h4 class="m-l-20" id="preMp3">歌名</h4>
	    <!-- Audio -->
	    <div class="row m-b-20" >
	        <div class="col-md-7">
	            <audio id="audioPlayer" src="${blog.mp3}" type="audio/mp3" style="width:100%;" controls></audio>
	        </div>
	    </div>
	    <script>
	    //<audio id="player" src="../media/AirReview-Landmarks-02-ChasingCorporate.mp3"  >
	    $(document).ready(function(){
	    	$.post('/DJSBlog/back/files/ajaxGetById',{
		    	id:$('#audioPlayer').attr("src")
		    },function(res){
		    	$('#audioPlayer').attr({"src":"/DJSBlog"+res.files.src});
		    	$('#preMp3').html(res.files.name);
		    },'json');
	    });
	    </script>
	</c:if>
	<!-- 正文 -->
	<div class="p-5 m-t-15">
	${blog.text}
	</div>
       <ol class="breadcrumb">
	       	<c:forEach items="${tags}" var="tag">
	           <li class="active"><a href="/DJSBlog/blog/tagList?biaoqian=${tag.id}&&biaoqianName=${tag.name}"><Strong style='font-size: 16px;'>${tag.name }</Strong></a></li>
	       	</c:forEach>
       </ol>
	<div class="p-5 m-t-15">
		   <!-- Breadcrumb -->
	</div>
	<div class="p-5 m-t-15">
     <p>
		<button class='btn  btn-sm' onclick="toAuthor(${blog.id},'${blog.nickName }');"><i class='icon-bubble'></i> <span>私信</span></button>
		<button class='btn  btn-sm' onclick="foucus(${blog.userId},'${blog.nickName }');"><i class='icon-user-2'></i> <span>关注</span></button>
       <a href="javascript:uOl('up',${blog.id});" class="btn btn-sm vot_up_${blog.id}">${blog.vote_up } <span class="icon">&#61845;</span> </a>
       <a href="javascript:uOl('low',${blog.id});" class="btn btn-sm vot_low_${blog.id}">${blog.vote_low } <span class="icon">&#61828; </span> </a>
   	</p>
	</div>
	<!-- 上一篇和下一篇 -->
    <div class="list-group block">
        <a class="list-group-item" href="<c:if test="${empty blogPre}">javascript:void(0)</c:if><c:if test="${!empty blogPre}">/DJSBlog/blog/getDetails/${blogPre.id}</c:if>">上一篇:<c:if test="${empty blogPre}">没有上一篇了 </c:if> ${blogPre.title}</a>
        <a class="list-group-item" href="<c:if test="${empty blogNex}">javascript:void(0)</c:if><c:if test="${!empty blogNex}">/DJSBlog/blog/getDetails/${blogNex.id}</c:if>">下一篇:<c:if test="${empty blogNex}">没有下一篇了 </c:if> ${blogNex.title} </a>
    </div>
	<!-- 评论 -->
      <h4>评论列表   <a  href="javascript:openComment('');" class="btn btn-sm pull-right">我也来说</a></h4>
      <div id="comments">
       ${comments} 
	 </div>
 </div>
 
<!-- Modal 评论 -->	
<div class="modal fade" id="comment" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">说点什么</h4>
            </div>
            <div class="modal-body">
		       <form class="form-validation-3">
	                <label id="toSb">说点什么</label>
		   			<textarea class="form-control validate[required] input-smauto-size m-b-10" id="commentText" placeholder="说点什么..."></textarea>
		       </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm" onclick="sendComment();">提交</button>
                <button type="button" class="btn btn-sm" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="http://www.coding123.net/getip.ashx?js=1"></script>
<input id="currentUserNickName" value="${currentUser.nickName}" type="hidden">
<input id="faId" type="hidden" value=""/>
<script>
ip = ip.split("|")[1];
var blogId = ${blog.id};
var nickName = $('#currentUserNickName').val() == ''?ip:$('#currentUserNickName').val();
function openComment(toSb,faId){
	resetVal();
	if(toSb!='') {
		$('#toSb').html(toSb);
		$('#faId').val(faId);
		$('#commentText').attr({'placeholder':toSb});
	}else{
		$('#toSb').html("说点什么");
		$('#commentText').attr({'placeholder': '说点什么...'});
	}
	$('#comment').modal('show');
}
function sendComment(){
	if($('#toSb').html().indexOf("回复") == 0){
		//回复某条评论
		send({'comment.text':$('#commentText').val(),'blogId':blogId,'faId':$('#faId').val(),'comment.nickName':nickName});
	}else{
		//新建评论
		send({'comment.text':$('#commentText').val(),'comment.blogId':blogId,'blogId':blogId,'comment.nickName':nickName});
		
	}
}
function send(parm){
	$.post('/DJSBlog/comment/addComment',parm,function(res){
		if(res.success){
			$('#comment').modal('hide');
			$('#comments').html(res.comments);
			mes("系统提示","评论成功!");
		}else{
			$('#comment').modal('hide');
			mes("系统提示","评论失败!");
		}
	},'json');
}
function resetVal(){
	$('#commentText').val('');
}
</script>
 