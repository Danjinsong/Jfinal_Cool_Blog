<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h4> ${typeName} </h4>
<c:if test='${!userCenter}'><hr  class="whiter"></c:if>
<div class="<c:if test='${userCenter}'>col-md-12</c:if><c:if test='${!userCenter}'>col-md-8</c:if>" id="blogs">
	${blogs}
	${foot}
</div>
<input type="hidden" id="pageNext" value="2"/>
<input type="hidden" id="qItem" value="${q}"/>
<input type="hidden" id="typeId" value="${typeId }"/>
<input type="hidden" id="mothly" value="${mothly }"/>
<input type="hidden" id="zhuanti" value="${zhuanti }"/>
<input type="hidden" id="heId" value="${heId }"/>
<input type="hidden" id="biaoqian" value="${biaoqian }"/>
<script>
function loadMore(tOq){
	var url='';
	var parm={};
	if(tOq == 'typeId'){
		url = '/DJSBlog/blog/typeList';
		parm = {
				typeId:$('#typeId').val(),
				page:$('#pageNext').val()
			};
	}else if(tOq == 'q'){
		url = '/DJSBlog/blog/getQList';
		parm = {
			q:$('#qItem').val(),
			page:$('#pageNext').val()
		};
	}else if(tOq == 'my'){
		url = '/DJSBlog/userCenter';
		parm = {
			page:$('#pageNext').val()
		};
	}else if(tOq == 'mothly'){
		url = '/DJSBlog/blog/mothlyList';
		parm = {mothly:$('#mothly').val(),page:$('#pageNext').val()};
	}else if(tOq == 'zhuanti'){
		url = '/DJSBlog/blog/topicList';
		parm = {zhuanti:$('#zhuanti').val(),page:$('#pageNext').val()};
	}else if(tOq == 'biaoqian'){
		url = '/DJSBlog/blog/tagList';
		parm = {biaoqian:$('#biaoqian').val(),page:$('#pageNext').val()};
	}else if(tOq == 'he'){
		url = '/DJSBlog/heCenter';
		parm = {
			page:$('#pageNext').val(),
			id:$('#heId').val()
		};
	}
	if(url != ''){
		$.post(url,parm,function(res){
			$("#more").before(res.blogs);
			if(res.blogs == ""){
				$('#more').attr({'href':'javascript:void(0);'});
				$("#more").html("没有更多了");
			}
			$('#pageNext').val(eval(1+"+"+$('#pageNext').val()));
		},'json');
	}
}
</script>