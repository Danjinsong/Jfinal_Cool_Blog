<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  
<!-- Content Boxes -->
<div class="block-area" id="content-boxes">
     <div class="row">
     <c:forEach items="${fourNewBlog}" var="blog">
     	 <div class="col-sm-6 col-md-3">
               <div class="thumbnail tile">
                    <img src="/DJSBlog/${blog.src}" alt="" style='height:180px;width:500%;'>
                    <div class="p-15">
                        <h4><a href='/DJSBlog/blog/getDetails/${blog.id}'>${blog.title}</a></h4>
	                        <p><fmt:formatDate value="${blog.created}"/>&nbsp;&nbsp;&nbsp;<a href='/DJSBlog/heCenter/${blog.userId}'>作者:${blog.nickName }</a></p>
	                  	<p>
	                      <a href='/DJSBlog/blog/getDetails/${blog.id }'>  
	                      	<c:if test="${fn:length(blog.summary)>150}">${fn:substring(blog.summary,0,149)}......</c:if>
	                        <c:if test="${fn:length(blog.summary)<=150}">${blog.summary} </c:if> 
	                       </a>
                        </p>	
                        <p>
                            <a href="javascript:uOl('up',${blog.id});" class="btn btn-sm vot_up_${blog.id}">${blog.vote_up}  <span class="icon">&#61845;</span> </a>
                            <a href="javascript:uOl('low',${blog.id});" class="btn btn-sm vot_low_${blog.id}">${blog.vote_low} <span class="icon">&#61828; </span> </a>
                        </p>
                    </div>
               </div>
          </div>
     </c:forEach>
     </div>
</div>
<hr class="whiter m-t-10" />
<script>
function uOl(state,id){
	if(state == 'up'){
		$.post('/DJSBlog/blog/uOl',{'blogId':id,'uOl':state},function(res){
			if(res.success){
				mes("温馨提示!","点赞成功!");
				$(".vot_up_"+id+" span").remove();
				$(".vot_up_"+id+"").html(eval($('<p>'+$(".vot_up_"+id+"").html()+'</p>').text()+"+"+"1")+"<span class='icon'>&#61845;</span>");
			}else{
				mes("温馨提示!","点赞失败!");
			}
		},'json');
	}
	if(state == 'low'){
		$.post('/DJSBlog/blog/uOl',{'blogId':id,'uOl':state},function(res){
			if(res.success){
				mes("温馨提示!","踩一脚成功!");
				$(".vot_low_"+id+" span").remove();
				$(".vot_low_"+id+"").html(eval($('<p>'+$(".vot_low_"+id+"").html()+'</p>').text()+"+"+"1")+"<span class='icon'>&#61828;</span>");
			}else{
				mes("温馨提示!","踩一脚失败!");
			}
		},'json');
	}
		
}
</script>
 