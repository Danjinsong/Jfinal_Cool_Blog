<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Lightbox -->
<div class="tile" id="picDiv">
   <h2 class="tile-title">照片墙</h2>
   
   <c:forEach items="${photoWall}" var="photo" varStatus="status">
	 <a href="javascript:showBigPic('${status.index}');"  class="pirobox_gall img-popup"  title="${photo.name}">
	       <img src="/DJSBlog${photo.src}"   style='width:100px;height:100px;'/>
	   </a>
   </c:forEach>
</div>
<hr class="whiter">
<div class="modal fade" id="picModal" tabindex="-1" role="dialog" aria-hidden="true" >
     <div class="modal-dialog" id="dig">
         <div class="modal-content">
          	<div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             </div>
             <div class="modal-body" style="text-align: center;">
                 	<img id="picNow"/>
             </div>
             <div class="modal-footer" style="text-align: center;">
                 <button type="button" class="btn btn-sm" onclick="" id="lastPic">上一张</button>
                 <button type="button" class="btn btn-sm" onclick="" id="nextPic">下一张</button>
             </div>
         </div>
     </div>
 </div>
<script>
function showBigPic(index){
	var theImage = new Image();
	theImage.src = $('#picDiv img').eq(index).attr('src');
	var imageWidth = theImage.width<150?150:(theImage.width>960?960:theImage.width);
	var imageHeight = theImage.height+20;
	$('#picNow').attr({'src':$('#picDiv img').eq(index).attr('src')});
	$('#dig').css({width:imageWidth,height:imageHeight});
	if(index == 0)
		$('#lastPic').attr({'onclick':'showBigPic(8)'});
	else
		$('#lastPic').attr({'onclick':'showBigPic('+eval(index+'-1')+')'});
	if(index == 8)
		$('#nextPic').attr({'onclick':'showBigPic(0)'});
	else
		$('#nextPic').attr({'onclick':'showBigPic('+eval(index+'+1')+')'});
	$('#picModal').modal('show');
}

</script>