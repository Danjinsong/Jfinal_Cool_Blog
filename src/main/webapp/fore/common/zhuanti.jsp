<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="tile">
    <h2 class="tile-title">热门专题博客</h2>
    <div class="listview icon-list">
    <c:forEach items="${topicBlogs }" var="topic">
        <div class="media">
            <div class="media-body" style="text-align: center;"><a href="/DJSBlog/blog/topicList?zhuanti=${topic.id}&topicName=${topic.name}">${topic.name}(${topic.blogCount })</a></div>
        </div>
    </c:forEach>
    </div>
</div>