<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--  Recent Postings -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="tile">
	<h2 class="tile-title">友情链接</h2>
	<div class="listview narrow">
	<c:forEach items="${linkList}" var="link">
		<div class="media p-l-5">
			<div class="media-body">
				<small class="text-muted"> ${link.val} </small><br /> <a
					class="t-overflow" href="${link.key}" target="_blank">${link.key}</a>
			</div>
		</div>
	</c:forEach>
	</div>
</div>
