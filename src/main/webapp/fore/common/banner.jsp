<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Image Banner -->
<div id="carousel-example-generic" class="carousel slide">
    <!-- Indicators -->
 <ol class="carousel-indicators">
     <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
     <li data-target="#carousel-example-generic" data-slide-to="1"></li>
     <li data-target="#carousel-example-generic" data-slide-to="2"></li>
 </ol>

 <!-- Wrapper for slides -->
 <div class="carousel-inner">
 	<c:forEach items="${bannerImg}" var="banner" varStatus="status">
	     <div class="item <c:if test='${status.index==0}'>active</c:if> ">
	         <img src="/DJSBlog/${banner.src}" alt="Slide-1" >
	     </div>
 	</c:forEach>
 </div>
 
 <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
        <i class="icon">&#61903;</i>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
        <i class="icon">&#61815;</i>
    </a>
</div>
