<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<header id="header" class="media">
	 <a href="" id="menu-toggle" class="hidden-md hidden-lg"></a> 
     <a class="logo pull-left" href="/DJSBlog/index">DJSBLOG</a>
     <div class="container visible-md  visible-lg" id="collapse">
     	<c:forEach items="${typeTop }" var="type">
	     <a class="logo-mune pull-left" href="/DJSBlog/blog/typeList/${type.id}"> ${type.name} </a>
     	</c:forEach>
     	<c:if test="${empty currentUser}">
         <span class="logo-mune pull-left" >
         	<a href="/DJSBlog/login">登录</a>/
         	<a href="/DJSBlog/login">注册</a>
         </span>
     	</c:if>
     	<c:if test="${!empty currentUser}">
         <span class="logo-mune pull-left" >
         	<a href="/DJSBlog/userCenter"><c:if test="${mesCount > 0}"><i class='n-count'> ${mesCount} </i></c:if>欢迎:${currentUser.nickName}
         	</a>
         	<img class="img-circle img-shadowed" id='userPhoto' src="/DJSBlog/static/userImages/untitled.png" alt="" style="width:18px;height: 18px;">
         </span>
         <span class="logo-mune pull-left" >
         	<a href="/DJSBlog/back/user/logout">退出登录
         	</a>
         </span>
     	</c:if>
	     <div class="media-body" style="display: inline;">
	         <div class="media" id="top-menu">
	             <div id="time" class="pull-right">
	                 <span id="hours"></span>
	                 :
	                 <span id="min"></span>
	                 :
	                 <span id="sec"></span>
	             </div>
	             <div class="media-body">
	                 <input type="text" class="main-search" id="q" placeholder="搜索...." onkeydown="if(event.keyCode==13) searchItems();">
	             </div>
	         </div>
	     </div>
     </div>
 </header>
 <div class="clearfix"></div>
 <script>
 function  searchItems(){
	 window.location.href= "/DJSBlog/blog/getQList?q="+$('#q').val();
 }
 </script>