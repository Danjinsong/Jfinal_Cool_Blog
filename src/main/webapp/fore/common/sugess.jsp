<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="col-md-4">
	 <div class="tile">
    <h2 class="tile-title">留点建议</h2>
    	<div class="p-l-5 p-r-5 p-b-5">
	       <form role="form" class='form-validation-3'>
	        <div class="form-group">
	            <label >你的名字</label>
	            <input type="text" class="form-control input-sm validate[required]" id="sugess_name" placeholder="用户名...">
	        </div>
	        
	        <div class="form-group">
	             <label>你的Email</label>
	            <input type="email" class="form-control input-sm validate[required,custom[email]]" id="sugess_mail" placeholder="邮箱...">
	        </div>
	        
	        <div class="form-group">
	             <label>说点什么</label>
				<textarea class="form-control input-smauto-size m-b-10 validate[required]" id="sugess_text" placeholder="说点什么..."></textarea>
	        </div>
	        <button type="button" class="btn btn-sm m-t-10" onclick="leaveSugess();">提交</button>
	    </form>
    	</div>
	</div>
</div>
<script>
function leaveSugess(){
	var s_name=$('#sugess_name').val();
	var s_mail=$('#sugess_mail').val();
	var s_text=$('#sugess_text').val();
	if(s_name!=''&&s_mail!=''&&s_text!=''){
		$.post('/DJSBlog/back/message/ajaxSave',{
			'message.title':"来自"+s_name+"的建议",
			'message.text':"邮箱:"+s_mail+"<BR>正文:"+s_text,
			'message.sendId':1,
			'message.getId':1
		},function(res){
			if(res.success){
				mes("温馨提示!","您的建议我们已经收到！感谢您对本站的建议!");
			}
		},'json');		
	}
}
</script>
  
