<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Sidbar Widgets -->
<aside id="sidebar" class="hidden-md hidden-lg" style="margin-top:  50px;">
    <!-- Side Menu -->
    <ul class="list-unstyled side-menu">
    	<c:forEach items="${typeTop }" var="type">
    	<li class="active">
            <a  href="/DJSBlog/blog/typeList/${type.id}">
                <span class="menu-item">${type.name}</span>
            </a>
        </li>
     	</c:forEach>
     	<c:if test="${empty currentUser}">
	     	<li>
	        	<a href="/DJSBlog/login">
	                <span class="menu-item">登录</span>
	            </a>
	        </li>
	        <li>
	        	<a href="/DJSBlog/login">
	                <span class="menu-item">注册</span>
	            </a>
	        </li>
     	</c:if>
     	<c:if test="${!empty currentUser}">
     		<li>
	        	<a href="javascript:void(0);">
	                <span class="menu-item">欢迎</span>
	            </a>
	        </li>
	        <li>
	        	<a href="/DJSBlog/userCenter">
	                <span class="menu-item">${currentUser.nickName}<img class="img-circle img-shadowed" id='userPhototiny' src="/DJSBlog/static/userImages/untitled.png" alt="" style="width:22px;height: 22px;"></span>
	            </a>
	        </li>
	        <li>
	       	 	<a href="/DJSBlog/back/user/logout"><span class="menu-item">退出登录</span></a>
	        </li>
     	</c:if>
        
        <li>
           <div class="media-body">
             <input type="text" class="main-search" id="qs" placeholder="搜索...." onkeydown="if(event.keyCode==13) searchItemQs();">
            </div>
        </li>
    </ul>
</aside>
<script>
function searchItemQs(){
	 window.location.href= "/DJSBlog/blog/getQList?q="+$('#qs').val();
}
</script>
