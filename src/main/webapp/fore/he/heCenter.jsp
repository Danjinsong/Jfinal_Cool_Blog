<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div class="tab-container tile col-sm-8" style='margin: 0px;padding: 0px;'>
        <ul class="nav tab nav-tabs">
            <li class="active"><a href="#heCenter">${he.nickName }的空间</a></li>
            <li><a href="#heBlog">${he.nickName }的博客</a></li>
            <li><a href="#heList">${he.nickName }的关注</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="heCenter">
				<h4>${he.nickName }的个人简介:</h4>
				<img src="/DJSBlog${heFiles.src}" class="img-rounded m-r-10 m-b-10"/><br>
				<H6> 个性签名: ${he.sign} </H6>
            	<p>
            	${he.text}
            	</p>
            </div>
            <div class="tab-pane" id="heBlog">
            	<jsp:include page="/fore/blog/typeList.jsp"></jsp:include>
            </div>
            <div class="tab-pane" id="heList">
            	<jsp:include page="/fore/he/foucusList.jsp"></jsp:include>
            </div>
        </div>
</div>