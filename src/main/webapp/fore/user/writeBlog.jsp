<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="form-group m-b-15">
    <label>博客标题</label>
    <input type="text" class="form-control input-sm" id="b_title" placeholder="在此填写标题...">
</div>
<div class="form-group m-b-15">
   <label>博客大图</label>
	 <div class="fileupload fileupload-new" data-provides="fileupload">
	     <div class="fileupload-preview thumbnail form-control" style="width: 57.6%;height:17.1%;"></div>
	     <div>
	         <span class="btn btn-file btn-alt btn-sm">
	             <span class="fileupload-new">选择图片</span>
	             <span class="fileupload-exists">更换</span>
	             <input type="file" id="blog_image" name="image" onchange="javascript:uploadBlogImage();" />
	             <input type="hidden" class="filesId" value="" id="blogFiles"/>
	         </span>
	         <a href="#" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
	     </div>
	 </div>
</div>
<div class="form-group m-b-15">
   <label>博客分类</label>
    <select  id="select_type" class="select" multiple>
	 </select>
</div>
<div class="form-group  m-b-15">
    <label>专题(是否为某一个专题写的博客)</label>
    <div class="">
     <select class="select" id="select_zhuanti">
     	<option value="0" selected="selected">无</option>
     </select>
   </div>
</div>
<div class="form-group m-b-15" id="blog_editor">
   <label>博客内容</label>
   <div class="wysiwye-editor"></div>
</div>
<div class="form-group  m-b-15">
    <label>文章标签(以空格分割)</label>
    <input type="text" class="form-control input-sm" id="blog_biaoqian" placeholder="标签.." value="">
</div>
   
<div class="form-group m-b-15">
      <label id="preMp3">MP3</label>
      <div class="fileupload fileupload-new" data-provides="fileupload">
               <span class="btn btn-file btn-sm btn-alt">
                   <span class="fileupload-new">选择mp3文件</span>
                   <span class="fileupload-exists">更换mp3文件</span>
                   <input type="file" name="mp3" id="mp3" onchange="uploadBlog_MP3();return false;"/>
 				<input type="hidden" class="mp3Id" value="" id="mp3_Id"/>
   	</span>
   <a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">
       <i class="fa fa-times"></i>
   </a>
</div>
</div>
<button type="button" class="btn btn-sm m-t-10" onclick="saveBlog();return false;">发布</button>
<script>
$(function(){
	loadType();
});
function uploadBlog_MP3(){
	uploadMP3('mp3',$('#mp3').val(),"/DJSBlog/back/files/ajaxSaveSong");
}
function saveBlog(){
	var blogText_notTag=$('<p>'+$("#blog_editor .note-editable").eq(0).html()+'</p>').text();
	var blogText=$("#blog_editor .note-editable").eq(0).html();
	var type="";
	for(var i=0;i<$('#select_type').val().length;i++){
		type+=$('#select_type').val()[i];
		if(i!=$('#select_type').val().length-1)
			type+=",";
	}
	if(blogText_notTag!=null&&blogText_notTag!=''&&$('#blog_up_title').val()!=''){
		$.post('/DJSBlog/back/blog/ajaxSave',{
			'blog.title':$('#b_title').val(),
			'blog.text':blogText,
			'blog.summary':blogText_notTag,
			'blog.photo':$('#blogFiles').val(),
			'blog.mp3':$('#mp3_Id').val(),
			'blog.userId':${currentUser.id},
			'type':type,
			'biaoqian':$('#blog_biaoqian').val(),
			'zhuanti':$('#select_zhuanti').val()
		},function(res){
			if(res.success){
				mes("系统提示","操作成功");
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}else{
		mes("系统提示","您还没有写博客");
	}
}
function loadType(){
	$.post('${pageContext.request.contextPath}/back/type/ajaxFindType',{},function(res){
		var str = "";
		for(var i =0;i<res.typeList.length;i++){
			str+="<option value='"+res.typeList[i].id+"'>"+res.typeList[i].name+"</option>";
		}
		$('#select_type').html(str);
		$('.select').selectpicker('refresh');
	},'json');
	$.post('${pageContext.request.contextPath}/back/type/ajaxFindzhuanti',{},function(res){
		var str = "";
		str+="<option value='0' selected='selected'>无</option>";
		for(var i =0;i<res.ztList.length;i++){
			str+="<option value='"+res.ztList[i].id+"'>"+res.ztList[i].name+"</option>";
		}
		$('#select_zhuanti').html(str);
		$('.select').selectpicker('refresh');
	},'json');
}
function uploadBlogImage(){
	upload('blog_image',$('#blog_image').val(),$('#blogFiles').val(),'',"/DJSBlog/back/files/ajaxSave?width=1920&&height=570");
}
</script>