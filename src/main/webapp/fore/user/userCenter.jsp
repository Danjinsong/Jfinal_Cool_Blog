<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<div class="tab-container tile col-sm-8" style='margin: 0px;padding: 0px;'>
        <ul class="nav tab nav-tabs">
            <li><a href="#userCenter">用户中心</a></li>
            <li><a href="#myBlog">我的博客</a></li>
            <li class="active"><a href="#writeBlog">写博客</a></li>
            <li><a href="#infoCenter">我的信息</a></li>
            <li><a href="#foucusList">我的关注</a></li>
            <li><a href="#setting">设置</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane" id="userCenter">
				<h4>${currentUser.nickName }的个人简介:</h4>
				<img src="/DJSBlog${userFile.src}" class="img-rounded m-r-10 m-b-10"/><br>
				<H6> 个性签名: ${currentUser.sign} </H6>
            	<p>
            	${currentUser.text}
            	</p>
            </div>
            <div class="tab-pane" id="myBlog">
            	<jsp:include page="/fore/blog/typeList.jsp"></jsp:include>
            </div>
            <div class="tab-pane active" id="writeBlog">
            	<jsp:include page="/fore/user/writeBlog.jsp"></jsp:include>
            </div>
            <div class="tab-pane" id="infoCenter">
            	<jsp:include page="/fore/user/mailBox.jsp"></jsp:include>
            </div>
            <div class="tab-pane" id="foucusList">
            	<jsp:include page="/fore/user/foucusList.jsp"></jsp:include>
            </div>
            <div class="tab-pane" id="setting">
            	<jsp:include page="/fore/user/userInfo.jsp"></jsp:include>
            </div>
        </div>
</div>