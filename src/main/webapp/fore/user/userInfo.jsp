<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<form role="form" class="form-validation-3">
    <div class="form-group">
        <label>用户名</label>
        <input type="text" id="u_userName" class="form-control input-sm "  placeholder="用户名..."  disabled="disabled" value="${currentUser.userName}">
    </div>
    <div class="form-group">
        <label>密码</label>
        <input type="password" id="u_password" class="form-control input-sm validate[required]" placeholder="密码..." value="${currentUser.password}">
    </div>
    <div class="form-group">
        <label>确认密码</label>
        <input type="password" id="u_eqPassword" class="form-control input-sm validate[required,equals[u_password]]" placeholder="确认密码..." value="${currentUser.password}">
    </div>
    <div class="form-group">
        <label>邮箱</label>
        <input type="email" id="u_email" class="form-control input-sm validate[required,custom[email]]" placeholder="邮箱..." value="${currentUser.email}">
    </div>
    <div class="form-group">di
        <label>昵称</label>
        <input type="text" id="u_nickName" class="form-control input-sm validate[required]" placeholder="昵称..." value="${currentUser.nickName}">
    </div>
    <div class="form-group">
        <label>真名</label>
        <input type="text" id="u_realName" class="form-control input-sm validate[required]" placeholder="真实姓名..." value="${currentUser.realName}">
    </div>
    <div class="form-group">
        <label>QQ</label>
        <input type="text" id="u_qq" class="form-control input-sm validate[required,custom[number]]" placeholder="qq..." value="${currentUser.qq}">
    </div>
    <div class="form-group">
        <label>手机</label>
        <input type="text" id="u_phone" class="form-control input-sm validate[required,custom[number]]" placeholder="手机..." value="${currentUser.phone}">
    </div>
    <div class="form-group">
        <label>个性签名</label>
        <input type="text" id="u_sign" class="form-control input-sm validate[required]" placeholder="手机..." value="${currentUser.sign}">
    </div>
    
    <div class="form-group">
        <label>头像</label>
        <div class="fileupload fileupload-new" data-provides="fileupload"  >
	         <div class="fileupload-preview thumbnail form-control" id="userThm" style='width:60px;height:60px;'></div>
	         <div>
		        <span class="btn btn-file btn-alt btn-sm">
		            <span class="fileupload-new">选择图片</span>
		            <span class="fileupload-exists">更换</span>
		            <input type="file" id="image" name="image" onchange="javascript:uploadImage();"/>
		      		<input type="hidden" class="filesId" value="" id="user_Files"/>
		        </span>
		        <a href="javascript:$('#image').val('');return false;" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
	   		 </div>
	   	</div>
 	</div>    
 	
    <div class="form-group m-b-15" id="userText_editor">
	   <label>个人介绍</label>
	   <div class="wysiwye-editor"></div>
	</div>
	<div id="userText" style='display: none;'>${currentUser.text}</div>
    <button type="button" class="btn btn-sm m-t-10" onclick="saveUser();return false;">确定</button>
</form>
<script>
$(function (){
	$('#user_Files').val(${currentUser.photo});
	$('#userText_editor .overflow').eq(0).html($('#userText').html());
});
function uploadImage(){
	upload('image',$('#image').val(),$('.filesId').val(),'user_Files',"/DJSBlog/back/files/ajaxSave?width=60&&height=60");
}
function saveUser(){
	var userText=$('#userText_editor .overflow').eq(0).html();
	if($('#u_password').val() == $('#u_eqPassword').val()){
		if($('#u_userName').val()!=''&&$('#u_nickName').val()!=''&&$('#u_email').val()!=''){
			$.post('/DJSBlog/back/user/ajaxSave',{
				'user.userName':$('#u_userName').val(),
				'user.nickName':$('#u_nickName').val(),
				'user.realName':$('#u_realName').val(),
				'user.password':$('#u_password').val(),
				'user.qq':$('#u_qq').val(),
				'user.sign':$('#u_sign').val(),
				'user.id':${currentUser.id},
				'user.email':$('#u_email').val(),
				'user.photo':$('#user_Files').val(),
				'user.phone':$('#u_phone').val(),
				'user.text':userText
			},function(res){
				if(res.success){
					mes("温馨提示!","操作成功!");
				}
				else
					mes("温馨提示!","操作失败");
			},'json');
		}else{
			mes("温馨提示!","请补全个人信息!");
		}
	}else{
		mes("温馨提示!","两次输入的密码不一样!");
	}
}

</script>
