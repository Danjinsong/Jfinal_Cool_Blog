<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
  <!-- 后台CSS -->
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/bootstrap.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/animate.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/form.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/calendar.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/style.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/icons.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/generics.css" rel="stylesheet"> 
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/css/media-player.css" rel="stylesheet">
   <!-- 后台js Libraries -->
   <!-- jQuery -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/jquery.min.js"></script> <!-- jQuery Library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/jquery-ui-1.8.2.custom.min.js"></script> <!-- jQuery UI -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

   <!-- Bootstrap -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/bootstrap.min.js"></script>
     <!-- Charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.orderBar.js"></script> <!-- Flot Bar chart -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts/jquery.flot.pie.min.js"></script> <!-- Flot Pie chart -->

   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/charts.js"></script> <!-- All the above chart related functions -->

   <!-- Map -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/maps/jvectormap.min.js"></script> <!--   jVectorMap main library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/maps/world.js"></script> <!-- USA Map for jVectorMap -->

   <!--  Form Related -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/jquery.validationEngine-zh_CN.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/select.min.js"></script> <!-- Custom Select -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/chosen.min.js"></script> <!-- Custom Multi Select -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/datetimepicker.min.js"></script> <!-- Date & Time Picker -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/colorpicker.min.js"></script> <!-- Color Picker -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/icheck.js"></script> <!-- Custom Checkbox + Radio -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/autosize.min.js"></script> <!-- Textare autosize -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/toggler.min.js"></script> <!-- Toggler -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/input-mask.min.js"></script> <!-- Input Mask -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/spinner.min.js"></script> <!-- Spinner -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/slider.min.js"></script> <!-- Input Slider -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/fileupload.min.js"></script> <!-- File Upload -->
   <!-- Text Editor -->
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/static/js/editor2.min.js"></script> <!-- WYSIWYG Editor -->
 
   <!-- UX -->
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/static/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

   <!-- Other -->
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/static/js/calendar.min.js"></script> <!-- Calendar -->
   <!-- Media -->
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/static/js/media-player.min.js"></script> <!-- Video Player -->

   <!-- All JS functions -->
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/static/js/functions.js"></script>
   <script src="http://ofhnialdm.bkt.clouddn.com//DJSBlog/back/js/ajaxfileupload.js"></script>