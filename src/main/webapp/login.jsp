<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <meta name="format-detection" content="telephone=no">
        <meta charset="UTF-8">

        <meta name="description" content="Violate Responsive Admin Template">
        <meta name="keywords" content="Super Admin, Admin, Template, Bootstrap">

        <title>DJSBLOG LOGIN</title>
        <jsp:include page="/static/linkDemo/loginCssAndJs.jsp"></jsp:include>  
    </head>
    <body id="skin-blur-lights">
        <section id="login">
            <header>
                <h1>欢迎登录</h1>
                <p>劲松博客系统诚挚欢迎您登录本系统</p>
            </header>
            <div class="clearfix"></div>
            <!-- Login -->
            <form class="box tile animated active form-validation-3" id="box-login" action="${pageContext.request.contextPath }/back/user/login" method="post">
                <h2 class="m-t-0 m-b-15">登录</h2>
                <input type="text" class="login-control validate[required] m-b-10" value="${user.userName }" name="user.userName" placeholder="用户名">
                <input type="password" class="login-control validate[required]" value="${user.password }" name="user.password" placeholder="密码">
                <div class="checkbox m-b-20">
                    <label>
                        <input type="checkbox" name="remeber" value="true" class='pull-left list-parent-check'>
                        	记住我
                    </label>
                </div>
                <button class="btn btn-sm m-r-5" type="submit">登录</button>
                
                <small>
                    <a class="box-switcher" data-switch="box-register" href="">还没有账户呢?</a> 或者
                    <a class="box-switcher" data-switch="box-reset" href="">忘记密码?</a><strong style="color: red;"> ${error} </strong>
                </small>
            </form>
            <!-- Register -->
            <form class="box animated tile form-validation-3"  id="box-register" action="/DJSBlog/back/user/signUp" method="post">
                <h2 class="m-t-0 m-b-15">注册</h2>
                <input type="text" class="input-sm form-control validate[required,ajax[ajaxRegisterCheckUserName]] login-control m-b-10" placeholder="用户名" name="user.userName">
                <input type="text" class="input-sm validate[required,ajax[ajaxRegisterCheckNickName]] form-control login-control m-b-10" placeholder="昵称" name="user.nickName">
                <input type="email" class="login-control m-b-10 form-control input-sm validate[required,custom[email]]" placeholder="邮箱" name="user.email"> 
                <input type="password" class="input-sm validate[required] form-control login-control m-b-10"  id="password" placeholder="密码" name="user.password">
                <input type="password" class="input-sm validate[required,equals[password]] form-control login-control m-b-10" placeholder="确认密码">
 				<input class="btn btn-sm m-r-5" type="submit" value="注册">
        		<button class="btn btn-sm validation-clear" onclick="javascript:$(':input','#box-register').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');  ">清空</button>

                <small><a class="box-switcher" data-switch="box-login" href="">已经有账户了?</a></small>
            </form>
            <!-- Forgot Password -->
            <form class="box animated tile" id="box-reset" method="post">
                <h2 class="m-t-0 m-b-15">找回密码</h2>
                <p>填写您的注册邮箱我们将发送邮件到您的邮箱协助您找回密码</p>
                <input type="email" class="login-control m-b-20" placeholder="邮箱">    
                <button class="btn btn-sm m-r-5">找回密码</button>
                <small><a class="box-switcher" data-switch="box-login" href="">已经有账户了?</a></small>
            </form>
        </section>                      
    </body>
    
</html>

