<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
   <head>
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
       <meta name="format-detection" content="telephone=no">
       <meta name="description" content="Violate Responsive Admin Template">
       <meta name="keywords" content="Super Admin, Admin, Template, Bootstrap">
       <title>WELCOME TO DJSBLOG ADMIN MANAGE</title>
	   <jsp:include page="/back/common/jsAndCss.jsp"></jsp:include>
   </head>
  <body id="<c:if test="${!empty skin}">${skin}</c:if><c:if test="${empty skin}">skin-blur-lights</c:if>">
  		<!-- 顶部导航 -->
  		<jsp:include page="/back/common/header.jsp"></jsp:include>
  		<section id="main" class="p-relative" role="main">
  			<!-- aside -->
  			<jsp:include page="/back/common/aside.jsp"></jsp:include>
  				<section id="content" class="container">
		  			<!-- content -->
					<jsp:include page="${mainPage}"></jsp:include>
		  		</section>
  		</section>
  </body>
</html>
<div class="modal fade" id="mes" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="title"></h4>
             </div>
             <div class="modal-body">
                 <p id="body"></p>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-sm" data-dismiss="modal">关闭</button>
             </div>
         </div>
     </div>
 </div>
<script>
function mes(title,body){
	$('#title').html(title);
	$('#body').html(body);
	$('#mes').modal('show');
}
var settings =  '<a id="settings" href="#changeSkin" data-toggle="modal">' +
'<i class="fa fa-gear"></i> 戳这里' +
'</a>' +   
'<div class="modal fade" id="changeSkin" tabindex="-1" role="dialog" aria-hidden="true">' +
'<div class="modal-dialog modal-lg">' +
    '<div class="modal-content">' +
	'<div class="modal-header">' +
	    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
	    '<h4 class="modal-title">换个皮肤</h4>' +
	'</div>' +
	'<div class="modal-body">' +
	    '<div class="row template-skins">' +
		'<a data-skin="skin-blur-violate" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-violate.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-lights" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-lights.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-city" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-city.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-greenish" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-greenish.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-night" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-night.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-blue" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-blue.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-sunny" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-sunny.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-cloth" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-cloth.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-tectile" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-tectile.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-chrome" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-chrome.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-ocean" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-ocean.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-sunset" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-sunset.jpg" alt="">' +
		'</a>' +
		'<a data-skin="skin-blur-yellow" class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-yellow.jpg" alt="">' +
		'</a>' +
		'<a  data-skin="skin-blur-kiwi"class="col-sm-2 col-xs-4" href="">' +
		    '<img src="${pageContext.request.contextPath }/static/img/skin-kiwi.jpg" alt="">' +
		'</a>' +
	    '</div>' +
	'</div>' +
    '</div>' +
'</div>' +
'</div>';
</script>
