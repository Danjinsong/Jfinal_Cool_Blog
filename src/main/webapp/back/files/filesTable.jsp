<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-12">
<!-- Photos -->
	<div class="tile">
	    <h2 class="tile-title">附件列表</h2>
	    <div class="p-5 photos">
	    	<c:forEach items="${filesList }" var="photo">
	    	 <div class="col-xs-1">
		    	<div class="tile p-5 m-b-10">
			    	 <a title="${photo.name}" href="javascript:void(0)">
			            <img src="/DJSBlog${photo.src}"  alt="" style="width:150px;height:150px;">
		    	    </a>
	    	    </div>
    	    </div>
	    	</c:forEach>
	        <div class="clearfix"></div>
	    </div>
	</div>
</div>