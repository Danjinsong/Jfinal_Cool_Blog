<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="message-list list-container" id="inner">

</div>

<!-- Compose -->
<div class="modal fade" id="compose-message" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
           	<form class="form-validation-3">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">新信息</h4>
            </div>
	            <div class="modal-header p-0">
	                <input type="text" id="mes_nickName" class="form-control input-sm validate[required,ajax[ajaxRegisterMessageNickName]] input-transparent" placeholder="收信人昵称...">
	            </div>
            <div class="modal-header p-0">
                <input type="text" id="mes_title" class="form-control input-sm validate[required] input-transparent" placeholder="主题(标题)...">
            </div>
            <div class="p-relative">
                <div class="message-options">
                    <img src="/DJSBlog/static/img/icon/tile-actions.png" alt="">
                </div>
                <textarea class="message-editor" placeholder="说点什么..."></textarea>
            </div>
            <div class="modal-footer m-0">
                <button class="btn" onclick="sendNewItem();return false;">发送</button>
                <button class="btn"  data-dismiss="modal">取消</button>
            </div>
           	</form>
        </div>
    </div>
</div>
<!-- Compose -->
<div class="modal fade" id="messageModal"  data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">查看信息</h4>
            </div>
            <div class="modal-header">
            	<h4 id="mes_read_title"></h4>
            </div>
            <div class="modal-header">
            	<h6 id="mes_date"></h6>
            </div>
            <div class="modal-header">
            	<p id="mes_text"></p>
            </div>
            <div class="modal-header p-0">
            	<label>回复:</label>
            	<input type="hidden" id="mes_re_id" value="">
                <input type="text" id='mes_re_nickName' class="form-control input-sm input-transparent" placeholder="收信人..." disabled="disabled">
            </div>
            <div class="modal-header p-0">
            	<label>标题:</label>
                <input type="text" id='mes_re_title' class="form-control input-sm input-transparent" placeholder="主题(标题)...">
            </div>
            <div class="p-relative">
                <div class="message-options">
                    <img src="/DJSBlog/static/img/icon/tile-actions.png" alt="">
                </div>
                <textarea class="message-editor" placeholder="说点什么..."></textarea>
            </div>
            <div class="modal-footer m-0">
                <button class="btn" onclick="sendItem();return false;">发送</button>
                <button class="btn" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<!-- Compose -->
<div class="modal fade" id="mes_send_Modal"  data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">查看信息</h4>
            </div>
            <div class="modal-header">
            	<h4 id="mes_get_title"></h4>
            </div>
            <div class="modal-header">
            	<h6 id="mes_get_date"></h6>
            </div>
            <div class="modal-header">
            	<p id="mes_get_text"></p>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="sog" value='get'></input>
<input type="hidden" id="show_state" value='false'></input>
<input type="hidden" id="pageNow" value='1'></input>
 <script type="text/javascript">
 function resetVal(){
 	$('#mes_nickName').val('');
 	$('#mes_title').val('');
	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
	$(".note-editable").html('<p></p>');
	}
 function write(){
	 resetVal();
	 $('#compose-message').modal('show');
 }
 function sendNewItem(){
	 if($('#mes_nickName').val()!=''&&$('#mes_title').val()&&$('<p>'+$(".note-editable").eq(0).html()+'</p>').text()!=''){
		 $.post('/DJSBlog/back/message/ajaxSave',{
			 'nickName':$('#mes_nickName').val(),
			 'message.title':$('#mes_title').val(),
			 'message.text':$(".note-editable").eq(0).html(),
			 'message.sendId':${currentUser.id}
		 },function(res){
			 if(res.success){
				 $('#compose-message').modal('hide');
				 toMessage("send");
				 mes("系统提示","发送成功");
			 }else{
				 mes("系统提示","发送失败");
			 }
		 },'json');
	 }else{
		 mes("系统提示","输入不可以为空");
	 }
	
 }
 function sendItem(){
	 $.post('/DJSBlog/back/message/ajaxSave',{
		 'message.getId':$('#mes_re_id').val(),
		 'message.title':$('#mes_re_title').val()==''?('回复:'+$('#mes_re_nickName').val()):$('#mes_re_title').val(),
		 'message.text':$(".note-editable").eq(1).html(),
		 'message.sendId':${currentUser.id}
	 },function(res){
		 if(res.success){
			 $('#messageModal').modal('hide');
			 toMessage("send");
			 mes("系统提示","发送成功");
		 }else{
			 mes("系统提示","发送失败");
		 }
	 },'json');
 }
 function getChecked(){
		var checked = $("input:checked");
		var val=[];
		for(var i=0;i<checked.length;i++){
			if(checked.eq(i).val()!='on'&&checked.eq(i).val()!='false'&&checked.eq(i).val()!='true')
			val.push(checked.eq(i).val());
		}
		return val;
	}
function ajaxDeleteOrComeback(del){
	var val=getChecked();
	if(val.length==0){
		mes("温馨提示!","请选择元素进行操作!");
	}else{
		var str="";
		for(var i=0;i<val.length;i++){
				str+=val[i];
			if(i!=val.length-1)
				str+=",";
		}
		$.post('/DJSBlog/back/message/ajaxDeleteOrComeback',{ids:str,del:del},function(res){
			if(res.success){
				mes("系统提示","操作成功");
				loadItems({'s_message.get_del':$('#show_state').val(),page:$('#pageNow').val(),'s_message.getId':${currentUser.id}});				
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}
}
function edit(id){
	$.post('/DJSBlog/back/message/ajaxGetById',{id:id,read:false},function(res){
		 $('#mes_get_title').html(res.message.title);
		 $('#mes_get_text').html(res.message.text);
		 $('#mes_get_date').html(res.message.created);
		 $('#mes_send_Modal').modal('show');
	 },'json');
}
 function read(id){
	 $.post('/DJSBlog/back/message/ajaxGetById',{id:id},function(res){
		 $('#mes_read_title').html(res.message.title);
		 $('#mes_text').html(res.message.text);
		 $('#mes_date').html(res.message.created);
		 $.post('/DJSBlog/back/user/ajaxGetById',{id:res.message.sendId},function(data){
		 	$('#mes_re_id').val(data.user.id);
		 	$('#mes_re_nickName').val(data.user.nickName);
		 },'json');
		 $('#messageModal').modal('show');
	 },'json');
 }
 function loadItems(parm){
	 $.post('/DJSBlog/back/message/ajaxList',parm,function(res){
		 $('#inner').html(res.thead);
		 $('#thead').after(res.tbody);
		 $('#tfoot').html(res.tfoot);
		 loadchecks();
	 },'json');
 }
 function toMessage(sog){
	 if("get"==sog){
	 	loadItems({'s_message.get_del':false,page:1,'s_message.getId':${currentUser.id}});
	 	$('#sog').val('get');
	 	$('#show_state').val('false');
	 	$('#pathNow').html('收信箱');
	 }
	 else if("send"==sog){
		loadItems({'s_message.send_del':false,page:1,'s_message.sendId':${currentUser.id}});
		$('#sog').val('send');
		$('#show_state').val('false');
		$('#pathNow').html('发信箱');
	 }else{
		 $('#show_state').val('true');
		 $('#sog').val('rubbish'); 
		 loadItems({
			 's_message.getId':${currentUser.id},
			 's_message.get_del':true,
			 page:1
		 });
		 $('#pathNow').html('垃圾箱');
	 }
	$('#pageNow').val(1);
 }
 function toPage(page){
	 if("get"==$('#sog').val()){
		 loadItems({'s_message.get_del':$('#show_state').val(),page:page,'s_message.getId':${currentUser.id}});
	 }else{
		 loadItems({'s_message.send_del':false,page:page,'s_message.sendId':${currentUser.id}}); 
	 }
	 $('#pageNow').val(page);
 }
 $(document).ready(function(){ 
	 loadItems({'s_message.get_del':false,page:1,'s_message.getId':${currentUser.id}});
     //Editor
     $('.message-editor').summernote({
         toolbar: [
           //['style', ['style']], // no style button
           ['style', ['bold', 'italic', 'underline', 'clear']],
           ['fontsize', ['fontsize']],
           ['color', ['color']],
           ['para', ['ul', 'ol', 'paragraph']],
           //['height', ['height']],
           ['insert', ['picture', 'link']] // no insert buttons
           //['table', ['table']], // no table button
           //['help', ['help']] //no help button
         ],
         height: 200,
         resizable: false
     });
     $('.message-options').click(function(){
         $(this).closest('.modal').find('.note-toolbar').toggle(); 
     });  
 });
</script>