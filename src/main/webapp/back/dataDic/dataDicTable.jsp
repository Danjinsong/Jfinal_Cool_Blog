<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Table Hover -->
<div class="col-md-6 ">
     <form class="form-inline" role="form" action="#">
       	<span style="float: right; font-size: 20px;" >共有<font color="red" id="total" >0</font>条数据</span>
       <div class="form-group">
           <input type="text" class="form-control input-sm" id="s_key" placeholder="输入KEY关键字..." onkeydown="if(event.keyCode==13) searchItems();">
       </div>
       <div class="form-group">
           <input type="text" class="form-control input-sm" id="s_val" placeholder="输入VALUE关键字..." onkeydown="if(event.keyCode==13) searchItems();">
		   <button class="btn tooltips" data-placement="top"  onclick="searchItems();"><i class="sa-list-search"></i></button>
       </div>
       <div class="btn-group">
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="修改" onclick="updateItem();return false;"><i class="sa-list-move"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="删除" onclick="deleteOrComebackItem('true');return false;"><i class="sa-list-delete"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="恢复" onclick="deleteOrComebackItem('false');return false;"><i class="sa-list-forwad"></i></button>
  		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='已删除'onclick="showDel('true');return false;"><i class='sa-list-archive'></i></button>
		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='未删除'onclick="showDel('false');return false;"><i class='sa-list-spam'></i></button>
 		 <input type="hidden" id="show_state" value="false"/>
  	  </div>
    </form>
    <div class="table-responsive overflow">
        <table class="table table-bordered table-hover tile message-list list-container">
            <thead id="thead">
            </thead>
            <tbody id="tbody">
            </tbody>
            <tfoot id="tfoot">
            </tfoot>
        </table>
    </div>
</div>
<div class="col-md-6 ">
	 <div class="tile" id="basic" style="margin-top: 60px;">
	    <h3 class="tile-title">添加数据</h3>
	    <div class="tile p-15">
	        <form role="form" >
	            <div class="form-group">
	                <input type="text" class="form-control input-sm" id="key" placeholder="KEY..(建议输入大写字母下划线模式)">
	            </div>
	            <div class="form-group">
	                <input type="text" class="form-control input-sm" id="val" placeholder="VALUE..">
	            </div>
	            <button type="button" class="btn btn-sm m-t-10" onclick="save_Item();">提交</button>
	            <button type="button" class="btn btn-sm m-t-10" onclick="resetVal();">重置</button>
	        </form>
	    </div>
	</div>
	 <div class="tile" id="basic" style="margin-top: 60px;">
	    <h3 class="tile-title">添加友情链接</h3>
	    <div class="tile p-15">
	        <form role="form" class="form-validation-3">
	            <div class="form-group">
	                <input type="text" class="form-control validate[required,custom[url]] input-sm" id="link_key" placeholder="友情链接URL">
	            </div>
	            <div class="form-group">
	                <input type="text" class="form-control validate[required] input-sm" id="link_val" placeholder="友情链接名称">
	            </div>
	            <button type="button" class="btn btn-sm m-t-10" onclick="save_link_Item();">提交</button>
	            <button type="button" class="btn btn-sm m-t-10" onclick="resetVal();">重置</button>
	        </form>
	    </div>
	</div>
</div>
<div class="modal fade" id="datadicModal" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="datadic_title"></h4>
             </div>
             <div class="modal-body">
           	    <div class="tile p-15">
			        <form role="form" >
			            <div class="form-group">
			                <input type="hidden" id='datadic_id' value="">
			                <label>key:</label>
			                <input type="text" class="form-control input-sm" id="datadic_key" placeholder="输入一个KEY" value="">
			            </div>
			            <div class="form-group">
			                <label>val:</label>
			                <input type="text" class="form-control input-sm" id="datadic_val" placeholder="输入一个VALUE" value="">
			            </div>
			        </form>
			    </div>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-sm" onclick="save_datadic_Item()" >提交</button>
                 <button type="button" class="btn btn-sm" data-dismiss="modal">取消</button>
             </div>
         </div>
     </div>
 </div>
<input type="hidden" id="pageNow" value="1"/>
<script>
function showDel(del){
	$('#show_state').val(del);
	loadItems({'s_datadic.del':$('#show_state').val(),
			's_datadic.key':$('#s_key').val(),
			's_datadic.val':$('#s_val').val(),
			page:1,
			});
	$('#pageNow').val(1);
}function getChecked(){
	var checked = $("input:checked");
	var val=[];
	for(var i=0;i<checked.length;i++){
		if(checked.eq(i).val()!='on'&&checked.eq(i).val()!='false'&&checked.eq(i).val()!='true')
		val.push(checked.eq(i).val());
	}
	return val;
}
function deleteOrComebackItem(del){
	var val=getChecked();
	if(val.length==0){
		mes("温馨提示!","请选择元素进行操作!");
	}else{
		var str="";
		for(var i=0;i<val.length;i++){
				str+=val[i];
			if(i!=val.length-1)
				str+=",";
		}
		$.post('/DJSBlog/back/datadic/ajaxDeleteOrComeback',{ids:str,del:del},function(res){
			if(res.success){
				mes("系统提示","操作成功");
				loadItems({'s_datadic.del':$('#show_state').val(),page:$('#pageNow').val()});				
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}
}
function searchItems(){
	loadItems({'page':1,'s_datadic.del':$('#show_state').val(),'s_datadic.key':$('#s_key').val(),'s_datadic.val':$('#s_val').val()});
}
function resetVal(){
	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
}
function save_link_Item(){
	saveItem({'datadic.key':$('#link_key').val(),'datadic.val':$('#link_val').val()});
}
function save_Item(){
	saveItem({'datadic.key':$('#key').val(),'datadic.val':$('#val').val()});
}
function save_datadic_Item(){
	saveItem({'datadic.key':$('#datadic_key').val(),'datadic.val':$('#datadic_val').val(),'datadic.id':$('#datadic_id').val()});
}
function updateItem(){
	var val=getChecked();
	if(val.length!=1){
		mes("温馨提示!","请选择一行元素进行修改!");
	}else{
		$.post('/DJSBlog/back/datadic/ajaxGetById',{id:val[0]},function(res){
				$('#datadic_title').html("修改类别");
				$('#datadic_key').val(res.datadic.key);
				$('#datadic_val').val(res.datadic.val);
				$('#datadic_id').val(res.datadic.id);
				$('#datadicModal').modal('show');
		},'json');
	}
}
function saveItem(parm){
	$.post('/DJSBlog/back/datadic/ajaxSave',parm,function(res){
		if(res.success){
			$('#datadicModal').modal('hide');
			mes("温馨提示!","操作成功");
			loadItems({'s_datadic.del':false,page:$('#pageNow').val()});	
		}
		else
			mes("温馨提示!","操作失败,KEY已存在");
	},'json');
}
function toPage(page){
	loadItems({'s_datadic.del':$('#show_state').val(),
			's_datadic.key':$('#s_key').val(),
			's_datadic.val':$('#s_val').val(),
			page:page
			});
	$('#pageNow').val(page);
}
function loadItems(parm){
	$.post('/DJSBlog/back/datadic/ajaxList',parm,function(res){
		$('#thead').html(res.thead);
		$('#tbody').html(res.tbody);
		$('#tfoot').html(res.tfoot);
		$('#total').html(res.total);
		loadchecks();
	},'json');
}
$(function(){loadItems({'page':1,'s_datadic.del':false});});
</script>
	