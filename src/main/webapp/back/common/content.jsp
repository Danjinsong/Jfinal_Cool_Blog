<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--数量总计组件 -->
<jsp:include page="/back/backComponent/quickStats.jsp"></jsp:include>
<!--博客分类条形图 -->
<jsp:include page="/back/backComponent/barchar.jsp"></jsp:include>
<!-- 天气预报 -->
<jsp:include page="/back/backComponent/tianqi.jsp"></jsp:include>
<hr class="whiter"/>
<!-- 撰写频率折线图 -->
<jsp:include page="/back/backComponent/linechar.jsp"></jsp:include>
<!-- 照片墙 -->
<jsp:include page="/back/backComponent/photoWall.jsp"></jsp:include>
