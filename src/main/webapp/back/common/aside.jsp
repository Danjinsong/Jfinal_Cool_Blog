<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<aside id="sidebar">
    <!-- Sidbar Widgets -->
    <div class="side-widgets overflow">
        <!-- Profile Menu -->
        <div class="text-center s-widget m-b-25 dropdown" id="profile-menu">
            <a href="" data-toggle="dropdown">
                <img class="profile-pic animated currentUserPhoto" src="/DJSBlog/static/userImages/untitled.png" alt="" style="width: 150px;height: 150px;">
            </a>
            <ul class="dropdown-menu profile-menu">
                <li><a href="javascript:void(0);"> ${currentUser.nickName } </a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
                <li><a href="/DJSBlog/back/message/index">信息</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
                <li><a href="javascript:updateCurrentUser();">设置</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
                <li><a href="/DJSBlog/back/user/logout">注销</a> <i class="icon left">&#61903;</i><i class="icon right">&#61815;</i></li>
            </ul>
            <!-- 日历组件 -->
            <jsp:include page="/back/component/calendar.jsp"></jsp:include>
            <!-- 项目记录组件 -->
            <jsp:include page="/back/component/project.jsp"></jsp:include>
        </div>
    </div>
    
    <div class="modal fade" id="currentUser_up_Modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="currentUser_up_title">设置个人信息</h4>
             </div>
             <div class="modal-body">
			        <form role="form" class="form-validation-3">
			            <div class="form-group">
			                <input type="hidden" id='currentUser_up_id' value="">
			                <input type="hidden" id="currentUser_up_userName" value="">
			            </div>
			            <div class="form-group">
			            	<label>真实姓名:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="currentUser_up_realName" placeholder="真实姓名.." value="">
			            </div>
			            <div class="form-group">
			            	<label>密码设置:</label>
			                <input type="password" class="form-control validate[required] input-sm" id="currentUser_up_password" placeholder="密码设置.." value="">
			            </div>
			            <div class="form-group">
			            	<label>确认密码:</label>
			                <input type="password" class="form-control validate[required,equals[currentUser_up_password]] input-sm" id="currentUser_up_expassword" placeholder="确认密码.." value="">
			            </div>
			            <div class="form-group">
			            	<label>昵称:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="currentUser_up_nickName" placeholder="昵称.." value="">
			            </div>
			            <div class="form-group">
			            	<label>QQ:</label>
			                <input type="text" class="form-control  validate[required,custom[number]] input-sm" id="currentUser_up_qq" placeholder="QQ.." value="">
			            </div>
			            <div class="form-group">
			            	<label>手机:</label>
			                <input type="text" class="form-control validate[required,custom[phone]]  input-sm" id="currentUser_up_phone" placeholder="电话号码.." value="">
			            </div>
			            <div class="form-group">
			            	<label>邮箱:</label>
			                <input type="text" class="form-control validate[required,custom[email]] input-sm" id="currentUser_up_email" placeholder="邮箱.." value="">
			            </div>
			            <div class="form-group">
			            	<label>个性签名:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="currentUser_up_sign" placeholder="个性签名.." value="">
			            </div>
			            <div class="form-group">
			            	<label>角色选择:</label>
			                <div class="m-b-15">
			                 <select class="select" id="currentUser_up_role">
			                 	<option selected='selected' value="visitor">选择角色</option>
			                 	<c:if test="${fn:contains(currentUser.role,'superAdmin') }">
				                 	<option  value="superAdmin">超级管理员</option>
				                 	<option  value="admin">管理员</option>
			                 	</c:if>
			                 	<option  value="vip">注册会员</option>
			                 	<option  value="visitor">游客</option>
			                 </select>
			               </div>
			            </div>
			            <div class="form-group">
				            <div class="fileupload fileupload-new" data-provides="fileupload">
				            <div class="fileupload-preview thumbnail form-control"></div>
						     <div>
						         <span class="btn btn-file btn-alt btn-sm">
						             <span class="fileupload-new">选择图片</span>
						             <span class="fileupload-exists">更换</span>
						             <input type="file" id="user_image" name="user_image" onchange="javascript:uploadImages();"/>
						       		 <input type="hidden" class="filesId" value=""/>
						         </span>
						         <a href="javascript:$('#image-user').val('');return false;" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
						     </div>
					     </div>
			            </div>
			            <div class="modal-footer">
			                 <button type="button" class="btn btn-sm" onclick="save_currentUser_up_Item()" >提交</button>
			                 <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="resetVal();">取消</button>
			             </div>
			        </form>
			    </div>
             </div>
	     </div>
	 </div>
    <script>
    function uploadImages(){
    	upload('user_image',$('#user_image').val(),$('.filesId').val(),'',"/DJSBlog/back/files/ajaxSave?width=60&&height=60");
    }
    function resetVals(){
    	$('.fileupload-preview').html("");
    	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
    }
    function updateCurrentUser(){
    	resetVals();
    	$.post('/DJSBlog/back/user/ajaxGetById',{id:${currentUser.id}},function(res){
			$('#currentUser_up_userName').val(res.user.userName);
			$('#currentUser_up_realName').val(res.user.realName);
			$('#currentUser_up_nickName').val(res.user.nickName);
			$('#currentUser_up_qq').val(res.user.qq);
			$('#currentUser_up_phone').val(res.user.phone);
			$('#currentUser_up_id').val(res.user.id);
			$('#currentUser_up_role').val(res.user.role);
			$('#currentUser_up_email').val(res.user.email);
			$('#currentUser_up_sign').val(res.user.sign);
			$('#currentUser_up_password').val(res.user.password);
			$('#currentUser_up_expassword').val(res.user.password);
			if(res.user.photo!=null&&res.user.photo!=''){
				$.post('/DJSBlog/back/files/ajaxGetById',{id:res.user.photo},function(data){
					$('.filesId').val(data.files.id);
					$('.fileupload-preview').html("<img src='/DJSBlog"+data.files.src+"'/>");
				});
			}
			$('.select').selectpicker('refresh');
    		$('#currentUser_up_Modal').modal('show');
		},'json');
    }
    function save_currentUser_up_Item(){
    	if($('#currentUser_up_userName').val()!=''&&$('#currentUser_up_nickName').val()!=''&&$('#currentUser_up_password').val()!=''){
    		$.post('/DJSBlog/back/user/ajaxSave',{
        		'user.userName':$('#currentUser_up_userName').val(),
        		'user.nickName':$('#currentUser_up_nickName').val(),
        		'user.realName':$('#currentUser_up_realName').val(),
        		'user.role':$('#currentUser_up_role').val(),
        		'user.qq':$('#currentUser_up_qq').val(),
        		'user.sign':$('#currentUser_up_sign').val(),
        		'user.id':$('#currentUser_up_id').val(),
        		'user.email':$('#currentUser_up_email').val(),
        		'user.photo':$('.filesId').eq(0).val(),
        		'user.phone':$('#currentUser_up_phone').val(),
        		'user.password':$('#currentUser_up_password').val()
        	},function(res){
        		if(res.success){
        			$('#currentUser_up_Modal').modal('hide');
        			mes("温馨提示!","操作成功!请刷新页面查看数据更新状况");
        			
        		}
        		else
        			mes("温馨提示!","操作失败,用户名已经存在");
        	},'json');
    	}else{
    		mes("温馨提示","必须输入项目不可以为空");
    	}
    	
    }
    </script>
    <!-- Side Menu -->
    <ul class="list-unstyled side-menu">
        <li class="<c:if test='${home}'>active</c:if>">
            <a class="sa-side-home" href="/DJSBlog/back/index">
                <span class="menu-item">首页</span>
            </a>
        </li>
        <li class="dropdown <c:if test='${blog}'>active</c:if>">
            <a class="sa-side-typography" href="">
                <span class="menu-item">博客</span>
            </a>
            <ul class="list-unstyled menu-item">
                <li><a href="/DJSBlog/back/blog/index">所有博客</a></li>
                <li><a href="/DJSBlog/back/type/index">编辑分类</a></li>
            </ul>
        </li>
        <li class="dropdown <c:if test='${yemian}'>active</c:if>">
            <a class="sa-side-form" href="">
                <span class="menu-item">页面</span>
            </a>
            <ul class="list-unstyled menu-item">
                <li><a href="/DJSBlog/static/from">所有页面</a></li>
                <li><a href="/DJSBlog/static/exCheckFrom">新建页面</a></li>
            </ul>
        </li>
        <li class="<c:if test='${user}'>active</c:if>">
            <a class="sa-side-folder" href="/DJSBlog/back/user/index">
                <span class="menu-item">用户</span>
            </a>
        </li>
        <li class="<c:if test='${files}'>active</c:if>">
            <a class="sa-side-widget" href="/DJSBlog/back/files/index">
                <span class="menu-item">附件</span>
            </a>
        </li>
        <li class="dropdown <c:if test='${dataDic}'>active</c:if>">
            <a class="sa-side-ui" href="">
                <span class="menu-item">数据</span>
            </a>
            <ul class="list-unstyled menu-item">
                <li><a href="/DJSBlog/back/datadic/index">数据字典</a></li>
                <li><a href="/DJSBlog/static/bootComponent">数据管理</a></li>
            </ul>
        </li>
    </ul>
</aside>
<script>
$(function(){
	if(${currentUser.photo}!=null&&${currentUser.photo}!=''){
		$.post('/DJSBlog/back/files/ajaxGetById',{id:${currentUser.photo}},function(res){
			$('.currentUserPhoto').attr({src:'/DJSBlog'+res.files.src});
		},'json');
	}
});

</script>