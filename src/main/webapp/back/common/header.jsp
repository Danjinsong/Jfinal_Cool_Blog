<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
 <header id="header" class="media">
     <a href="" id="menu-toggle"></a> 
     <a class="logo pull-left" href="/DJSBlog/back/index">DJSBLOG管理</a>
     <div class="media-body">
         <div class="media" id="top-menu">
             <div id="time" class="pull-right">
                 <span id="hours"></span>
                 :
                 <span id="min"></span>
                 :
                 <span id="sec"></span>
             </div>
             <div class="media-body">
                 <h5 id="pathNow">${pathNow}</h5>
             </div>
         </div>
     </div>
 </header>
 <div class="clearfix"></div>
