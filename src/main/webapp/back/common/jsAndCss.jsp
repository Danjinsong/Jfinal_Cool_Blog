<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
  <!-- 后台CSS -->
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/bootstrap.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/animate.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/form.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/calendar.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/style.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/icons.css" rel="stylesheet">
  <link href="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/css/generics.css" rel="stylesheet"> 
  
   <!-- 后台js Libraries -->
   <!-- jQuery -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/jquery.min.js"></script> <!-- jQuery Library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/jquery-ui.min.js"></script> <!-- jQuery UI -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/jquery.easing.1.3.js"></script> <!-- jQuery Easing - Requirred for Lightbox + Pie Charts-->

   <!-- Bootstrap -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/bootstrap.min.js"></script>

   <!-- Charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.js"></script> <!-- Flot Main -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.time.js"></script> <!-- Flot sub -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.animator.min.js"></script> <!-- Flot sub -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.resize.min.js"></script> <!-- Flot sub - for repaint when resizing the screen -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.orderBar.js"></script> <!-- Flot Bar chart -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts/jquery.flot.pie.min.js"></script> <!-- Flot Pie chart -->

   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/sparkline.min.js"></script> <!-- Sparkline - Tiny charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/easypiechart.js"></script> <!-- EasyPieChart - Animated Pie Charts -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/charts.js"></script> <!-- All the above chart related functions -->

   <!-- Map -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/maps/jvectormap.min.js"></script> <!--   jVectorMap main library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/maps/world.js"></script> <!-- USA Map for jVectorMap -->

   <!--  Form Related -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/validate.min.js"></script> <!-- jQuery Form Validation Library -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/validationEngine.min.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/static/js/validation/jquery.validationEngine-zh_CN.js"></script> <!-- jQuery Form Validation Library - requirred with above js -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/select.min.js"></script> <!-- Custom Select -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/chosen.min.js"></script> <!-- Custom Multi Select -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/datetimepicker.min.js"></script> <!-- Date & Time Picker -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/colorpicker.min.js"></script> <!-- Color Picker -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/icheck.js"></script> <!--  Custom Checkbox + Radio -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/autosize.min.js"></script> <!-- Textare autosize -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/toggler.min.js"></script> <!-- Toggler -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/input-mask.min.js"></script> <!-- Input Mask -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/spinner.min.js"></script> <!-- Spinner -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/slider.min.js"></script> <!-- Input Slider -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/fileupload.min.js"></script> <!-- File Upload -->
        
  <!-- Text Editor -->
  <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/editor2.min.js"></script> <!-- WYSIWYG Editor -->
  <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/markdown.min.js"></script> <!-- Markdown Editor -->
 
   <!-- UX -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/scroll.min.js"></script> <!-- Custom Scrollbar -->

   <!-- Other -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/calendar.min.js"></script> <!-- Calendar -->
   <!-- <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/feeds.min.js"></script> News Feeds -->
   

   <!-- All JS functions -->
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/functions.js"></script>
   <script src="http://ofhnialdm.bkt.clouddn.com/DJSBlog/back/js/ajaxfileupload.js"></script>
