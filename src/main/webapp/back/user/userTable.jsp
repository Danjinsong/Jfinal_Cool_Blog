<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!-- Table Hover -->
<div class="col-md-12 ">
     <form class="form-inline" role="form" action="#">
       	<span style="float: right; font-size: 20px;" >共有<font color="red" id="total" >0</font>条数据</span>
       <div class="form-group">
           <input type="text" class="form-control input-sm" id="s_userName" placeholder="输入关键字..." onkeydown="if(event.keyCode==13) searchItems();">
		   <button class="btn tooltips" data-placement="top"  onclick="searchItems();"><i class="sa-list-search"></i></button>
       </div>
       <div class="btn-group">
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="添加" onclick="addItem();return false;"><i class="sa-list-add"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="修改" onclick="updateItem();return false;"><i class="sa-list-move"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="删除" onclick="deleteOrComebackItem('true');return false;"><i class="sa-list-delete"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="恢复" onclick="deleteOrComebackItem('false');return false;"><i class="sa-list-forwad"></i></button>
  		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='已删除'onclick="showDel('true');return false;"><i class='sa-list-archive'></i></button>
		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='未删除'onclick="showDel('false');return false;"><i class='sa-list-spam'></i></button>
 		<input type="hidden" id="show_state" value="false"/>
  	  </div>
    </form>
    <div class="table-responsive overflow">
        <table class="table table-bordered table-hover tile message-list list-container">
            <thead id="thead">
            </thead>
            <tbody id="tbody">
            </tbody>
            <tfoot id="tfoot">
            </tfoot>
        </table>
    </div>
</div>
<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="user_title"></h4>
             </div>
             <div class="modal-body">
			        <form role="form" >
			            <div class="form-group">
			                <input type="hidden" id='user_id' value="">
			            	<label>用户名:</label>
			                <input type="text" class="form-control input-sm" id="user_userName" placeholder="用户名.." value="">
			            </div>
			            <div class="form-group">
			            	<label>密码:</label>
			                <input type="password" class="form-control input-sm" id="user_passwrod" placeholder="密码.." value="">
			            </div>
			            <div class="form-group">
			            	<label>确认密码:</label>
			                <input type="password" class="form-control input-sm" id="user_exPassword" placeholder="确认密码.." value="">
			            </div>
			            <div class="form-group">
			            	<label>昵称:</label>
			                <input type="text" class="form-control input-sm" id="user_nickName" placeholder="昵称.." value="">
			            </div>
			            <div class="form-group">
			            	<label>角色选择:</label>
			                <div class="m-b-15">
			                 <select class="form-control select" id="user_role">
			                 	<option selected='selected' value="visitor">选择角色</option>
			                 	<c:if test="${fn:contains(currentUser.role,'superAdmin') }">
				                 	<option  value="superAdmin">超级管理员</option>
				                 	<option  value="admin">管理员</option>
			                 	</c:if>
			                 	<option  value="vip">注册会员</option>
			                 	<option  value="visitor">游客</option>
			                 </select>
			               </div>
			            </div>
			            <div class="modal-footer">
			                 <button type="button" class="btn btn-sm" onclick="save_user_Item()" >提交</button>
			                 <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="resetVal();">取消</button>
			             </div>
			        </form>
			    </div>
             </div>
     </div>
 </div>
 <div class="modal fade" id="user_up_Modal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="user_up_title"></h4>
             </div>
             <div class="modal-body">
			        <form role="form" class="form-validation-3">
			            <div class="form-group">
			                <input type="hidden" id='user_up_id' value="">
			                <input type="hidden" id="user_up_userName" value="">
			            </div>
			            <div class="form-group">
			            	<label>真实姓名:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="user_up_realName" placeholder="真实姓名.." value="">
			            </div>
			            <div class="form-group">
			            	<label>昵称:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="user_up_nickName" placeholder="昵称.." value="">
			            </div>
			            <div class="form-group">
			            	<label>QQ:</label>
			                <input type="text" class="form-control  validate[required,custom[number]] input-sm" id="user_up_qq" placeholder="QQ.." value="">
			            </div>
			            <div class="form-group">
			            	<label>手机:</label>
			                <input type="text" class="form-control validate[required,custom[phone]]  input-sm" id="user_up_phone" placeholder="电话号码.." value="">
			            </div>
			            <div class="form-group">
			            	<label>邮箱:</label>
			                <input type="text" class="form-control validate[required,custom[email]] input-sm" id="user_up_email" placeholder="邮箱.." value="">
			            </div>
			            <div class="form-group">
			            	<label>个性签名:</label>
			                <input type="text" class="form-control validate[required] input-sm" id="user_up_sign" placeholder="个性签名.." value="">
			            </div>
			            <div class="form-group">
			            	<label>角色选择:</label>
			                <div class="m-b-15">
			                 <select class="select" id="user_up_role" >
			                 	<option selected='selected' value="visitor">选择角色</option>
			                 	<c:if test="${fn:contains(currentUser.role,'superAdmin') }">
				                 	<option  value="superAdmin">超级管理员</option>
				                 	<option  value="admin">管理员</option>
			                 	</c:if>
			                 	<option  value="vip">注册会员</option>
			                 	<option  value="visitor">游客</option>
			                 </select>
			               </div>
			            </div>
			            <div class="form-group">
				            <div class="fileupload fileupload-new" data-provides="fileupload">
				            <div class="fileupload-preview thumbnail form-control"></div>
						     <div>
						         <span class="btn btn-file btn-alt btn-sm">
						             <span class="fileupload-new">选择图片</span>
						             <span class="fileupload-exists">更换</span>
						             <input type="file" id="image" name="image" onchange="javascript:uploadImage();"/>
						       		 <input type="hidden" class="filesId" value="" id="userFiles"/>
						         </span>
						         <a href="javascript:$('#image').val('');return false;" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
						     </div>
					     </div>
			            </div>
			            <div class="modal-footer">
			                 <button type="button" class="btn btn-sm" onclick="save_user_up_Item()" >提交</button>
			                 <button type="button" class="btn btn-sm" data-dismiss="modal" onclick="resetVal();">取消</button>
			             </div>
			        </form>
			    </div>
             </div>
     </div>
 </div>
 <input type="hidden" value="1" id='pageNow'/>
<script>
function uploadImage(){
	upload('image',$('#image').val(),$('.filesId').val(),$('.filesId').val(),"/DJSBlog/back/files/ajaxSave?width=60&&height=60");
}
function resetVal(){
	$('.fileupload-preview').html("");
	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
}
function showDel(del){
	$('#show_state').val(del);
	loadItems({'s_user.del':$('#show_state').val(),
		   's_user.userName':$('#s_userName').val(),
			page:1,
			});
	$('#pageNow').val(1);
}
function getChecked(){
	var checked = $("input:checked");
	var val=[];
	for(var i=0;i<checked.length;i++){
		if(checked.eq(i).val()!='on'&&checked.eq(i).val()!='false'&&checked.eq(i).val()!='true')
		val.push(checked.eq(i).val());
	}
	return val;
}
function deleteOrComebackItem(del){
	var val=getChecked();
	if(val.length==0){
		mes("温馨提示!","请选择元素进行操作!");
	}else{
		var str="";
		for(var i=0;i<val.length;i++){
				str+=val[i];
			if(i!=val.length-1)
				str+=",";
		}
		$.post('/DJSBlog/back/user/ajaxDeleteOrComeback',{ids:str,del:del},function(res){
			if(res.success){
				mes("系统提示","操作成功");
				loadItems({'s_user.del':$('#show_state').val(),page:$('#pageNow').val()});				
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}
}
function addItem(){
	$('#user_title').html("用户添加");
	$('#userModal').modal('show');
}
function updateItem(){
	var val=getChecked();
	if(val.length!=1){
		mes("温馨提示!","请选择一行元素进行修改!");
	}else{
		$.post('/DJSBlog/back/user/ajaxGetById',{id:val[0]},function(res){
			$('#user_up_title').html("修改用户:"+res.user.userName);
			$('#user_up_userName').val(res.user.userName);
			$('#user_up_realName').val(res.user.realName);
			$('#user_up_nickName').val(res.user.nickName);
			$('#user_up_qq').val(res.user.qq);
			$('#user_up_phone').val(res.user.phone);
			$('#user_up_id').val(res.user.id);
			$('#user_up_role').val(res.user.role);
			$('#user_up_email').val(res.user.email);
			$('#user_up_sign').val(res.user.sign);
			if(res.user.photo!=null&&res.user.photo!=''){
				$.post('/DJSBlog/back/files/ajaxGetById',{id:res.user.photo},function(data){
					$('.filesId').val(data.files.id);
					$('.fileupload-preview').html("<img src='/DJSBlog"+data.files.src+"'/>");
					
				});
			}
			$('.select').selectpicker('refresh');
			$('#user_up_Modal').modal('show');
		},'json');
	}
}
function save_user_up_Item(){
	saveItem({
		'user.userName':$('#user_up_userName').val(),
		'user.nickName':$('#user_up_nickName').val(),
		'user.realName':$('#user_up_realName').val(),
		'user.role':$('#user_up_role').val(),
		'user.qq':$('#user_up_qq').val(),
		'user.sign':$('#user_up_sign').val(),
		'user.id':$('#user_up_id').val(),
		'user.email':$('#user_up_email').val(),
		'user.photo':$('#userFiles').val(),
		'user.phone':$('#user_up_phone').val()});
}
function save_user_Item(){
	var password = $('#user_passwrod').val();
	var exPassword = $('#user_exPassword').val();
	var userName = $('#user_userName').val();
	var nickName = $('#user_nickName').val();
	if(password!=''&&exPassword!=''&&userName!=''&&nickName!=''){
		if(password != exPassword){
			mes("系统提示","两次输入的密码不相等");
		}
		else {
			saveItem({
			'user.userName':userName,
			'user.password':password,
			'user.nickName':nickName,
			'user.id':$('#user_id').val(),
			'user.role':$('#user_role').val()});
		}
			
	}else{
		mes("系统提示","输入不能为空");
	}
		
	
}
function saveItem(parm){
	$.post('/DJSBlog/back/user/ajaxSave',parm,function(res){
		if(res.success){
			$('#userModal').modal('hide');
			$('#user_up_Modal').modal('hide');
			mes("温馨提示!","操作成功");
			resetVal();
			loadItems({'s_user.del':false,page:$('#pageNow').val()});	
		}
		else
			mes("温馨提示!","操作失败,用户名已经存在");
	},'json');
}
function toPage(page){
	loadItems({'s_user.del':$('#show_state').val(),
		   's_user.userName':$('#s_userName').val(),
			page:page
			});
	$('#pageNow').val(page);
}
function loadItems(parm){
	$.post('/DJSBlog/back/user/ajaxList',parm,function(res){
		$('#thead').html(res.thead);
		$('#tbody').html(res.tbody);
		$('#tfoot').html(res.tfoot);
		$('#total').html(res.total);
		loadchecks();
	},'json');
}
function searchItems(){
	loadItems({'s_user.del':$('#show_state').val(),
			   's_user.userName':$('#s_userName').val(),
				page:1,
				});
}
$(function(){loadItems({'s_user.del':false,page:1});});
</script>