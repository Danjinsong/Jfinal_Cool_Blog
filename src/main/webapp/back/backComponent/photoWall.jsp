<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-4">
<!-- Photos -->
	<div class="tile">
	    <h2 class="tile-title">照片墙</h2>
	    <div class="tile-config dropdown">
	        <a  href="" class="tooltips tile-menu" title="" data-original-title="刷新"></a>
	    </div>
	    <div class="p-5 photos">
	    	<c:forEach items="${photoWall }" var="photo">
		        <div class="col-xs-3">
		            <img src="/DJSBlog${photo.src}" alt="" style="width:150px;height:150px;">
	    	    </div>
	    	</c:forEach>
	        <div class="clearfix"></div>
	    </div>
	</div>
</div>