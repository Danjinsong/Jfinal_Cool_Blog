<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Quick Stats -->
<div class="block-area">
	<div class="row">
		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats">
				<div id="stats-line-2" class="pull-left"></div>
				<div class="data">
					<h2 data-value="${data_map['blogTotal']}"></h2>
					<small>所有博客</small>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line-3" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="${data_map['blog_week_Total']}"></h2>
					<small>近一周博客</small>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats media">

				<div id="stats-line-4" class="pull-left"></div>

				<div class="media-body">
					<h2 data-value="${data_map['userTotal']}"></h2>
					<small>所有用户</small>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="${data_map['user_week_Total']}"></h2>
					<small>近一周注册用户</small>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="${data_map['filesTotal']}">0</h2>
					<small>所有附件</small>
				</div>
			</div>
		</div>

		<div class="col-md-2 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="${data_map['files_week_Total']}">0</h2>
					<small>近一周附件</small>
				</div>
			</div>
		</div>
	</div>

</div>

<hr class="whiter"/>