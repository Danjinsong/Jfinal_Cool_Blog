<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="col-md-8">
	<div class="tile">
	    <h2 class="tile-title">博客分布</h2>
	    <div class="tile-config dropdown">
	        <a  href="" class="tooltips tile-menu" title="更新"></a>
	    </div>
	    <div class="p-10">
	        <div id="bar-chart" class="main-chart" style="height: 395px"></div>
	    </div>
	</div>
</div>
<input type="hidden" value="${data_map['barChar']}" id="barChar">
<script>
$(function(){
	    if ($("#bar-chart")[0]) {
	    	var data1=[[1,0],[2,0],[3,0],[4,0]];
	        var data2 = [[1,$('#barChar').val().split(",")[0]], [2,$('#barChar').val().split(",")[1]], [3,$('#barChar').val().split(",")[2]], [4,$('#barChar').val().split(",")[3]]];
	        var data3 = [[1,0],[2,0],[3,0],[4,0]];
	        var ticks = [
	                     [1, '游戏'], [2,'电影'], [3, '生活'],[4,'学习']
	                 ];

	        var barData = new Array();
			
	        barData.push({
	                data : data1,
	                label: '',
	                bars : {
	                        show : true,
	                        barWidth : 0.1,
	                        order : 1,
	                        fill:1,
	                        lineWidth: 0,
	                        fillColor: 'rgba(255,255,255,0.6)'
	                },
	                ticks:ticks,
	        });
			
	        barData.push({
	                data : data2,
	                label: '',
	                bars : {
	                        show : true,
	                        barWidth : 0.1,
	                        order : 2,
	                        fill:1,
	                        lineWidth: 0,
	                        fillColor: 'rgba(255,255,255,0.6)'
	                },
	                ticks:ticks,
	        });
	        barData.push({
	                data : data3,
	                label: '',
	                bars : {
	                        show : true,
	                        barWidth : 0.1,
	                        order : 3,
	                        fill:1,
	                        lineWidth: 0,
	                        fillColor: 'rgba(255,255,255,0.6)'
	                },
	                ticks:ticks,
	        });

	        //Display graph
	        $.plot($("#bar-chart"), barData, {
	                
	                grid : {
	                        borderWidth: 1,
	                        borderColor: 'rgba(255,255,255,0.25)',
	                        show : true,
	                        hoverable : true,
	                        clickable : true,       
	                },
	                
	                yaxis: {
	                    tickColor: 'rgba(255,255,255,0.15)',
	                    tickDecimals: 0,
	                    font :{
	                        lineHeight: 13,
	                        style: "normal",
	                        color: "rgba(255,255,255,0.8)",
	                    },
	                    shadowSize: 0
	                    
	                },
	                
	                xaxis: {
	                    tickColor: 'rgba(255,255,255,0)',
	                    tickDecimals: 0,
	                    font :{
	                        lineHeight: 13,
	                        style: "normal",
	                        color: "rgba(255,255,255,0.8)",
	                    },
	                    shadowSize: 0,
	                    axisLabel: "",
	                    axisLabelUseCanvas: true,
	                    axisLabelFontSizePixels: 12,
	                    axisLabelFontFamily: 'Verdana, Arial',
	                    axisLabelPadding: 10,
	                    ticks: ticks

	                    
	                },
	                
	                legend : true,
	                tooltip : true,
	                tooltipOpts : {
	                        content : "<b>%x</b> = <span>%y</span>",
	                        defaultTheme : false
	                }

	        });
	        
	        $("#bar-chart").bind("plothover", function (event, pos, item) {
	            if (item) {
	                var x = item.datapoint[0].toFixed(0),
	                    y = item.datapoint[1].toFixed(0);
	                $("#barchart-tooltip").html(item.series.ticks[x-1][1]+":" +y+"条").css({top: item.pageY+5, left: item.pageX+5}).fadeIn(200);
	            }
	            else {
	                $("#barchart-tooltip").hide();
	            }
	        });

	        $("<div id='barchart-tooltip' class='chart-tooltip'></div>").appendTo("body");

	    }

});
</script>
