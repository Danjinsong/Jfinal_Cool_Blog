<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="col-md-8">
	<div class="tile">
		  <h2 class="tile-title">撰写频率</h2>
		    <div class="tile-config dropdown">
            <a data-view="month" href="#" class="tooltips" title="最近一周">
              <i class="sa-list-month"></i>
            </a>
	        <a data-view="agendaWeek" href="#" class="tooltips" title="最近一月">
               <i class="sa-list-week"></i>
            </a>
            <a data-view="agendaDay" href="#" class="tooltips" title="最近几月">
               <i class="sa-list-day"></i>
            </a>
		    </div>
		<div class="p-10">
	    	<div id="line-chart" class="main-chart" style="height: 400px"></div>
	    </div>
	</div>
</div>
<input type="hidden" value="${data_map['lineChar']}" id="lineChar">
<script>
$(function () {
    if ($('#line-chart')[0]) {
       var item=$('#lineChar').val().split(",");
	//var d1 = [[1,14], [2,15], [3,18], [4,16], [5,19], [6,17], [7,15],[8]];
	//var ticks = [[1, "周一"], [2, "周二"], [3, "周三"], [4, "周四"], [5, "周五"], [6, "周六"], [7, "周日"],[8,'']];
       var d1 = [[1,item[0]], [2,item[2]], [3,item[4]], [4,item[6]], [5,item[8]], [6,item[10]], [7,item[12]],[8,item[14]]];
       var ticks = [[1, item[1]], [2, item[3]], [3, item[5]], [4, item[7]], [5, item[9]], [6,item[11]], [7,item[13]],[8,'今天']];
    	//alert(item);
       $.plot('#line-chart', [ {
            data: d1,
            label: "最近一个周编写频率折线图",
            ticks:ticks

        },],

            {
                series: {
                    lines: {
                        show: true,
                        lineWidth: 1,
                        fill: 0.25,
                    },

                    color: 'rgba(255,255,255,0.7)',
                    shadowSize: 0,
                    points: {
                        show: true,
                    }
                },

                yaxis: {
                    min: 0,
                    tickColor: 'rgba(255,255,255,0.15)',
                    tickDecimals: 0,
                    font :{
                        lineHeight: 13,
                        style: "normal",
                        color: "rgba(255,255,255,0.8)",
                    },
                    shadowSize: 0,
                },
                xaxis: {
                    tickColor: 'rgba(255,255,255,0)',
                    font :{
                        lineHeight: 13,
                        style: "normal",
                        color: "rgba(255,255,255,0.8)",
                    },
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 20,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,	
                    ticks: ticks
                },
                grid: {
                    borderWidth: 1,
                    borderColor: 'rgba(255,255,255,0.25)',
                    labelMargin:10,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius:6,
                },
                legend: {
                    show: false
                }
            });

        $("#line-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(0);
                $("#linechart-tooltip").html(item.series.ticks[x-1][1] + ":"   + y+ "条").css({top: item.pageY+5, left: item.pageX+5}).fadeIn(200);
            }
            else {
                $("#linechart-tooltip").hide();
            }
        });

        $("<div id='linechart-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }

});
</script>