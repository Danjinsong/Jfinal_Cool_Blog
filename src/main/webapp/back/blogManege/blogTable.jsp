<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Table Hover -->
<div class="block-area" id="tableHover">
     <form class="form-inline" role="form" action="#">
       	<span style="float: right; font-size: 20px;" >共有<font color="red" id="total" >0</font>条数据</span>
       <div class="form-group">
           <input type="text" class="form-control input-sm" id="s_title" placeholder="输入关键字..." onkeydown="if(event.keyCode==13){ searchItems(); return false;}">
		   <button class="btn tooltips" data-placement="top"  onclick="searchItems();return false;"><i class="sa-list-search"></i></button>
       </div>
       <div class="btn-group">
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="添加" onclick="addItem();return false;"><i class="sa-list-add"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="修改" onclick="updateItem();return false;"><i class="sa-list-move"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="删除" onclick="deleteOrComebackItem('true');return false;"><i class="sa-list-delete"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="恢复" onclick="deleteOrComebackItem('false');return false;"><i class="sa-list-forwad"></i></button>
  		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='已删除'onclick="showDel('true');return false;"><i class='sa-list-archive'></i></button>
		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='未删除'onclick="showDel('false');return false;"><i class='sa-list-spam'></i></button>
 		<input type="hidden" id="show_state" value="false"/>
  	  </div>
    </form>
    <div class="table-responsive">
        <table class="table table-bordered table-hover tile list-container" style='text-align: center;'>
            <thead id="thead">
            </thead>
            <tbody id="tbody">
            </tbody>
            <tfoot id="tfoot">
            </tfoot>
        </table>
    </div>
</div>
<div class="modal fade" id="blogModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="blog_title"></h4>
             </div>
             <div class="modal-body">
			        <form role="form" class="form-validation-3">
			            <div class="form-group">
			                <input type="hidden" id='blog_id' value="">
			                <input type="hidden" id='blog_user_id' value="">
			                <label>标题</label>
			                <input type="text" class="form-control input-sm validate[required]" id="blog_up_title" placeholder="标题.." value="">
			            </div>
			            <div class="form-group">
			                <label>分类</label>
			                <div class="">
			                 <select class="select" id="select_type" multiple >
			                 </select>
			               </div>
			            </div>
			             <div class="form-group">
			                <label>专题(是否为某一个专题写的博客)</label>
			                <div class="">
			                 <select class="select" id="select_zhuanti">
			                 	<option value="0" selected="selected">无</option>
			                 </select>
			               </div>
			            </div>
		             	<label>博客大图（1920x570）</label>
			            <div class="form-group">
				            <div class="fileupload fileupload-new" data-provides="fileupload">
				            <div class="fileupload-preview thumbnail form-control" style="width: 100%"></div>
						     <div>
						         <span class="btn btn-file btn-alt btn-sm">
						             <span class="fileupload-new">选择图片</span>
						             <span class="fileupload-exists">更换</span>
						             <input type="file" id="image" name="image" onchange="javascript:uploadImage();return false;"/>
						       		 <input type="hidden" class="filesId" value="" id="blogFiles"/>
						         </span>
						         <a href="javascript:$('#image').val('');return false;" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
						     </div>
					     </div>
			            </div>
			            <div class="form-group">
						   <label>博客内容</label>
						   <div class="wysiwye-editor" id="blog_text"></div>
						</div>
						 <div class="form-group">
			                <label>文章标签(以空格分割)</label>
			                <input type="text" class="form-control input-sm" id="blog_biaoqian" placeholder="标签.." value="">
			            </div>
			            
					 <div class="form-group">
		                <label id="preMp3">MP3</label>
		                <div class="fileupload fileupload-new" data-provides="fileupload">
                           <span class="btn btn-file btn-sm btn-alt">
                               <span class="fileupload-new">选择mp3文件</span>
                               <span class="fileupload-exists">更换mp3文件</span>
                               <input type="file" name="mp3" id="mp3" onchange="uploadBlog_MP3();return false;"/>
                               <input type="hidden" class="mp3Id" value="" id="mp3_Id"/>
                           </span>
                           <a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">
                               <i class="fa fa-times"></i>
                           </a>
                        </div>
			            </div>
			        </form>
	             <div class="modal-footer">
	                 <button type="button" class="btn btn-sm" onclick="save_blog_Item()" >提交</button>
	                 <button type="button" class="btn btn-sm" data-dismiss="modal">取消</button>
	             </div>
             </div>
         </div>
     </div>
 </div>
 <input type="hidden" value="1" id='pageNow'/>
<script>
function uploadBlog_MP3(){
	//alert($('#mp3').val());
	uploadMP3('mp3',$('#mp3').val(),"/DJSBlog/back/files/ajaxSaveSong");
}
function saveItem(parm){
	$.post('/DJSBlog/back/blog/ajaxSave',parm,function(res){
		if(res.success){
			resetVal();
			$('#blogModal').modal('hide');
			mes("系统提示","操作成功");
			loadItems({'s_blog.del':$('#show_state').val(),page:$('#pageNow').val()});				
		}
		else
			mes("系统提示","操作失败");
	},'json');
}
function save_blog_Item(){
	var blogText_notTag=$('<p>'+$(".note-editable").eq(0).html()+'</p>').text();
	var blogText=$(".note-editable").eq(0).html();
	var type="";
	for(var i=0;i<$('#select_type').val().length;i++){
		type+=$('#select_type').val()[i];
		if(i!=$('#select_type').val().length-1)
			type+=",";
	}
	if(blogText_notTag!=null&&blogText_notTag!=''&&$('#blog_up_title').val()!=''){
		saveItem({
			'blog.title':$('#blog_up_title').val(),
			'blog.id':$('#blog_id').val(),
			'blog.text':blogText,
			'blog.summary':blogText_notTag,
			'blog.photo':$('#blogFiles').val(),
			'blog.mp3':$('#mp3_Id').val(),
			'blog.userId':$('#blog_user_id').val()==''?${currentUser.id}:$('#blog_user_id').val(),
			'type':type,
			'biaoqian':$('#blog_biaoqian').val(),
			'zhuanti':$('#select_zhuanti').val()
		});
	}else{
		mes("系统提示","您还没有写博客");
	}
	
}
function uploadImage(){																				
	upload('image',$('#image').val(),$('.filesId').val(),'',"/DJSBlog/back/files/ajaxSave?width=1920&&height=570");
}
function showDel(del){
	$('#show_state').val(del);
	loadItems({'s_blog.del':$('#show_state').val(),
		   's_blog.title':$('#s_title').val(),
			page:1,
			});
	$('#pageNow').val(1);
}
function getChecked(){
	var checked = $("input:checked");
	var val=[];
	for(var i=0;i<checked.length;i++){
		if(checked.eq(i).val()!='on'&&checked.eq(i).val()!='false'&&checked.eq(i).val()!='true')
		val.push(checked.eq(i).val());
	}
	return val;
}
function deleteOrComebackItem(del){
	var val=getChecked();
	if(val.length==0){
		mes("温馨提示!","请选择元素进行操作!");
	}else{
		var str="";
		for(var i=0;i<val.length;i++){
				str+=val[i];
			if(i!=val.length-1)
				str+=",";
		}
		$.post('/DJSBlog/back/blog/ajaxDeleteOrComeback',{ids:str,del:del},function(res){
			if(res.success){
				mes("系统提示","操作成功");
				loadItems({'s_blog.del':$('#show_state').val(),page:$('#pageNow').val()});				
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}
}
function addItem(){
	resetVal();
	$('#blog_title').html("添加博客");
	$('#blogModal').modal('show');
}
function updateItem(){
	var val=getChecked();
	if(val.length!=1){
		mes("温馨提示!","请选择一行元素进行修改!");
	}else{
		loadFt();
		resetVal();
		$.post('/DJSBlog/back/blog/ajaxGetById',{id:val[0]},function(res){
			$('#blog_title').html("修改博客");
			$('#blog_up_title').val(res.blog.title);
			$('#blog_id').val(res.blog.id);
			$('#blog_user_id').val(res.blog.userId);
			//分类
			var type=[];
			for(var i=0;i<res.type.length;i++) type.push(res.type[i].sonId);
			$('#select_type').val(type);
			//构造图片
			if(res.blog.photo!=null&&res.blog.photo!=''){
				$.post('/DJSBlog/back/files/ajaxGetById',{id:res.blog.photo},function(data){
					$('.filesId').val(data.files.id);
					$('.fileupload-preview').html("<img src='/DJSBlog"+data.files.src+"'/>");
				},'json');
				$('.filesId').val(res.blog.photo);
			}
			//加载mp3
				//	alert(res.blog.mp3);
			if(res.blog.mp3!=null&&res.blog.mp3!=''){
				$.post('/DJSBlog/back/files/ajaxGetById',{id:res.blog.mp3},function(data){
					$('#preMp3').html(data.files.name);
					$('#mp3_Id').val(data.files.id);
				},'json');
			}
			//编辑器内容
			$(".note-editable").eq(0).html(res.blog.text);
			//构造标签
			var biaoqian="";
			if(res.biaoqian!=null){
				for(var i=0;i<res.biaoqian.length;i++){
					if(i!=0) biaoqian+=" ";
					biaoqian+=res.biaoqian[i].name;
				}
			}
			$('#blog_biaoqian').val(biaoqian);
			//构造专题
			if(res.zhuanti!=null&&res.zhuanti!='')
				$('#select_zhuanti').val(res.zhuanti.sonId);
			//刷新下拉栏
			$('.select').selectpicker('refresh');
			$('#blogModal').modal('show');
		},'json');
	}
}
function resetVal(){
	$('#blog_biaoqian').val('');
	$('#blog_up_title').val('');
	$('#blog_user_id').val('');
	$('#blog_id').val('');
	$('#mp3_Id').val('');
	$('#preMp3').html('MP3');
	$('.filesId').val('');
	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
	$('#select_type').val([]);
	$('#select_zhuanti').val(0);
	$('.select').selectpicker('refresh');
	$('.fileupload-preview').html("");
	$(".note-editable").eq(0).html('<p></p>');
}
function loadItems(parm){
		$.post('/DJSBlog/back/blog/ajaxList',parm,function(res){
			$('#thead').html(res.thead);
			$('#tbody').html(res.tbody);
			$('#tfoot').html(res.tfoot);
			$('#total').html(res.total);
			loadchecks();
		},'json');
	}
function toPage(page){
	loadItems({'s_blog.del':$('#show_state').val(),
		   's_blog.title':$('#s_title').val(),
			page:page,
			});
	$('#pageNow').val(page);
}
function searchItems(){
	loadItems({'s_blog.del':$('#show_state').val(),
			   's_blog.title':$('#s_title').val(),
				page:1,
				});
}
function loadFt(){
	$.post('${pageContext.request.contextPath}/back/type/ajaxFindType',{},function(res){
		var str = "";
		for(var i =0;i<res.typeList.length;i++){
			str+="<option value='"+res.typeList[i].id+"'>"+res.typeList[i].name+"</option>";
		}
		$('#select_type').html(str);
		$('.select').selectpicker('refresh');
	},'json');
	$.post('${pageContext.request.contextPath}/back/type/ajaxFindzhuanti',{},function(res){
		var str = "";
		str+="<option value='0' selected='selected'>无</option>";
		for(var i =0;i<res.ztList.length;i++){
			str+="<option value='"+res.ztList[i].id+"'>"+res.ztList[i].name+"</option>";
		}
		$('#select_zhuanti').html(str);
		$('.select').selectpicker('refresh');
	},'json');
}
$(function(){loadItems({'s_blog.del':false,page:1});loadFt();});
 </script>