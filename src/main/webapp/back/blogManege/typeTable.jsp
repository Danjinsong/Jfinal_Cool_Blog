<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Table Hover -->
<div class="col-md-6 ">
     <form class="form-inline" role="form" action="#">
       	<span style="float: right; font-size: 20px;" >共有<font color="red" id="total" >0</font>条数据</span>
       <div class="form-group">
           <input type="text" class="form-control input-sm" id="s_name" placeholder="输入关键字..." onkeydown="if(event.keyCode==13) searchItems(); return false;">
		   <button class="btn tooltips" data-placement="top"  onclick="searchItems(); return false;"><i class="sa-list-search"></i></button>
       </div>
       <div class="btn-group">
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="修改" onclick="updateItem();return false;"><i class="sa-list-move"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="删除" onclick="deleteOrComebackItem('true');return false;"><i class="sa-list-delete"></i></button>
		 <button class="btn tooltips" data-toggle="tooltip" data-placement="top" title="恢复" onclick="deleteOrComebackItem('false');return false;"><i class="sa-list-forwad"></i></button>
  		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='已删除'onclick="showDel('true');return false;"><i class='sa-list-archive'></i></button>
		 <button class='btn tooltips' data-toggle='tooltip' data-placement='top' title='未删除'onclick="showDel('false');return false;"><i class='sa-list-spam'></i></button>
 		<input type="hidden" id="show_state" value="false"/>
  	  </div>
    </form>
    <div class="table-responsive overflow">
        <table class="table table-bordered table-hover tile message-list list-container">
            <thead id="thead">
            </thead>
            <tbody id="tbody">
            </tbody>
            <tfoot id="tfoot">
            </tfoot>
        </table>
    </div>
</div>
<!-- 添加分类 -->
<div class="col-md-6">
	<div class="tile" id="basic" style="margin-top: 60px;">
    <h3 class="tile-title">添加类别</h3>
    <div class="tile p-15">
        <form role="form" >
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="name" placeholder="输入一个类别名称..">
            </div>
            <div class="form-group">
                 <select class="select" id='select'>
                 </select>
            </div>
            <div class="form-group">
                 <div class="radio">
                     <label>
                         <input type="radio" name="joinState" value="true">加入导航
                     </label>
                 </div>
                 <div class="radio">
                     <label>
                         <input type="radio" name="joinState" value="false">不加入导航
                     </label>
                 </div>
            </div>
            <button type="button" class="btn btn-sm m-t-10" onclick="save_Item();">提交</button>
            <button type="button" class="btn btn-sm m-t-10" onclick="resetVal();">重置</button>
        </form>
    </div>
    <div class="tile" id="basic" style="margin-top: 60px;">
    <h3 class="tile-title">添加专题</h3>
    <div class="tile p-15">
        <form role="form" >
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="zhuantiname" placeholder="输入一个专题名称..">
            </div>
            <button type="button" class="btn btn-sm m-t-10" onclick="save_zt_Item();">提交</button>
            <button type="button" class="btn btn-sm m-t-10" onclick="resetVal();">重置</button>
        </form>
    </div>
    <div class="tile" id="basic" style="margin-top: 60px;">
    <h3 class="tile-title">添加标签</h3>
    <div class="tile p-15">
        <form role="form" >
            <div class="form-group">
                <input type="text" class="form-control input-sm" id="biaoqianname" placeholder="输入一个标签名称..">
            </div>
            <button type="button" class="btn btn-sm m-t-10" onclick="save_bq_Item();">提交</button>
            <button type="button" class="btn btn-sm m-t-10" onclick="resetVal();">重置</button>
        </form>
    </div>
</div>
</div>
</div>
</div>
<div class="modal fade" id="typeModal" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="type_title"></h4>
             </div>
             <div class="modal-body">
			        <form role="form" >
			            <div class="form-group">
			                <input type="hidden" id='type_id' value="">
			                <input type="text" class="form-control input-sm" id="type_name" placeholder="输入一个类别名称.." value="">
			            </div>
			            <div class="form-group">
			                 <select class="select" id="select_type">
			                 </select>
			            </div>
			            <div class="form-group">
			                 <div class="radio">
			                     <label>
			                         <input type="radio" name="join" id="type_join_true" value="true">加入导航
			                     </label>
			                 </div>
			                 <div class="radio">
			                     <label>
			                         <input type="radio" name="join" id="type_join_false" value="false">不加入导航
			                     </label>
			                 </div>
			            </div>
			            <div class="form-group">
				            <div class="fileupload fileupload-new" data-provides="fileupload">
				            <div class="fileupload-preview thumbnail form-control"></div>
						     <div>
						         <span class="btn btn-file btn-alt btn-sm">
						             <span class="fileupload-new">选择图片</span>
						             <span class="fileupload-exists">更换</span>
						             <input type="file" id="image" name="image" onchange="javascript:uploadImage();return false;"/>
						       		 <input type="hidden" class="filesId" value="" id="typeFiles"/>
						         </span>
						         <a href="javascript:$('#image').val('');return false;" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">移除</a>
						     </div>
					     </div>
			            </div>
			        </form>
	             <div class="modal-footer">
	                 <button type="button" class="btn btn-sm" onclick="save_type_Item()" >提交</button>
	                 <button type="button" class="btn btn-sm" data-dismiss="modal">取消</button>
	             </div>
             </div>
         </div>
     </div>
 </div>
 <input type="hidden" value="1" id='pageNow'/>
<script>
function uploadImage(){
	upload('image',$('#image').val(),$('.filesId').val(),'',"/DJSBlog/back/files/ajaxSave?width=960&&height=540");
}
function showDel(del){
	$('#show_state').val(del);
	loadItems({'s_type.del':$('#show_state').val(),
		   's_type.name':$('#s_name').val(),
			page:1,
			});
	$('#pageNow').val(1);
}
function getChecked(){
	var checked = $("input:checked");
	var val=[];
	for(var i=0;i<checked.length;i++){
		if(checked.eq(i).val()!='on'&&checked.eq(i).val()!='false'&&checked.eq(i).val()!='true')
		val.push(checked.eq(i).val());
	}
	return val;
}
function deleteOrComebackItem(del){
	var val=getChecked();
	if(val.length==0){
		mes("温馨提示!","请选择元素进行操作!");
	}else{
		var str="";
		for(var i=0;i<val.length;i++){
				str+=val[i];
			if(i!=val.length-1)
				str+=",";
		}
		$.post('/DJSBlog/back/type/ajaxDeleteOrComeback',{ids:str,del:del},function(res){
			if(res.success){
				mes("系统提示","操作成功");
				loadItems({'s_type.del':$('#show_state').val(),page:$('#pageNow').val()});				
			}
			else
				mes("系统提示","操作失败");
		},'json');
	}
}
function updateItem(){
	var val=getChecked();
	if(val.length!=1){
		mes("温馨提示!","请选择一行元素进行修改!");
	}else{
		loadFa();$('.fileupload-preview').html("");
		$.post('/DJSBlog/back/type/ajaxGetById',{id:val[0]},function(res){
			if(res.type.type_type != 'g_zhuanti' && res.type.type_type != 'g_biaoqian'){
				$('#type_title').html("修改类别");
				$('#type_name').val(res.type.name);
				$('#type_id').val(res.type.id);
				if(res.type.type_type=='g_father'){
					$('#type_join_true').iCheck('check');
				}else {
					$('#select_type').val(res.type.type_type);
					$('#type_join_false').iCheck('check');
				}
				if(res.type.photo!=null&&res.type.photo!=''){
					$.post('/DJSBlog/back/files/ajaxGetById',{id:res.type.photo},function(data){
						$('.filesId').val(data.files.id);
						$('.fileupload-preview').html("<img src='/DJSBlog"+data.files.src+"'/>");
					});
					$('.filesId').val(res.type.photo);
				}
				$('#typeModal').modal('show');
			}
			else{
				mes("温馨提示!","这是一条专题或者标签，无法修改具体信息");
			}
		},'json');
	}
}
function resetVal(){
	$(':input','form').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected'); 
	$('select').val(0);
	$('.fileupload-preview').html("");
}
function save_zt_Item(){
	if($('#zhuantiname').val() == '') mes("系统提示","专题名称不能为空");
	else saveItem({'type.name':$('#zhuantiname').val(),'type.type_type':"g_zhuanti"});
}
function save_bq_Item(){
	if($('#biaoqianname').val() == '') mes("系统提示","标签名称不能为空");
	else saveItem({'type.name':$('#biaoqianname').val(),'type.type_type':"g_biaoqian"});
}
function save_Item(){
	var joinState = $('.radio input[name="joinState"]:checked ').val();
	saveItem({'type.name':$('#name').val(),'joinState':joinState,'type.type_type':$('#select').val()});
}
function save_type_Item(){
	var joinState = $('.radio input[name="join"]:checked ').val();
	saveItem({'type.name':$('#type_name').val(),
		'joinState':joinState,
		'type.id':$('#type_id').val(),
		'type.type_type':$('#select_type').val(),
		'type.photo':$('#typeFiles').val()});
	$('.filesId').eq(0).val('');
}
function saveItem(parm){
	$.post('/DJSBlog/back/type/ajaxSave',parm,function(res){
		if(res.success){
			$('#typeModal').modal('hide');
			mes("温馨提示!","操作成功");
			loadItems({'s_type.del':false,page:$('#pageNow').val()});	
		}
		else
			mes("温馨提示!","操作失败,分类已经存在");
	},'json');
}
function toPage(page){
	loadItems({'s_type.del':$('#show_state').val(),
		   's_type.name':$('#s_name').val(),
			page:page
			});
	$('#pageNow').val(page);
}
function loadItems(parm){
	$.post('/DJSBlog/back/type/ajaxList',parm,function(res){
		$('#thead').html(res.thead);
		$('#tbody').html(res.tbody);
		$('#tfoot').html(res.tfoot);
		$('#total').html(res.total);
		loadchecks();
	},'json');
}
function searchItems(){
	loadItems({'s_type.del':$('#show_state').val(),
			   's_type.name':$('#s_name').val(),
				page:1,
				});
}
function loadFa(){
	$.post('/DJSBlog/back/type/ajaxFindFa',{},function(res){
		var str = "";
		str+="<option selected='selected'value='0'>选择一个父节点</option>";
		for(var i =0;i<res.typeList.length;i++){
			str+="<option value='"+res.typeList[i].id+"'>"+res.typeList[i].name+"</option>";
		}
		$('#select').html(str);
		$('#select_type').html(str);
	    $('.select').selectpicker('refresh');
	},'json');
}
$(function(){loadItems({'s_type.del':false,page:1});loadFa();});
</script>