<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Buttons -->
<div class="block-area" id="button">
    <h3 class="block-title">Button Checkbox and Radio</h3>
    
    <br/>
    
    <p>Checkbox</p>

    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-gr-gray btn-sm">
            <input type="checkbox" /> Option 1
        </label>
        <label class="btn btn-gr-gray btn-sm">
            <input type="checkbox" /> Option 2
        </label>
        <label class="btn btn-gr-gray btn-sm">
            <input type="checkbox" /> Option 3
        </label>
    </div>
    
    <p></p>
    <p>Radio</p>
    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-gr-gray btn-sm">
            <input type="radio" name="options" id="option1" /> Option 1
        </label>
        <label class="btn btn-gr-gray btn-sm">
            <input type="radio" name="options" id="option2" /> Option 2
        </label>
        <label class="btn btn-gr-gray btn-sm">
            <input type="radio" name="options" id="option3" /> Option 3
        </label>
    </div>
</div>

<hr class="whiter m-t-20" />
