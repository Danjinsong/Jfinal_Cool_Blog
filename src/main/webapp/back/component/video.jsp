<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Video -->
<div class="tile">
    <h2 class="tile-title">播放器</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Edit</a></li>
            <li><a href="">Delete</a></li>
        </ul>
    </div>
    <video width="100%" height="100%" id="multiCodec" poster="${pageContext.request.contextPath }/back/img/media-player/media-player-poster.jpg" controls="controls" preload="none"> <!-- id could be any according to you -->
	<!-- MP4 source must come first for iOS -->
	<source type="video/mp4" src="${pageContext.request.contextPath }/back/media/echohereweare.mp4" />
	<!-- WebM for Firefox 4 and Opera -->
	<source type="video/webm" src="${pageContext.request.contextPath }/back/media/echohereweare.webm" />
	<!-- OGG for Firefox 3 -->
	<source type="video/ogg" src="${pageContext.request.contextPath }/back/media/echohereweare.ogv" />
	<!-- Fallback flash player for no-HTML5 browsers with JavaScript turned off -->
	<object width="100%" height="100%" type="application/x-shockwave-flash" data="media/flashmediaelement.swf"> 		
        <param name="movie" value="${pageContext.request.contextPath }/back/media/flashmediaelement.swf" /> 
        <param name="flashvars" value="controls=true&amp;poster=${pageContext.request.contextPath }/back/img/media-player/media-player-poster.jpg&amp;file=${pageContext.request.contextPath }/back/media/echohereweare.mp4" /> 		
        <!-- Image fall back for non-HTML5 browser with JavaScript turned off and no Flash player installed -->
                <img src="${pageContext.request.contextPath }/back/img/media-player/media-player-poster.jpg" width="100%" height="100%" alt="Media" title="No video playback capabilities" />
        </object> 	
    </video>
</div>
<!-- Media -->
<script src="${pageContext.request.contextPath }/back/js/media-player.min.js"></script> <!-- Video Player -->