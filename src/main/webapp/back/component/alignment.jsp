<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Alignment Classes -->
<article id="alignmentClasses" class="block-area">
    <h3 class="block-title">对齐组件</h3>
    <p class="text-left">左对齐
    	李青是一个近战战士型英雄，拥有很高的机动性和爆发力，单挑和小规模团战能力很强，同时李青也是非常优秀的打野英雄，非常擅长野区的遭遇战和Gank，是非常致命的英雄人物。
	</p>    
    <p class="text-center">中对齐
    	李青是一个近战战士型英雄，拥有很高的机动性和爆发力，单挑和小规模团战能力很强，同时李青也是非常优秀的打野英雄，非常擅长野区的遭遇战和Gank，是非常致命的英雄人物。
	</p>    

    <p class="text-right">右对齐
    	李青是一个近战战士型英雄，拥有很高的机动性和爆发力，单挑和小规模团战能力很强，同时李青也是非常优秀的打野英雄，非常擅长野区的遭遇战和Gank，是非常致命的英雄人物。
	</p>    
    <br/>
</article>

<hr class="whiter" />    