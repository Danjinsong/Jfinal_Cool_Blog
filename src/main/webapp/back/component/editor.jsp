<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Quick Posting -->
    <div class="tile">
        <h2 class="tile-title">编辑器组件</h2>
        <div class="tile-config dropdown">
            <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
            <ul class="dropdown-menu pull-right text-right">
                <li><a href="">Edit</a></li>
                <li><a href="">Delete</a></li>
            </ul>
        </div>
        
        <form role="form" class="p-15">
            <div class="form-group m-b-15">
                <label>标题</label>
                <input type="text" class="form-control input-sm">
            </div>
            
            <div class="form-group m-b-15">
                <label>下拉栏</label>
                <select class="select">
                    <option>Default</option>
                    <option>Toyota Avalon</option>
                    <option>Toyota Crown</option>
                    <option>Lexus LX570</option>
                </select>
            </div>
            
            <div class="form-group m-b-15">
                <label>发送文本</label>
                <div class="wysiwye-editor"></div>
            </div>
            <button type="submit" class="btn btn-sm">提交</button>
            <button type="submit" class="btn btn-sm">清空</button>
            
        </form>
    </div>
    

<script src="${pageContext.request.contextPath }/back/js/editor.min.js"></script> <!-- WYSIWYG Editor -->