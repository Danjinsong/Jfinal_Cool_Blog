<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Custom Select -->
<div class="block-area" id="custom-select">
    
    <h3 class="block-title">Custom Select</h3>
    
    <br/>
    
    <p>Custom Select boxes based on Bootstrap Dropdowns</p>
    
    <div class="row">
        <div class="col-md-2 m-b-15">
            <select class="select">
                <option>Default</option>
                <option>Toyota Avalon</option>
                <option>Toyota Crown</option>
                <option>Lexus LX570</option>
            </select>
        </div>
        
        <div class="col-md-2 m-b-15">
            <select class="select">
                <optgroup label="Toyota">
                    <option>Grouped</option>
                    <option>Toyota Avalon</option>
                    <option>Toyota Crown</option>
                    <option>Toyota FT86 </option>
                </optgroup>
                 <optgroup label="Lexus">
                    <option>Lexus LS600</option>
                    <option>Lexus LFA</option>
                    <option>Lexus LX570</option>
                </optgroup>
            </select>
        </div>
        
        <div class="col-md-2 m-b-15">
            <select class="select">
                <option>Divider</option>
                <option>Toyota Avalon</option>
                <option>Toyota Crown</option>
                <option>Toyota FT86 </option>
                <option data-divider="true">&nbsp;</option>
                <option>Lexus LS600</option>
                <option>Lexus LFA</option>
                <option>Lexus LX570</option>
            </select>
        </div>
        
        <div class="col-md-2 m-b-15">
            <select class="select">
                <option>Option Disabled</option>
                <option>Toyota Avalon</option>
                <option disabled="disabled">Toyota Crown</option>
                <option>Lexus LX570</option>
            </select>
        </div>
        
        <div class="col-md-2 m-b-15">
            <select class="select">
                <option data-icon="fa fa-comment">With Icon</option>
                <option data-icon="fa fa-flickr">Toyota FT86</option>
                <option data-icon="fa fa-heart">Toyota Crown</option>
                <option data-icon="fa fa-star">Lexus LX570</option>
            </select>
        </div>
    </div>
    
    <p>Multiple Selects</p>
    <div class="row">
        <div class="col-md-5 m-b-15">
            <select class="select" multiple>
                <option>Toyota Avalon</option>
                <option>Toyota Crown</option>
                <option>Toyota FT86 </option>
                <option>Lexus LS600</option>
                <option>Lexus LFA</option>
                <option>Lexus LX570</option>
            </select>
        </div>
        
        <div class="col-md-5 m-b-15">
            <select class="select" multiple data-selected-text-format="count>3">
                <option>Toyota Avalon</option>
                <option>Toyota Crown</option>
                <option>Toyota FT86 </option>
                <option>Lexus LS600</option>
                <option>Lexus LFA</option>
                <option>Lexus LX570</option>
            </select>
        </div>
    </div>
</div>

<hr class="whiter m-t-20" />
 <!-- Tag Selection -->
<div class="block-area" id="tag-select">
    <h3 class="block-title">Tag Selection</h3>
    
    <br/>
    
    <p>Default Tag selector</p>
    <select data-placeholder="Select Users..." class="tag-select" multiple>
        <option value="David Becham">David Becham</option> 
        <option value="Christian Bale">Christian Bale</option> 
        <option value="Malinda Hollaway">Malinda Hollaway</option> 
        <option value="Jason Stathom">Jason Stathom</option> 
        <option value="Wen De Soza">Wen De Soza</option> 
        <option value="Jhon Morrison">Jhon Morrison</option> 
        <option value="William Gale">William Gale</option> 
        <option value="Mark Hakngtosh">Mark Hakngtosh</option> 
        <option value="Angola Jenolia">Angola Jenolia</option> 
        <option value="William Jansen">William Jansen</option> 
        <option value="Kat Steven">Kat Steven</option> 
        <option value="Henry Hadson">Henry Hadson</option> 
        <option value="Joshep Fernandez">Joshep Fernandez</option> 
        <option value="Armani Jens">Armani Jens</option> 
        <option value="Wen Diopal">Wen Diopal</option> 
        <option value="Aura Moringson">Aura Moringson</option> 
        <option value="Peter Robinson">Peter Robinson</option> 
        <option value="Dave Watmore">Dave Watmore</option> 
        <option value="Jordan Orlendo">Jordan Orlendo</option> 
        <option value="Christopher Nolen">Christopher Nolen</option> 
    </select>
    
    <br/><br/>
    
    <p>Disabled Options</p>
    <select data-placeholder="Select Users..." class="tag-select" multiple>
        <option value="David Becham" disabled>David Becham</option> 
        <option value="Christian Bale">Christian Bale</option> 
        <option value="Malinda Hollaway" disabled>Malinda Hollaway</option> 
        <option value="Jason Stathom" disabled>Jason Stathom</option> 
        <option value="Wen De Soza">Wen De Soza</option> 
        <option value="Jhon Morrison">Jhon Morrison</option> 
        <option value="William Gale">William Gale</option> 
        <option value="Mark Hakngtosh" disabled>Mark Hakngtosh</option> 
        <option value="Angola Jenolia" disabled>Angola Jenolia</option> 
        <option value="William Jansen">William Jansen</option>
        <option value="Kat Steven">Kat Steven</option> 
        <option value="Henry Hadson">Henry Hadson</option> 
        <option value="Joshep Fernandez" disabled>Joshep Fernandez</option> 
        <option value="Armani Jens" disabled>Armani Jens</option> 
        <option value="Wen Diopal">Wen Diopal</option> 
        <option value="Aura Moringson">Aura Moringson</option> 
        <option value="Peter Robinson">Peter Robinson</option> 
        <option value="Dave Watmore">Dave Watmore</option> 
        <option value="Jordan Orlendo" disabled>Jordan Orlendo</option> 
        <option value="Christopher Nolen">Christopher Nolen</option> 
    </select>
    
    <br/><br/>
    
    <p>Limit Selected Options(Set to 5)</p>
    <select data-placeholder="Select Users..." class="tag-select-limited" multiple>
        <option value="David Becham">David Becham</option> 
        <option value="Christian Bale">Christian Bale</option> 
        <option value="Malinda Hollaway">Malinda Hollaway</option> 
        <option value="Jason Stathom">Jason Stathom</option> 
        <option value="Wen De Soza">Wen De Soza</option> 
        <option value="Jhon Morrison">Jhon Morrison</option> 
        <option value="William Gale">William Gale</option> 
        <option value="Mark Hakngtosh">Mark Hakngtosh</option> 
        <option value="Angola Jenolia">Angola Jenolia</option> 
        <option value="William Jansen">William Jansen</option>
        <option value="Kat Steven">Kat Steven</option> 
        <option value="Henry Hadson">Henry Hadson</option> 
        <option value="Joshep Fernandez">Joshep Fernandez</option> 
        <option value="Armani Jens" >Armani Jens</option> 
        <option value="Wen Diopal">Wen Diopal</option> 
        <option value="Aura Moringson">Aura Moringson</option> 
        <option value="Peter Robinson">Peter Robinson</option> 
        <option value="Dave Watmore">Dave Watmore</option> 
        <option value="Jordan Orlendo">Jordan Orlendo</option> 
        <option value="Christopher Nolen">Christopher Nolen</option> 
    </select>
</div>

<hr class="whiter m-t-20" />
