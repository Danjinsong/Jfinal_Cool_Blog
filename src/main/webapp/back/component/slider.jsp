<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Slider -->
<div class="block-area" id="slider">
    <h3 class="block-title">Input Slider</h3>
    
    <br/>
    
    <p>Defaul Input Slidert</p>
    <input type="text" class="input-slider">
    <p></p>
    
    <p>Advanced - Orientation, Min Value, Max Value, Default Value, Increment Step and Slider Selection</p>
    <input type="text" class="input-slider" data-slider-min="-20" data-slider-max="20" data-slider-step="1" data-slider-value="-14" data-slider-orientation="horizontal" data-slider-selection="after">
    <p></p>
    
    <p>Range Selector</p>
    <input type="text" class="input-slider" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]">
    <p></p>
    
    <p>No Tooltip</p>
    <input type="text" class="input-slider" data-slider-tooltip="hide">
    <p></p>
    
    <p>Out put value to a text box</p>
    <div class="slider-container">
        <input type="text" class="input-slider" data-slider-min="0" data-slider-max="2000" data-slider-value="800">
        <p></p>
        
        <div class="row">
            <div class="col-md-2 pull-right">
                <input type="text" value="800" class="form-control input-sm slider-value">
            </div>
        </div>
    </div>
    <p></p>
    
    <p>Vertical Slider</p>
    <input type="text" class="input-slider" value="" data-slider-min="-20" data-slider-max="20" data-slider-value="-15" data-slider-orientation="vertical" data-slider-selection="after">
    <input type="text" class="input-slider" value="" data-slider-min="-20" data-slider-max="20" data-slider-value="14" data-slider-orientation="vertical" data-slider-selection="after">
    <input type="text" class="input-slider" value="" data-slider-min="-20" data-slider-max="20" data-slider-value="-2" data-slider-orientation="vertical" data-slider-selection="after">
    <input type="text" class="input-slider" value="" data-slider-min="-20" data-slider-max="20" data-slider-value="-11" data-slider-orientation="vertical" data-slider-selection="after">
    <input type="text" class="input-slider" value="" data-slider-min="-20" data-slider-max="20" data-slider-value="8" data-slider-orientation="vertical" data-slider-selection="after">
</div>


<hr class="whiter m-t-20" />
