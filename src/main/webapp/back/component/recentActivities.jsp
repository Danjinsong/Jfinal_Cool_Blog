<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Recent Activities -->
<div class="tile">
    <h2 class="tile-title">最近聊天消息组件</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Edit</a></li>
            <li><a href="">Delete</a></li>
        </ul>
    </div>
    
    <div class="listview narrow">
        <div class="media">
            <div class="pull-right">
                <img width="37" src="img/profile-pics/1.jpg" alt="">
            </div>
            <div class="media-body">
                Connected with <a class="news-title underline" href="">Mitchell Christein</a>
                <div class="clearfix"></div>
                <small class="muted">30 Minutes ago</small>
            </div>
        </div>
        <div class="media">
            <div class="pull-right">
                <img width="37" src="img/profile-pics/4.jpg" alt="">
            </div>
            <div class="media-body">
                <a class="news-title underline" href="">Wayne Cerolina</a> Accepted your friend request
                <div class="clearfix"></div>
                <small class="muted">2 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="media-body">
                <a class="news-title" href="">Uploaded 7 files to DOCS folder</a>
                <div class="clearfix"></div>
                <small class="muted">3 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="media-body">
                Joined the Group <a class="news-title underline" href="">'90s Rock Hits'</a>
                <div class="clearfix"></div>
                <small class="muted">5 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="pull-right">
                <img width="37" src="img/profile-pics/5.jpg" alt="">
            </div>
            <div class="media-body">
                Wrote on <a class="news-title underline" href="">David Villa's</a> Wall
                <div class="clearfix"></div>
                <small class="muted">8 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="media-body">
                Signed up an Affiliate
                <div class="clearfix"></div>
                <small class="muted">6 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="media-body">
                Completed and submited the project <a class="news-title underline" href="">Joomla</a>
                <div class="clearfix"></div>
                <small class="muted">12 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="media-body">
                Travelled to <a class="news-title underline" href="">San Fransisco</a>, United States
                <div class="clearfix"></div>
                <small class="muted">12 Hours ago</small>
            </div>
        </div>
        <div class="media">
            <div class="pull-right">
                <img width="37" src="img/profile-pics/6.jpg" alt="">
            </div>
            <div class="media-body">
                Conneted with <a class="news-title underline" href="">Emma Stone</a>
                <div class="clearfix"></div>
                <small class="muted">2 Days ago</small>
            </div>
        </div>
        <div class="media p-5 text-center l-100">
            <a href=""><small>VIEW ALL</small></a>
        </div>
    </div>
</div>