<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Shortcuts -->
<div class="block-area shortcut-area">
	<a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/money.png" alt=""> <small class="t-overflow">美元</small>
	</a> <a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/twitter.png" alt=""> <small class="t-overflow">推特</small>
	</a> <a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/calendar.png" alt=""> <small class="t-overflow">日程表</small>
	</a> <a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/stats.png" alt=""> <small class="t-overflow">设计图</small>
	</a> <a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/connections.png" alt=""> <small class="t-overflow">wifi</small>
	</a> <a class="shortcut tile" href=""> <img
		src="${pageContext.request.contextPath }/back/img/shortcuts/reports.png" alt=""> <small class="t-overflow">报告</small>
	</a>
</div>

<hr class="whiter" />