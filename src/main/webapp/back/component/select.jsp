<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>


<!-- Select -->
<div class="block-area" id="select">
    <h3 class="block-title">Select</h3>
    <p>Use Grid classes to adjust column sizes</p>
    
    <select class="form-control input-lg m-b-10">
        <option>Option 1</option>
        <option>Option 2</option>
        <option>Option 3</option>
    </select>
    
    <select class="form-control m-b-10">
        <option>Option 1</option>
        <option>Option 2</option>
        <option>Option 3</option>
    </select>
    
    <select class="form-control input-sm m-b-10">
        <option>Option 1</option>
        <option>Option 2</option>
        <option>Option 3</option>
    </select>
    
    <div class="select-container"> <!-- Hack to hide scrollbars -->
        <select multiple class="form-control">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>

    <p></p>
    <p>Disabled</p>
    <select class="form-control" disabled>
        <option>Option 1</option>
        <option>Option 2</option>
        <option>Option 3</option>
    </select>
</div>

<hr class="whiter m-t-20" />
