<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- NUM Spinner -->
<div class="block-area" id="spinner">
    <h3 class="block-title">Spinner</h3>
    
    <br/>

    <div class="row">
        <div class="col-md-4 m-b-15">
            <p>Basic</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-1 spinedit" />
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>Set Value</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-2 spinedit" />
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>Set Minimum(-10)</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-3 spinedit" />
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-4 m-b-15">
            <p>Set Maximum(10)</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-4 spinedit" />
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>Set Step(100)</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-5 spinedit" />
            </div>
        </div>

        <div class="col-md-4 m-b-15">
            <p>Decimal</p>
            <div class="p-relative">
                <input type="text" class="form-control input-sm spinner-6 spinedit" />
            </div>
        </div>
    </div>

</div>

<hr class="whiter m-t-20" />
