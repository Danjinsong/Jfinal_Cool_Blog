<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Works -->
<div class="tile">
    <h2 class="tile-title">工作进度表</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Edit</a></li>
            <li><a href="">Delete</a></li>
        </ul>
    </div>
    
    <div class="p-10">
        <div class="m-b-10">
            Joomla CMS website for Lexus Inc. - 60%
            <div class="progress progress-striped progress-alt">
                <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"></div>
        </div>
    </div>
    
    <div class="m-b-10">
        Lotus Design's AMC updates - 90%
        <div class="progress progress-striped progress-alt">
            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%"></div>
        </div>
    </div>    
    
    <div class="m-b-10">
        Chrome Extension developement - 33%
        <div class="progress progress-striped progress-alt">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="33" aria-valuemin="0" aria-valuemax="100" style="width: 33%"></div>
        </div>
    </div>
    
    <div class="m-b-10">
        Wireframes for new website - 50%
        <div class="progress progress-striped progress-alt">
            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 50%"></div>
        </div>
    </div>
    
    <div>
        Wordpress Website & Plugin - 99%
        <div class="progress progress-striped progress-alt">
            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="99" aria-valuemin="0" aria-valuemax="100" style="width: 99%"></div>
            </div>
        </div>
    </div>
</div>