<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Text Input -->
 <div class="block-area" id="text-input">
     <h3 class="block-title">Text Input</h3>
     
     <p>Text Inputs with different sizes by height and column.</p>
     
     <input class="form-control input-lg m-b-10" type="text" placeholder=".input-lg">
     <input type="text" class="form-control m-b-10" placeholder="Default">
     <input class="form-control input-sm m-b-10" type="text" placeholder=".input-sm">
         
     <div class="row">
         <div class="col-lg-2">
             <input type="text" class="form-control m-b-10" placeholder=".col-lg-2">
         </div>
         <div class="col-lg-3">
             <input type="text" class="form-control m-b-10" placeholder=".col-lg-3">
         </div>
         <div class="col-lg-4">
             <input type="text" class="form-control m-b-10" placeholder=".col-lg-4">
         </div>
         <div class="col-lg-5">
             <input type="text" class="form-control m-b-10" placeholder=".col-lg-5">
         </div>
         <div class="col-lg-6">
             <input type="text" class="form-control m-b-10" placeholder=".col-lg-6">
         </div>
         <div class="clearfix"></div>
     </div>
     <p></p>
     <p>Input focus</p>
     <input class="form-control input-focused" type="text" value="This is focused...">
     <p></p>
     <p>Disabled</p>
     <input class="form-control" type="text" value="This is disabled..." disabled>
 </div>
 
<hr class="whiter m-t-20" />