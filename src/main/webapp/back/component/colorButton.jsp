<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Color Picker -->
<div class="block-area" id="color-picker">
    <h3 class="block-title">Color Picker</h3>
    
    <br/>
    
    <div class="row">
        <div class="col-md-4 m-b-15">
            <p>Default - hex</p>
            <div class="color-pick input-icon">
                <input class="form-control color-picker input-sm" type="text" />
                <span class="color-preview"></span>
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>RGB</p>
            <div class="color-pick input-icon">
                <input class="form-control color-picker-rgb input-sm" type="text" />
                <span class="color-preview"></span>
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>RGBA</p>
            <div class="color-pick input-icon">
                <input class="form-control color-picker-rgba input-sm" type="text" />
                <span class="color-preview"></span>
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<hr class="whiter m-t-20" />
