<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- About me -->
<div class="tile">
    <h2 class="tile-title">有关我</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Edit</a></li>
            <li><a href="">Delete</a></li>
        </ul>
    </div>
    
    <div class="listview icon-list">
        <div class="media">
            <i class="icon pull-left">&#61744;</i>
            <div class="media-body">Software Developer at Google</div>
        </div>
        
        <div class="media">
            <i class="icon pull-left">&#61753;</i>
            <div class="media-body">Studied at Oxford University</div>
        </div>
        
        <div class="media">
            <i class="icon pull-left">&#61713;</i>
            <div class="media-body">Lives in Saturn, Milkyway</div>
        </div>
        
        <div class="media">
            <i class="icon pull-left">&#61742;</i>
            <div class="media-body">From Titan, Jupitor</div>
        </div>
    </div>
</div>