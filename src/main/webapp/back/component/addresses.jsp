<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Addresses -->
<article class="block-area" id="addresses">
    <h3 class="block-title">地址</h3>
    <address>
        <strong>我是加粗字体</strong><br>
        	黑龙江科技大学<br>
        	迪拜, UAE<br>
        <abbr title="Phone">P:</abbr> (123) 456-7890
    </address>
    
    <address>
        <strong>Full Name</strong><br>
        <a href="mailto:#">first.last@example.com</a>
    </address>
</article>

<hr class="whiter" />