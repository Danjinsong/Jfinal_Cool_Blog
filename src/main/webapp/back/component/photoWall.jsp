<%@ page language="java"  import="java.util.*" pageEncoding="UTF-8"%>
<!-- Photos -->
<div class="tile">
    <h2 class="tile-title">照片墙</h2>
    <div class="tile-config dropdown">
        <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
        <ul class="dropdown-menu pull-right text-right">
            <li><a href="">Edit</a></li>
            <li><a href="">Delete</a></li>
        </ul>
    </div>
    
    <div class="p-5 photos">
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/1.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/2.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/3.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/4.jpg"  alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/5.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/6.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/2.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/5.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/1.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/3.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/4.jpg" alt="">
        </div>
        <div class="col-xs-3">
            <img src="${pageContext.request.contextPath }/back/img/profile-pics/6.jpg" alt="">
        </div>
        
        <div class="clearfix"></div>
    </div>
</div>