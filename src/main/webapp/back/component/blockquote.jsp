<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Blockquote  -->
<article class="block-area">
    <h3 class="block-title">引用  </h3>
    <blockquote>
        <p>"一人之行可灭世，众人之勤可救世！"——李青</p>
        <i class="icon-quote-right"></i>
    </blockquote>
</article>

<article class="block-area">
    <h3 class="block-title">引用</h3>
    <blockquote>
        <p>"一人之行可灭世，众人之勤可救世！"</p>
        <small>李青<cite title="Source Title">《LOL》</cite></small>
        <i class="icon-quote-right"></i>
    </blockquote>
</article>

<hr class="whiter" />