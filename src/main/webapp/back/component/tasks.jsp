<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Tasks to do -->
	<div class="tile">
		<h2 class="tile-title">任务</h2>
		<div class="tile-config dropdown">
			<a data-toggle="dropdown" href="" class="tile-menu"></a>
			<ul class="dropdown-menu pull-right text-right">
				<li id="todo-add"><a href="">添加</a></li>
				<li id="todo-refresh"><a href="">刷新</a></li>
				<li id="todo-clear"><a href="">清除所有</a></li>
			</ul>
		</div>

		<div class="listview todo-list sortable">
			<div class="media">
				<div class="checkbox m-0">
					<label class="t-overflow"> <input type="checkbox">
						为了构建社会主义和谐社会
					</label>
				</div>
			</div>
			<div class="media">
				<div class="checkbox m-0">
					<label class="t-overflow"> <input type="checkbox">
						为了构建社会主义和谐社会
					</label>
				</div>

			</div>
		</div>

		<h2 class="tile-title">已完成任务</h2>

		<div class="listview todo-list sortable">
			<div class="media">
				<div class="checkbox m-0">
					<label class="t-overflow"> <input type="checkbox"
						checked="checked"> 构建社会主义和谐社会
					</label>
				</div>

			</div>
			<div class="media">
				<div class="checkbox m-0">
					<label class="t-overflow"> <input type="checkbox"
						checked="checked"> 构建社会主义和谐社会
					</label>
				</div>

			</div>
		</div>
	</div>
