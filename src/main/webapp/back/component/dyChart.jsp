<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Dynamic Chart -->
<div class="tile">
	<h2 class="tile-title">动态图表</h2>
	<div class="tile-config dropdown">
		<a data-toggle="dropdown" href="" class="tile-menu"></a>
		<ul class="dropdown-menu pull-right text-right">
			<li><a href="">刷新</a></li>
			<li><a href="">设置</a></li>
		</ul>
	</div>

	<div class="p-t-10 p-r-5 p-b-5">
		<div id="dynamic-chart" style="height: 200px"></div>
	</div>

</div>