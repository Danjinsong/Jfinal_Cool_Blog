<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Input Masking -->
<div class="block-area" id="input-masking">
    
    <h3 class="block-title">Input Masking</h3>
    
    <br/>
    
    <p>Start typing on feilds below, It will automatically mask the inputs</p>
    
    <div class="row">
        <div class="col-md-3 m-b-15">
            <label>Date</label>
            <input type="text" class="input-sm form-control mask-date" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Time</label>
            <input type="text" class="input-sm form-control mask-time" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Date Time</label>
            <input type="text" class="input-sm form-control mask-date_time" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>CEP</label>
            <input type="text" class="input-sm form-control mask-cep" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Phone Number</label>
            <input type="text" class="input-sm form-control mask-phone" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Phone with Odd</label>
            <input type="text" class="input-sm form-control mask-phone_with_ddd" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>US Phone Number</label>
            <input type="text" class="input-sm form-control mask-phone_us" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Mixed</label>
            <input type="text" class="input-sm form-control mask-mixed" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>CPF</label>
            <input type="text" class="input-sm form-control mask-cpf" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Money</label>
            <input type="text" class="input-sm form-control mask-money" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Money 2</label>
            <input type="text" class="input-sm form-control mask-money2" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>IP Address</label>
            <input type="text" class="input-sm form-control mask-ip_address" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Percentage</label>
            <input type="text" class="input-sm form-control mask-percent" placeholder="...">
        </div>
        
        <div class="col-md-3 m-b-15">
            <label>Credit Card</label>
            <input type="text" class="input-sm form-control mask-credit_card" placeholder="...">
        </div>
        
    </div>
</div>

<hr class="whiter m-t-20" />
