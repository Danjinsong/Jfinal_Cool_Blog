<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- File Upload -->
<div class="block-area" id="upload">
    <h3 class="block-title">Simple File Upload</h3>
    
    <br/>
    
    <p>Default with button only</p>
    
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <span class="btn btn-file btn-sm btn-alt">
            <span class="fileupload-new">Select file</span>
            <span class="fileupload-exists">Change</span>
            <input type="file" />
        </span>
        <span class="fileupload-preview"></span>
        <a href="#" class="close close-pic fileupload-exists" data-dismiss="fileupload">
            <i class="fa fa-times"></i>
        </a>
    </div>
    
    <br/>
    
    <p>With remove button</p>
    <div class="fileupload fileupload-new row" data-provides="fileupload">
        <div class="input-group col-md-6">
            <div class="uneditable-input form-control">
                <i class="fa fa-file m-r-5 fileupload-exists"></i>
                <span class="fileupload-preview"></span>
            </div>
            <div class="input-group-btn">
                <span class="btn btn-file btn-alt btn-sm">
                <span class="fileupload-new">Select file</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" />
            </span>
            </div>

            <a href="#" class="btn btn-sm btn-gr-gray fileupload-exists" data-dismiss="fileupload">Remove</a>
        </div>
    </div>
    
    <br/>
    
    <p>Image Preview</p>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail form-control"></div>
        
        <div>
            <span class="btn btn-file btn-alt btn-sm">
                <span class="fileupload-new">Select image</span>
                <span class="fileupload-exists">Change</span>
                <input type="file" />
            </span>
            <a href="#" class="btn fileupload-exists btn-sm" data-dismiss="fileupload">Remove</a>
        </div>
    </div>
    
    <br/>
    
    <p>Another preview option</p>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-new thumbnail small form-control"></div>
        <div class="fileupload-preview form-control fileupload-exists thumbnail small"></div>
        <span class="btn btn-file btn-alt btn-sm">
            <span class="fileupload-new">Select image</span>
            <span class="fileupload-exists">Change</span>
            <input type="file" />
        </span>
        <a href="#" class="btn-sm btn fileupload-exists" data-dismiss="fileupload">Remove</a>
    </div>
    
    <br/><br/><br/>
</div>