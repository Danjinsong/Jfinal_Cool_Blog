<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="block-area">
	<div class="row">
		<div class="col-md-8">
			<!-- 折线图组件 -->
			<jsp:include page="/back/component/line.jsp"></jsp:include>
			<jsp:include page="/back/component/pie.jsp"></jsp:include>
			<div class="row">
				<div class="col-md-6">
					<!-- 最近联系人组件 -->
					<jsp:include page="/back/component/recentPostings.jsp"></jsp:include>
				</div>
				<div class="col-md-6">
					<!-- tasks组件 -->
					<jsp:include page="/back/component/tasks.jsp"></jsp:include>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-4">
			<!-- USA地图组件  不会用 -->
			<!-- 动态图表 -->
			<jsp:include page="/back/component/dyChart.jsp"></jsp:include>
			<!-- activity组件 -->
			<jsp:include page="/back/component/activity.jsp"></jsp:include>
		</div>
		<div class="clearfix"></div>
	</div>

</div>