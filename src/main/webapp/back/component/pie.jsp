<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Pies -->
<div class="tile text-center">
	<div class="tile-dark p-10">
		<div class="pie-chart-tiny" data-percent="86">
			<span class="percent"></span> <span class="pie-title">
			日本人数 <i class="m-l-5 fa fa-retweet"></i>
			</span>
		</div>
		<div class="pie-chart-tiny" data-percent="23">
			<span class="percent"></span> <span class="pie-title">
			韩国人数<i class="m-l-5 fa fa-retweet"></i>
			</span>
		</div>
		<div class="pie-chart-tiny" data-percent="57">
			<span class="percent"></span> <span class="pie-title">
				怀孕概率 <i class="m-l-5 fa fa-retweet"></i>
			</span>
		</div>
		<div class="pie-chart-tiny" data-percent="34">
			<span class="percent"></span> <span class="pie-title">
				生孩子概率 <i class="m-l-5 fa fa-retweet"></i>
			</span>
		</div>
		<div class="pie-chart-tiny" data-percent="81">
			<span class="percent"></span> <span class="pie-title">
				盖伦 <i class="m-l-5 fa fa-retweet"></i>
			</span>
		</div>
	</div>
</div>
