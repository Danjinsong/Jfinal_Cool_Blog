<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--  Recent Postings -->
	<div class="tile">
		<h2 class="tile-title">Recent Postings</h2>
		<div class="tile-config dropdown">
			<a data-toggle="dropdown" href="" class="tile-menu"></a>
			<ul class="dropdown-menu animated pull-right text-right">
				<li><a href="">Refresh</a></li>
				<li><a href="">Settings</a></li>
			</ul>
		</div>

		<div class="listview narrow">
			<div class="media p-l-5">
				<div class="pull-left">
					<img width="40" src="${pageContext.request.contextPath }/back/img/profile-pics/1.jpg" alt="">
				</div>
				<div class="media-body">
					<small class="text-muted">2 Hours ago by Adrien San</small><br /> <a
						class="t-overflow" href="">Cras molestie fermentum nibh, ac
						semper</a>

				</div>
			</div>
			<div class="media p-l-5">
				<div class="pull-left">
					<img width="40" src="${pageContext.request.contextPath }/back/img/profile-pics/2.jpg" alt="">
				</div>
				<div class="media-body">
					<small class="text-muted">5 Hours ago by David Villa</small><br />
					<a class="t-overflow" href="">Suspendisse in purus ut nibh
						placerat</a>

				</div>
			</div>
			<div class="media p-l-5">
				<div class="pull-left">
					<img width="40" src="${pageContext.request.contextPath }/back/img/profile-pics/3.jpg" alt="">
				</div>
				<div class="media-body">
					<small class="text-muted">On 15/12/2013 by Mitch bradberry</small><br />
					<a class="t-overflow" href="">Cras pulvinar euismod nunc quis
						gravida. Suspendisse pharetra</a>

				</div>
			</div>
			<div class="media p-l-5">
				<div class="pull-left">
					<img width="40" src="${pageContext.request.contextPath }/back/img/profile-pics/4.jpg" alt="">
				</div>
				<div class="media-body">
					<small class="text-muted">On 14/12/2013 by Mitch bradberry</small><br />
					<a class="t-overflow" href="">Cras pulvinar euismod nunc quis
						gravida. </a>

				</div>
			</div>
			<div class="media p-l-5">
				<div class="pull-left">
					<img width="40" src="${pageContext.request.contextPath }/back/img/profile-pics/5.jpg" alt="">
				</div>
				<div class="media-body">
					<small class="text-muted">On 13/12/2013 by Mitch bradberry</small><br />
					<a class="t-overflow" href="">Integer a eros dapibus, vehicula
						quam accumsan, tincidunt purus</a>

				</div>
			</div>
			<div class="media p-5 text-center l-100">
				<a href=""><small>VIEW ALL</small></a>
			</div>
		</div>
	</div>
