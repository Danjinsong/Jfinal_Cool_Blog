<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Date Time Picker -->
<div class="block-area" id="date-time">
    <h3 class="block-title">Date Time Picker</h3>
    
    <br/>
    <p>Use Grid Classes to resize the column sizes.</p>
    
    <div class="row">
        <div class="col-md-4 m-b-15">
            <p>Date Picker</p>
            <div class="input-icon datetime-pick date-only">
                <input data-format="dd/MM/yyyy" type="text" class="form-control input-sm" />
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>24hr Time Picker</p>
            <div class="input-icon datetime-pick time-only">
                <input data-format="hh:mm:ss" type="text" class="form-control input-sm" />
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
        
        <div class="col-md-4 m-b-15">
            <p>12hr Time Picker</p>
            <div class="input-icon datetime-pick time-only-12">
                <input data-format="hh:mm:ss" type="text" class="form-control input-sm" />
                <span class="add-on">
                    <i class="sa-plus"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<hr class="whiter m-t-20" />

