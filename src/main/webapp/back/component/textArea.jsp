<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
 <!-- Textarea -->
<div class="block-area" id="textarea">
    <h3 class="block-title">Textarea</h3>
    
    <p>Form control which supports multiple lines of text. Change 'rows' attribute as necessary.</p>
    <textarea class="form-control overflow" rows="3" placeholder="This is a default Textarea..."></textarea>
    
    <p></p>
    
    <p>Textarea with auto-grow height</p>
    <textarea class="form-control auto-size m-b-10" placeholder="This is an auto sizing Textarea..."></textarea>
    
    <p>Textarea Disabled</p>
    <textarea class="form-control" placeholder="This is disabled" disabled></textarea>
</div>

<hr class="whiter m-t-20" />