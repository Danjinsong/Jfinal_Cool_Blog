<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Checkbox + Radio-->
<div class="block-area" id="check">
    <h3 class="block-title">Checkbox + Radio</h3>
    <p>Default Checkbox</p>
    
    <div class="checkbox m-b-5">
        <label>
            <input type="checkbox" checked>
            This is an awesome sample Checkbox
        </label>
    </div>
    
    <div class="clearfix"></div>
    
    <div class="checkbox m-b-5">
        <label>
            <input type="checkbox">
            This is another awesome sample Checkbox
        </label>
    </div>
    
    <div class="clearfix"></div>
    
    <div class="checkbox m-b-5">
        <label>
            <input type="checkbox">
            One more awesome sample Checkbox
        </label>
    </div>
    
    <br/>
    
    <p>Inline Checkboxes</p>
    <label class="checkbox-inline">
        <input type="checkbox">
        1
    </label>
    
    <label class="checkbox-inline">
        <input type="checkbox">
        2
    </label>
    
    <label class="checkbox-inline">
        <input type="checkbox">
        3
    </label>

    <p class="m-t-20">Radio</p>
    <div class="radio">
        <label>
            <input type="radio" name="radio">
            Option one is this and that&mdash;be sure to include why it's great
        </label>
    </div>
                        
    <div class="radio">
        <label>
            <input type="radio" name="radio">
            Option one is this and that&mdash;be sure to include why it's great
        </label>
    </div>
    
    <div class="clearfix"></div>
    
    <p class="m-t-20">Disabled</p>
    <div class="radio">
        <label>
            <input type="radio" checked disabled>
            This Radio is checked and disabled
        </label>
    </div>
    <div class="radio">
        <label>
            <input type="radio" disabled>
            This Radio is not checked and disabled
        </label>
    </div>
    
    <div class="checkbox m-t-10 m-b-5">
        <label>
            <input type="checkbox" checked disabled>
            This is an awesome sample Checkbox checked and disabled
        </label>
    </div>
    
    <div class="checkbox">
        <label class="m-t-10">
            <input type="checkbox" disabled>
            This is too disabled
        </label>
    </div>
    
    <br/><br/><br/>
</div>  