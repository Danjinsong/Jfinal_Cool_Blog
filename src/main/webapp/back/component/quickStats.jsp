<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Quick Stats -->
<div class="block-area">
	<div class="row">
		<div class="col-md-3 col-xs-6">
			<div class="tile quick-stats">
				<div id="stats-line-2" class="pull-left"></div>
				<div class="data">
					<h2 data-value="122"></h2>
					<small>所有博客</small>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line-3" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="12"></h2>
					<small>近一周博客</small>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-xs-6">
			<div class="tile quick-stats media">

				<div id="stats-line-4" class="pull-left"></div>

				<div class="media-body">
					<h2 data-value="7">0</h2>
					<small>所有页面</small>
				</div>
			</div>
		</div>

		<div class="col-md-3 col-xs-6">
			<div class="tile quick-stats media">
				<div id="stats-line" class="pull-left"></div>
				<div class="media-body">
					<h2 data-value="2">0</h2>
					<small>近一周页面</small>
				</div>
			</div>
		</div>
	</div>

</div>

<hr class="whiter"/>