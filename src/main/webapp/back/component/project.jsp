<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
  <!-- Projects -->
        <div class="s-widget m-b-25">
            <h2 class="tile-title">
                	项目任务
            </h2>
            
            <div class="s-widget-body">
                <div class="side-border">
                    <small>创建网站</small>
                    <div class="progress progress-small">
                         <a href="#" data-toggle="tooltip" title="" class="progress-bar tooltips progress-bar-danger" style="width: 60%;" data-original-title="60%">
                              <span class="sr-only">60% 已完成</span>
                         </a>
                    </div>
                </div>
                <div class="side-border">
                    <small>发布博客</small>
                    <div class="progress progress-small">
                         <a href="#" data-toggle="tooltip" title="" class="tooltips progress-bar progress-bar-info" style="width: 43%;" data-original-title="43%">
                              <span class="sr-only">43% 已完成</span>
                         </a>
                    </div>
                </div>
                <div class="side-border">
                    <small>编写API文档</small>
                    <div class="progress progress-small">
                         <a href="#" data-toggle="tooltip" title="" class="tooltips progress-bar progress-bar-warning" style="width: 81%;" data-original-title="81%">
                              <span class="sr-only">81% 已完成</span>
                         </a>
                    </div>
                </div>
                <div class="side-border">
                    <small>新技术学习</small>
                    <div class="progress progress-small">
                         <a href="#" data-toggle="tooltip" title="" class="tooltips progress-bar progress-bar-success" style="width: 10%;" data-original-title="10%">
                              <span class="sr-only">10% 已完成</span>
                         </a>
                    </div>
                </div>
                <div class="side-border">
                    <small>优化浏览器</small>
                    <div class="progress progress-small">
                         <a href="#" data-toggle="tooltip" title="" class="tooltips progress-bar progress-bar-success" style="width: 95%;" data-original-title="95%">
                              <span class="sr-only">95% 已完成</span>
                         </a>
                    </div>
                </div>
            </div>
        </div>