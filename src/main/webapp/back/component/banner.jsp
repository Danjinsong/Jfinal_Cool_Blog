<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Image Banner -->
 <div class="tile">
     <h2 class="tile-title">轮播图组件</h2>
     <div class="tile-config dropdown">
         <a data-toggle="dropdown" href="" class="tooltips tile-menu" title="" data-original-title="Options"></a>
         <ul class="dropdown-menu pull-right text-right">
             <li><a href="">Edit</a></li>
             <li><a href="">Delete</a></li>
         </ul>
     </div>
     <div id="carousel-example-generic" class="carousel slide">
         <!-- Indicators -->
         <ol class="carousel-indicators">
             <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
             <li data-target="#carousel-example-generic" data-slide-to="1"></li>
             <li data-target="#carousel-example-generic" data-slide-to="2"></li>
         </ol>
       
         <!-- Wrapper for slides -->
         <div class="carousel-inner">
             <div class="item active">
                 <img src="img/carousel/c-1.jpg" alt="Slide-1">
             </div>
             <div class="item">
                 <img src="img/carousel/c-2.jpg" alt="Slide-2">
                 <div class="carousel-caption hidden-xs">
                     <h3>This is a Caption</h3>
                     <p>Sample detail text here</p>
                 </div>
             </div>
             <div class="item">
                 <img src="img/carousel/c-3.jpg" alt="Slide-3">
                 <div class="carousel-caption hidden-xs">
                     <h3>This is a Caption</h3>
                     <p>Sample detail text here</p>
                 </div>
             </div>
         </div>
         
         <!-- Controls -->
         <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
             <i class="icon">&#61903;</i>
         </a>
         <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
             <i class="icon">&#61815;</i>
         </a>
     </div>
 </div>