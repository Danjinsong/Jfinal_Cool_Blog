<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- USA Map -->
<div class="tile">
	<h2 class="tile-title">Live Visits</h2>
	<div class="tile-config dropdown">
		<a data-toggle="dropdown" href="" class="tile-menu"></a>
		<ul class="dropdown-menu pull-right text-right">
			<li><a href="">Refresh</a></li>
			<li><a href="">Settings</a></li>
		</ul>
	</div>

	<div id="usa-map"></div>
</div>