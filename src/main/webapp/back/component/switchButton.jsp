<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- Toggler -->
<div class="block-area" id="toggle-switch">
    
    <h3 class="block-title">Toggle Switch</h3>
    
    <br/>
    
    <p>Default Tag selector</p>
    
    <div class="make-switch switch-large" data-on="info" data-off="success">
        <input type="checkbox">
    </div>
    
    <div class="make-switch">
        <input type="checkbox">
    </div>
    
    <div class="make-switch switch-small">
        <input type="checkbox">
    </div>
    
    <div class="make-switch switch-mini">
        <input type="checkbox">
    </div>
    
    <p></p>
    <p>Custom Text Labels</p>
    <div class="make-switch" data-text-label="TV">
        <input type="checkbox">
    </div>
    
    <div class="make-switch" data-text-label="MT">
        <input type="checkbox">
    </div>
    
    <div class="make-switch" data-text-label="ab">
        <input type="checkbox">
    </div>
    
    <div class="make-switch" data-text-label="Zr">
        <input type="checkbox">
    </div>
    
    <p></p>
    <p>With Icons</p>
    <div class="make-switch" data-label-icon="fa fa-phone" data-on-label="<i class='fa fa-check'></i>" data-off-label="<i class='fa fa-times'></i>">
    <input type="checkbox">
</div>
 
<div class="make-switch" data-label-icon="fa fa-facebook" data-on-label="<i class='fa fa-thumbs-up'></i>" data-off-label="<i class='fa fa-thumbs-down'></i>">
    <input type="checkbox">
</div>
 
<div class="make-switch" data-label-icon="fa fa-globe" data-on-label="<i class='fa fa-angle-left'></i>" data-off-label="<i class='fa fa-angle-right'></i>">
    <input type="checkbox">
</div>

<div class="make-switch" data-label-icon="fa fa-comment" data-on-label="<i class='fa fa-undo'></i>" data-off-label="<i class='fa fa-rotate-right'></i>">
        <input type="checkbox">
    </div>
    
    <p></p>
    <p>Disabled</p>
    <div class="make-switch switch-large" data-on="info" data-off="success">
        <input type="checkbox" checked disabled>
    </div>
    
    <div class="make-switch">
        <input type="checkbox" checked disabled>
    </div>
    
    <div class="make-switch switch-small">
        <input type="checkbox" checked disabled>
    </div>
    
    <div class="make-switch switch-mini">
        <input type="checkbox" checked disabled>
    </div>
</div>

<hr class="whiter m-t-20" />
