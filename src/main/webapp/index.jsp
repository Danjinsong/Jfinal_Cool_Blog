<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
   <head>
       <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
       <meta name="format-detection" content="telephone=no">
       <meta name="description" content="Violate Responsive Admin Template">
       <meta name="keywords" content="Super Admin, Admin,Bootstrap,Blog">
       <title>WELCOME TO DJSBLOG</title>
	   <jsp:include page="/static/linkDemo/jsAndCss.jsp"></jsp:include>
   </head>
  <body id="<c:if test="${!empty skin}">${skin}</c:if><c:if test="${empty skin}">skin-blur-lights</c:if>" >
  		<!-- header -->
  		<jsp:include page="/fore/common/header.jsp"></jsp:include>
  		<!-- main -->
  		<section id="main" class="p-relative" role="main">
  			<!-- siderMune -->
  			<jsp:include page="/fore/common/siderMune.jsp"></jsp:include>
  			<!-- banner -->
  			<c:if test="${banner}">
  				<jsp:include page="/fore/common/banner.jsp"></jsp:include>
  			</c:if>
  			<c:if test="${!banner}">
  				<jsp:include page="/fore/common/bannerForList.jsp"></jsp:include>
			</c:if>
			<div  class="container" >
			
			<c:if test="${contentBox}">
  				<jsp:include page="/fore/blog/contentBox.jsp"></jsp:include>
			</c:if>
				
               <div class="block-area" class="container">
               
               
                  	<!-- blog -->
                   	<jsp:include page="${mainPage}"></jsp:include>   
                   	
                   	
	                  <div class="col-md-4">
	                   	 <jsp:include page="/fore/common/tianqi.jsp"></jsp:include>
	                   	 <!-- blogType列表 --> 
	                   	 <jsp:include page="/fore/blog/blogTypeList.jsp"></jsp:include>
	                   	 <!-- Monthly列表 -->   	
	                   	 <jsp:include page="/fore/blog/monthlyList.jsp"></jsp:include>
	                   	 <!-- topic -->
	                   	 <jsp:include page="/fore/common/zhuanti.jsp"></jsp:include>
	                   	 <!-- links -->
	                   	 <jsp:include page="/fore/common/links.jsp"></jsp:include>
	                   	 <!-- photoWall -->
	                   	 <jsp:include page="/fore/common/picWall.jsp"></jsp:include>
               	  	</div>
            	  </div>
            	  
           	  <c:if test="${!contentBox}">
  				<jsp:include page="/fore/blog/contentBox.jsp"></jsp:include>
			 </c:if>
             </div>
             
        </section>
        <hr class="whiter">
       	<div class="block-area">
			<div class="container">
				<div class="tile">
						<!-- 意见 -->
	                   	<jsp:include page="/fore/common/sugess.jsp"></jsp:include>   
						<!-- 日历 -->
	                   	<jsp:include page="/fore/common/calendar.jsp"></jsp:include>   
						<!-- 本站信息 -->
	                   	<jsp:include page="/fore/common/elseInfo.jsp"></jsp:include>   
				</div>
			</div>
        	<div class="clearfix"></div>
		</div>
		<!-- footer -->
		<footer class="tm-footer" style="container ">                
		</footer> 
  </body>
</html>
<input type="hidden" id='currentUser' value="${currentUser.photo }"/>
<input type="hidden" id='currentUserId' value="${currentUser.id }"/>
<div class="modal fade" id="mes" tabindex="-1" role="dialog" aria-hidden="true">
     <div class="modal-dialog">
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" id="title"></h4>
             </div>
             <div class="modal-body">
                 <p id="body"></p>
             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-sm" data-dismiss="modal">关闭</button>
             </div>
         </div>
     </div>
 </div>
 <!-- Compose -->
<div class="modal fade" id="toAuthorModal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
           	<form class="form-validation-3">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">私信</h4>
            </div>
	            <div class="modal-header p-0">
	                <input type="text" id="author_nickName" class="form-control input-sm input-transparent" placeholder="收信人昵称..." disabled="disabled">
	            </div>
            <div class="modal-header p-0">
                <input type="text" id="author_title" class="form-control input-sm validate[required] input-transparent" placeholder="主题(标题)...">
            </div>
            <div class="p-relative" id="author_div">
                <div class="message-options">
                    <img src="/DJSBlog/static/img/icon/tile-actions.png" alt="">
                </div>
                <textarea class="message-editor" placeholder="说点什么..."></textarea>
            </div>
            <div class="modal-footer m-0">
                <button class="btn" onclick="toSendAuthor();return false;">发送</button>
                <button class="btn"  data-dismiss="modal">取消</button>
            </div>
           	</form>
        </div>
    </div>
</div>
<script>
$(function(){
	$('.message-editor').summernote({
        toolbar: [
          //['style', ['style']], // no style button
          ['style', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          //['height', ['height']],
          ['insert', ['picture', 'link']] // no insert buttons
          //['table', ['table']], // no table button
          //['help', ['help']] //no help button
        ],
        height: 200,
        resizable: false
    });
    $('.message-options').click(function(){
        $(this).closest('.modal').find('.note-toolbar').toggle(); 
    });  
});
function foucus(userId,nickName){
	if($('#currentUser').val() == ''){
		mes("系统提示","您请先登录!");
	}else{
		$.post('/DJSBlog/foucus',{
			currentUserId:$('#currentUserId').val(),
			id:userId,
		},function(res){
			if(res.success)
				mes("系统提示","关注:\""+nickName+"\"成功");
			else
				mes("系统提示","关注失败,您是不是已经关注过'他/她'了!");
		},'json');
	}
}
function mes(title,body){
	$('#title').html(title);
	$('#body').html(body);
	$('#mes').modal('show');
}
if($('#currentUser').val()!=null&&$('#currentUser').val()!=''){
	$.post('/DJSBlog/back/files/ajaxGetById',{id:$('#currentUser').val()},function(res){
		$('#userPhoto').attr({src:'/DJSBlog'+res.files.src});
		$('#userPhototiny').attr({src:'/DJSBlog'+res.files.src});
		$('#userThm').html("<img src='/DJSBlog"+res.files.src+"'/>");
	},'json');
}
function toAuthor(userId,nickName){
	$("#author_div .note-editable").eq(0).html('');
	$('#author_title').val('');
	if($('#currentUser').val() == ''){
		mes("系统提示","您请先登录!");
	}else{
		$('#author_nickName').val("给\""+nickName+"\"的私信");
		$('#toAuthorModal').modal('show');
	}
}
function toSendAuthor(){
	 if($('#author_title').val()&&$('<p>'+$("#author_div .note-editable").eq(0).html()+'</p>').text()!=''){
		 $.post('/DJSBlog/back/message/ajaxSave',{
			 'nickName':$('#author_nickName').val().split("\"")[1],
			 'message.title':$('#author_title').val(),
			 'message.text':$("#author_div .note-editable").eq(0).html(),
			 'message.sendId':$('#currentUserId').val()
		 },function(res){
			 if(res.success){
				 $('#toAuthorModal').modal('hide');
				 mes("系统提示","发送成功");
			 }else{
				 mes("系统提示","发送失败");
			 }
		 },'json');
	 }else{
		 mes("系统提示","输入不能为空");
	 }
}
</script>
