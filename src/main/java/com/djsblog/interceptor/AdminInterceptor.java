package com.djsblog.interceptor;

import com.djsblog.entity.User;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.StrKit;
/**
 * 拦截用户进入后台
 * @author 但劲松
 *
 */
public class AdminInterceptor implements Interceptor{
	/**
	 * 拦截除了登录，注册，找回密码，ajax请求之外的访问后台
	 */
	public void intercept(Invocation inv) {
		User currentUser = inv.getController().getSessionAttr("currentUser") ;
		if(currentUser!=null && ("superAdmin".equals(currentUser.getRole())||"admin".equals(currentUser.getRole())))
			inv.invoke();
		else {
			String methodName = inv.getMethodName();
			if(StrKit.notBlank(methodName)){
				if(methodName.startsWith("ajax")||methodName.startsWith("login")||methodName.startsWith("signUp")||methodName.startsWith("findPassWord")||methodName.startsWith("logout")){
					inv.invoke();
				}
				else inv.getController().redirect("/login.jsp");
			}
			else inv.getController().redirect("/login.jsp");
		}
	}

}
