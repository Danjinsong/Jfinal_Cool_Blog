package com.djsblog.interceptor;

import java.util.LinkedList;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.Type;
import com.djsblog.entity.User;
import com.djsblog.enums.Options;
import com.djsblog.service.DataDicService;
import com.djsblog.service.UserService;
import com.djsblog.service.impl.DataDicServiceImpl;
import com.djsblog.service.impl.UserServiceImpl;
import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;


/**
 * 全局拦截器
 * @author 但劲松
 *
 */
public class GlobalInterceptor implements Interceptor{
	/**
	 * 拦截未登录的用户访问后台
	 */
	public void intercept(Invocation inv) {
		if(inv.getController().getSessionAttr("currentUser") == null){
			User user = new User();
			user.setUserName(inv.getController().getCookie("user.userName"));	 
			user.setPassword(inv.getController().getCookie("user.password"));
			if(StrKit.notBlank(new String[]{user.getUserName(),user.getPassword()})){
				UserService userService = new UserServiceImpl();
				User u = userService.login(user);
				if(u!=null){
					if(u.getPassword().equals(HashKit.md5(user.getPassword()))){
						inv.getController().setSessionAttr("currentUser", u);
					}
					
				}
			}
			
		}
		//读取皮肤设置
		User user = inv.getController().getSessionAttr("currentUser");
		if(user!=null&&inv.getController().getSessionAttr("skin")==null){
			DataDicService dataDicService = new DataDicServiceImpl();
			String skin = dataDicService.getByKey(user.getId()+"_SKIN");
			inv.getController().setSessionAttr("skin", skin);
		}
		//读取分类设置
		if(inv.getController().getSessionAttr("typeTop")==null){
			List<Type> typeTop = Type.dao.find("select * from type where type_type='"+Options.G_FATHER+"' and del='"+false+"'");
			inv.getController().setSessionAttr("typeTop", typeTop);
		}
		//加载首页动图
		if(inv.getController().getSessionAttr("bannerImg")==null){
			List<Files> bannerImg  = Files.dao.find("select * from files where id in (select photo from blog where del='false') order by created desc limit 0,3");
			inv.getController().setSessionAttr("bannerImg", bannerImg);
		}
		//加载4条最新博客
		if(inv.getController().getSessionAttr("fourNewBlog")==null){
			//Db Record 模式查询
			List<Record> fourNewBlog  = Db.find("SELECT u.nickName,f.src,b.* FROM blog b,files f,user u WHERE b.del='false' AND b.`photo`=f.`id` and b.userId=u.id ORDER BY b.created DESC LIMIT 0,4");
			inv.getController().setSessionAttr("fourNewBlog", fourNewBlog);
		}
		//加载友情链接
		if(inv.getController().getSessionAttr("linkList")==null){
			//Db Record 模式查询
			List<Record> linkList  = Db.find("select * from datadic where `key` like '%http%' and del='false'");
			inv.getController().setSessionAttr("linkList", linkList);
		}
		//加载分类列表
		if(inv.getController().getSessionAttr("typeList")==null){
			List<Type> typeTop = inv.getController().getSessionAttr("typeTop");
			List<List<Type>> typeList = new LinkedList<List<Type>>();
			for(Type t:typeTop){
				List<Type> types = Type.dao.find("select * from type where type_type="+t.getId()+" and del='false'");
				typeList.add(types);
			}
			inv.getController().setSessionAttr("typeList", typeList);
		}
		//加载photoWall
		if(inv.getController().getSessionAttr("photoWall")==null){
			List<Files> photoWall = Files.dao.find("select * from files where type like '%image%' and del=false order by created desc limit 0,12");
			inv.getController().setSessionAttr("photoWall", photoWall);
		}
		//加载月分类
		if(inv.getController().getSessionAttr("mothlyBlogs")==null){
			//Db Record 模式查询
			List<Record> mothlyBlogs = Db.find("SELECT DATE_FORMAT(created,'%Y年%m月') AS mothly,COUNT(*) AS blogCount  FROM blog GROUP BY DATE_FORMAT(created,'%Y年%m月') ORDER BY  DATE_FORMAT(created,'%Y年%m月') DESC LIMIT 0,10");
			inv.getController().setSessionAttr("mothlyBlogs", mothlyBlogs);
		}
		//加载本站信息
		if(inv.getController().getSessionAttr("webInfo")==null){
			String webInfo = new String();
			inv.getController().setSessionAttr("webInfo", webInfo);
		}
		//加载我的信息数量
		if(inv.getController().getSessionAttr("mesCount")==null){
			User currentUser = inv.getController().getSessionAttr("currentUser");
			if(currentUser!=null){
				Long mesCount = Db.queryLong("SELECT COUNT(*) FROM message WHERE getId="+currentUser.getId()+" AND get_del=FALSE AND state='unRead'");
				inv.getController().setSessionAttr("mesCount", mesCount);
			}
		}
		//加载专题博客
		if(inv.getController().getSessionAttr("topicBlogs")==null){
			//Db Record 模式查询
			List<Record> topicBlogs = Db.find("SELECT t.*,COUNT(b.id) AS blogCount FROM TYPE t,blog b,mapping m WHERE t.type_type='g_zhuanti' AND t.del=FALSE AND b.id = m.`fatherId` AND b.`del`='false' AND m.`sonId`=t.`id` AND m.`mapping_type`='b2z' GROUP BY t.id DESC LIMIT 0,10");
			inv.getController().setSessionAttr("topicBlogs", topicBlogs);
		}
		inv.invoke();
		
	}

}
