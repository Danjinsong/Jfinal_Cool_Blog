package com.djsblog.enums;
/**
 * 配置参数
 * @author 但劲松
 *
 */
public class Options {
	//删除标识符
	public static final String DEL_TRUE="true";
	public static final String DEL_FALSE="false";
	//APP常量
	public static final int PAGESIZE=10;
	
	//祖父节点表示---导航
	public static final String G_FATHER="g_father";
	//专题分类
	public static final String G_ZHUANTI="g_zhuanti";
	//标签分类
	public static final String G_BIAOQIAN="g_biaoqian";
	//映射类型		博客对应分类
	public static final String BLOG_TYPE="b2t";
	//映射类型		评论对应回复
	public static final String COMMENT_RELAY="c2r";
	//映射类型		博客对应评论
	public static final String BLOG_COMMENT="b2c";
	//映射类型		博客对应标签
	public static final String BLOG_BIAOQIAN="b2b";
	//映射类型		博客对应专题
	public static final String BLOG_ZHUANTI="b2z";
	//映射类型		用户对应关注
	public static final String USER_FOUCUS="u2u";
	
	//信息阅读状态 已阅读
	public static final String MESSAGE_READ="read";
	//信息阅读状态 未阅读
	public static final String MESSAGE_UNREAD="unRead";
	
	
	
	
	
}
