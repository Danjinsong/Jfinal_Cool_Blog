package com.djsblog.entity;

import java.util.Map;

/**
 * ��ҳModel��
 *  @author ������
 *
 */
public class PageBean {

	private int page; // �ڼ�ҳ
	private int pageSize; // ÿҳ��¼��
	@SuppressWarnings("unused")
	private int start;  // ��ʼҳ
	
	
	public PageBean(int page, int pageSize) {
		super();
		this.page = page;
		this.pageSize = pageSize;
	}
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStart() {
		return (page-1)*pageSize;
	}
	public static PageBean creat(String page,String rows){
		PageBean pageBean = new PageBean(Integer.parseInt(page), Integer.parseInt(rows));
		return pageBean;
	}
	public static void setStartAndSize(PageBean pageBean,Map<String,Object> parm){
		parm.put("start", pageBean.getStart());
		parm.put("size", pageBean.getPageSize());
	}
	
}
