package com.djsblog.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.Thumbnails.Builder;
import net.coobird.thumbnailator.geometry.Positions;

public class PicUtil {

	/**
	 * 指定大小进行缩放
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param width 宽
	 * @param height 高
	 * @throws IOException
	 */
	public static void resize(String srcUrl,String targetUrl,int width,int height) throws IOException {
		/*
		 * size(width,height) 若图片横比200小，高比300小，不变
		 * 若图片横比200小，高比300大，高缩小到300，图片比例不变 若图片横比200大，高比300小，横缩小到200，图片比例不变
		 * 若图片横比200大，高比300大，图片按比例缩小，横为200或高为300
		 */
		if(width > 100){
			BufferedImage image = ImageIO.read(new File(srcUrl));  
			int imageWidth = image.getWidth();  
			int imageHeitht = image.getHeight();  
			if(imageWidth < 1920 || imageHeitht < 1080){
				Thumbnails.of(srcUrl).size(1920,1080).keepAspectRatio(false).toFile(targetUrl);
			}
			Thumbnails.of(srcUrl)
				.sourceRegion(Positions.TOP_CENTER,width,height)
				.size(width, height)
				.keepAspectRatio(false)
				.outputQuality(0.25f)
				.toFile(targetUrl);
			
		}else{
			Thumbnails.of(srcUrl).size(width,height).keepAspectRatio(false).outputQuality(0.25f).toFile(targetUrl);
		}
	}
	
	/**
	 * 按照比例进行缩放
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param num 质量比例如 0.8
	 * @throws IOException
	 */
	public static void scale(String srcUrl,String targetUrl,double num) throws IOException {
		Thumbnails.of(srcUrl).scale(num).outputQuality(0.25f).toFile(targetUrl );
	}
	
	/**
	 * 水印
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param width 宽
	 * @param height 高
	 * @param num 质量比例如 0.8
	 * @param pos 显示位置:  Positions.BOTTOM_RIGHT  
	 * @throws IOException
	 */
	public static void watermark(String srcUrl,String targetUrl,int width,int height,float num,Positions pos) throws IOException {
		Thumbnails.of(srcUrl).size(width,height).watermark(pos,ImageIO.read(new File(targetUrl)), num).outputQuality(num).toFile(targetUrl);
	}
	
	/**
	 * 裁剪
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param width 宽
	 * @param height 高
	 * @param pos 显示位置:  Positions.BOTTOM_RIGHT  
	 * @param x 区域宽度 
	 * @param y 区域高度
	 * @throws IOException
	 */
	public static void cut(String srcUrl,String targetUrl,int width,int height,Positions pos,int x,int y)throws IOException {
		Thumbnails.of(srcUrl).sourceRegion(pos,x,y).size(width, height).keepAspectRatio(false).outputQuality(0.25f).toFile(targetUrl);
	}
	
	
	/**
	 * 裁剪--指定坐标/大小
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param width 宽
	 * @param height 高
	 * @param pointA_1 坐标A1 
	 * @param pointA_2坐标A2 
	 * @param pointB_1坐标B1
	 * @param pointB_2坐标B2
	 * @throws IOException
	 */
	public static void cut(String srcUrl,String targetUrl,int width,int height,int pointA_1,int pointA_2,int pointB_1,int pointB_2) throws IOException {
		Thumbnails.of(srcUrl).sourceRegion(pointA_1, pointA_2, pointB_1, pointB_2).size(width, height).keepAspectRatio(false).toFile(targetUrl);
	}
	
	/**
	 * 转化图像格式
	 * @param srcUrl 源图片地址
	 * @param targetUrl 目标图片地址
	 * @param width 宽
	 * @param height 高
	 * @param format 格式 如png/gif/jpg
	 * @throws IOException
	 */
	public static void format(String srcUrl,String targetUrl,int width,int height,String format) throws IOException {
		Thumbnails.of(srcUrl).size(width, height).outputFormat(format).toFile(targetUrl);
	}
	/**
	 *  强制压缩
	 */
	public static void foroce(String srcUrl,String targetUrl,int width,int height) throws IOException{
		String imagePath = srcUrl;  
		BufferedImage image = ImageIO.read(new File(imagePath));  
		Builder<BufferedImage> builder = null;  
		  
		int imageWidth = image.getWidth();  
		int imageHeitht = image.getHeight();  
		if ((float)height / width != (float)imageWidth / imageHeitht) {  
		    if (imageWidth > imageHeitht) {  
		       image = Thumbnails.of(imagePath).height(height).asBufferedImage();  
		    } else {  
		        image = Thumbnails.of(imagePath).width(width).asBufferedImage();  
		    }  
		    builder = Thumbnails.of(image).sourceRegion(Positions.CENTER, width, height).size(width, height);  
		} else {  
		    builder = Thumbnails.of(image).size(width, height);  
		}  
		builder.outputFormat("jpg").toFile(targetUrl);  

	}
}
