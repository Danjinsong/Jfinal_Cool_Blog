package com.djsblog.util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import com.jfinal.plugin.activerecord.Db;

/**
 * 数据统计类
 * @author 但劲松
 *
 */
public class DataMapUtil {
	/**
	 * 封装构建后台数据的DataMap
	 * @return
	 */
	public static Map<String,String>  updateDataMap(){
		Map<String,String>   data_map = new HashMap<String, String>();
		//查询所有博客数量
		data_map.put("blogTotal", Db.queryLong("select count(*) as total from blog").toString());
		//查询近一周博客数量
		data_map.put("blog_week_Total", Db.queryLong("select count(*) from blog where date_sub(curdate(),interval 7 day)<=date(created)").toString());
		//查询所有用户数量
		data_map.put("userTotal", Db.queryLong("select count(*) as total from user").toString());
		//查询近一周博客数量
		data_map.put("user_week_Total", Db.queryLong("select count(*) from user where date_sub(curdate(),interval 7 day)<=date(created)").toString());
		//查询所有附件数量
		data_map.put("filesTotal", Db.queryLong("select count(*) as total from files").toString());
		//查询近一周附件数量
		data_map.put("files_week_Total", Db.queryLong("select count(*) from files where date_sub(curdate(),interval 7 day)<=date(created)").toString());
		//构建折线图数据
		StringBuffer lineChar = new StringBuffer();
		for(int i=7;i>=0;i--){
			//获取当天的撰写的博客数量
			long num = Db.queryLong("SELECT COUNT(*) FROM blog WHERE DATE_SUB(CURDATE(),INTERVAL "+i+" DAY)=DATE(created)");
			lineChar.append(num+",");
			//获取当天的星期数
			String week = DateUtil.getWeek(DateUtil.addDay(new Date(),-i)).getChineseName();
			lineChar.append(week+(i==0?"":","));
		}
		data_map.put("lineChar", lineChar.toString());
		//构建条形图数据
		StringBuffer barChar = new StringBuffer();
			long num_game = Db.queryLong("select count(*) from mapping where sonId=1");
			barChar.append(num_game+",");
			long num_movie = Db.queryLong("select count(*) from mapping where sonId=2");
			barChar.append(num_movie+",");
			long num_life = Db.queryLong("select count(*) from mapping where sonId=3");
			barChar.append(num_life+",");
			long num_study = Db.queryLong("select count(*) from mapping where sonId=4");
			barChar.append(num_study);
			System.out.println(barChar);
		data_map.put("barChar", barChar.toString());
		
		return data_map;
	}
	
}
