package com.djsblog.util;

import java.util.List;

import com.djsblog.entity.Blog;
import com.djsblog.entity.Datadic;
import com.djsblog.entity.Files;
import com.djsblog.entity.Message;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;
import com.djsblog.entity.User;
import com.djsblog.enums.DateStyle;
import com.djsblog.enums.Options;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * ajax代码编辑类
 * @author 但劲松
 *
 */
public class CodeUtil {
	/**
	 * 构造叶尾分页
	 * @param tFoot
	 * @param totalNum
	 * @param currentPage
	 * @param pageSize
	 * @return
	 */
	public static StringBuffer page(StringBuffer tFoot,long totalNum,int currentPage,int pageSize,int col){
		long totalPage=totalNum%pageSize==0?totalNum/pageSize:totalNum/pageSize+1;
		if(totalPage==0)
			tFoot.append("未查询到数据");
		else{
			tFoot.append("<tr><td colspan="+col+"><div class='btn-group'>");
			tFoot.append("<button type='button' class='btn btn-sm ' onclick='toPage(1);'><<</button>");
			if(currentPage >1)
				tFoot.append("<button type='button' class='btn btn-sm ' onclick='toPage("+(currentPage-1)+")'><</button>");
			else
				tFoot.append("<button type='button' class='btn btn-sm ' disabled='disabled')'><</button>");
			for(int i=currentPage-2;i<=currentPage+2;i++){
				if(i<1||i>totalPage){
					continue;
				}
				if(i==currentPage){
					tFoot.append("<button type='button' class='btn btn-sm ' disabled='disabled'>"+i+"</button>");	
				}else{
					tFoot.append("<button type='button' class='btn btn-sm '  onclick='toPage("+i+")'>"+i+"</button>");	
				}
			}
			if(currentPage<totalPage){
				tFoot.append("<button type='button' class='btn btn-sm '  onclick='toPage("+(currentPage+1)+")'>></button>");	
			}else{
				tFoot.append("<button type='button' class='btn btn-sm '  disabled='disabled'>></button>");	
			}
			tFoot.append("<button type='button' class='btn btn-sm ' onclick='toPage("+totalPage+");'>>></button>");
			tFoot.append(" </div></td></tr>");
		}
		return tFoot;
	}
	/**
	 * 编辑数据字典代码
	 */
	public static String[] datadicCode(List<Datadic> datadicList,PageBean pageBean,String[] parm){
		StringBuffer thead = new StringBuffer();//表头
		StringBuffer tbody = new StringBuffer();//身体
		StringBuffer tfoot = new StringBuffer();//分页
		//构建表头
		thead.append("<tr>");
		thead.append("<th><input type='checkbox' class='pull-left list-parent-check'/></th>");
		thead.append("<th>编号</th>");
		thead.append("<th>key</th>");
		thead.append("<th>value</th>");
		thead.append("</tr>");
		//构建表格
		for(Datadic datadic:datadicList){
			tbody.append("<tr>");
			tbody.append("<td><input type='checkbox' class='pull-left list-check' value='"+datadic.getId()+"' /></td>");
			tbody.append("<td>"+datadic.getId()+"</td>");
			if(datadic.getKey().startsWith("http"))
				tbody.append("<td><a href='"+datadic.getKey()+"' target='_blank'>"+datadic.getKey()+"</a></td>");
			else
				tbody.append("<td>"+datadic.getKey()+"</td>");
			tbody.append("<td>"+datadic.getVal()+"</td>");
			tbody.append("</tr>");
		}
		//构造分页
		tfoot = page(tfoot, Long.parseLong(parm[0]), pageBean.getPage(), pageBean.getPageSize(),4);
		return new String[]{thead.toString(),tbody.toString(),tfoot.toString()};
	}
  
	/**
	 * 编辑博客分类代码
	 * @return
	 */
	public static String[] typeBackCode(List<Type> typeList,PageBean pageBean,String[] parm){
		StringBuffer thead = new StringBuffer();//表头
		StringBuffer tbody = new StringBuffer();//身体
		StringBuffer tfoot = new StringBuffer();//分页
		//构建表头
		thead.append("<tr>");
		thead.append("<th><input type='checkbox' class='pull-left list-parent-check'/></th>");
		thead.append("<th>编号</th>");
		thead.append("<th>名称</th>");
		thead.append("<th>图片</th>");
		thead.append("<th>父节点</th>");
		thead.append("</tr>");
		//构建表格
		for(Type type:typeList){
			tbody.append("<tr>");
			tbody.append("<td><input type='checkbox' class='pull-left list-check' value='"+type.getId()+"' /></td>");
			tbody.append("<td>"+type.getId()+"</td>");
			tbody.append("<td>"+type.getName()+"</td>");
			if(type.getPhoto() == null)
				tbody.append("<td>暂时没有上传图片</td>");
			else
				tbody.append("<td>"+"<img src='/DJSBlog/"+Files.dao.findById(type.getPhoto()).getSrc()+"' class='img-thumbnail' style='width:96px;height:54px'/>"+"</td>");
			if(StrKit.isBlank(type.getTypeType())){
				tbody.append("<td>暂时没有父节点</td>");	
			}else if(Options.G_BIAOQIAN.equals(type.getTypeType())){
				tbody.append("<td>这是一条标签</td>");	
			}else if(Options.G_ZHUANTI.equals(type.getTypeType())){
				tbody.append("<td>这是一条专题</td>");	
			}else if(Options.G_FATHER.equals(type.getTypeType())){
				tbody.append("<td>这是一个祖父节点（已加入导航）</td>");	
			}else{
				tbody.append("<td>"+Db.queryStr("select name from type where id="+type.getTypeType())+"</td>");	
			}
			
			tbody.append("<tr>");
		}
		//构造分页
		tfoot = page(tfoot, Long.parseLong(parm[0]), pageBean.getPage(), pageBean.getPageSize(), 5);
		return new String[]{thead.toString(),tbody.toString(),tfoot.toString()};
	}
	/**
	 * 用户列表代码
	 */
	public static String[] userBackCode(List<User> userList, PageBean pageBean,
			String[] parm) {
		StringBuffer thead = new StringBuffer();//表头
		StringBuffer tbody = new StringBuffer();//身体
		StringBuffer tfoot = new StringBuffer();//分页
		//构建表头
		thead.append("<tr>");
		thead.append("<th><input type='checkbox' class='list-parent-check'/></th>");
		thead.append("<th>编号</th>");
		thead.append("<th>用户名</th>");
		thead.append("<th>真实名</th>");
		thead.append("<th>昵称名</th>");
		thead.append("<th>QQ</th>");
		thead.append("<th>邮箱</th>");
		thead.append("<th>电话</th>");
		thead.append("<th>角色</th>");
		thead.append("<th width='248'>个性签名</th>");
		thead.append("<th>头像</th>");
		thead.append("<th>创建日期</th>");
		thead.append("</tr>");
		//构建表格
		for(User user:userList){
			tbody.append("<tr>");
			tbody.append("<td><input type='checkbox' class='list-check' value='"+user.getId()+"'/></td>");
			tbody.append("<td>"+user.getId()+"</td>");
			tbody.append("<td>"+user.getUserName()+"</td>");
			tbody.append("<td>"+user.getRealName()+"</td>");
			tbody.append("<td>"+user.getNickName()+"</td>");
			tbody.append("<td>"+user.getQq()+"</td>");
			tbody.append("<td>"+user.getEmail()+"</td>");
			tbody.append("<td>"+user.getPhone()+"</td>");
			tbody.append("<td>"+("visitor".equals(user.getRole())?"游客":("admin".equals(user.getRole())?"管理员":("superAdmin".equals(user.getRole())?"超级管理员":"注册会员")))+"</td>");
			tbody.append("<td>"+user.getSign()+"</td>");
			if(user.getPhoto() == null)
				tbody.append("<td>"+"暂时没有上传头像"+"</td>");
			else
				tbody.append("<td>"+"<img src='/DJSBlog/"+Files.dao.findById(user.getPhoto()).getSrc()+"' class='img-circle img-shadowed' style='width:60px;height:60px'/>"+"</td>");
			tbody.append("<td>"+DateUtil.DateToString(user.getCreated(), DateStyle.MM_DD_HH_MM_CN)+"</td>");
			tbody.append("</tr>");
		}
		//构造分页
		tfoot = page(tfoot, Long.parseLong(parm[0]), pageBean.getPage(), pageBean.getPageSize(),12);
		return new String[]{thead.toString(),tbody.toString(),tfoot.toString()};
	}
	/**
	 * 文件列表代码
	 */
	public static String[] filesCode(List<Files> filesList, PageBean pageBean,
			String[] strings) {
		return null;
	}
	/**
	 * 博客列表代码 
	 */
	public static String[] blogCode(List<Blog> blogList, PageBean pageBean,
			String[] parm) {
		StringBuffer thead = new StringBuffer();//表头
		StringBuffer tbody = new StringBuffer();//身体
		StringBuffer tfoot = new StringBuffer();//分页
		//构建表头
		thead.append("<tr>");
		thead.append("<th><input type='checkbox' class='pull-left list-parent-check'/></th>");
		thead.append("<th style='text-align: center;'>编号</th>");
		thead.append("<th style='text-align: center;'>标题</th>");
		thead.append("<th style='text-align: center;'>作者</th>");
		thead.append("<th style='text-align: center;'>图片</th>");
		thead.append("<th style='text-align: center;'>日期</th>");
		thead.append("<th style='text-align: center;'>赞</th>");
		thead.append("<th style='text-align: center;'>踩</th>");
		thead.append("<th style='text-align: center;'>分享</th>");
		thead.append("<th style='text-align: center;'>浏览</th>");
		thead.append("<th style='text-align: center;'>概要</th>");
		thead.append("</tr>");
		//构建表格
		for(Blog blog:blogList){
			tbody.append("<tr>");
			tbody.append("<td><input type='checkbox' class='pull-left list-check' value='"+blog.getId()+"'/></td>");
			tbody.append("<td>"+blog.getId()+"</td>");	
			tbody.append("<td style='width:300px;'><a href='/DJSBlog/blog/getDetails/"+blog.getId()+"'  target='_blank'>"+blog.getTitle()+"</a></td>");	
			tbody.append("<td>"+User.dao.findById(blog.getUserId()).getNickName()+"</td>");	
			if(blog.getPhoto() == null)
				tbody.append("<td style='width:300px;'>"+"暂时没有上传头像"+"</td>");
			else
				tbody.append("<td style='width:125px;'>"+"<img src='/DJSBlog/"+Files.dao.findById(blog.getPhoto()).getSrc()+"' class='img-thumbnail' style='width:120px;height:80px'/>"+"</td>");
			tbody.append("<td>"+DateUtil.DateToString(blog.getCreated(), DateStyle.YYYY_MM_DD_HH_MM_CN)+"</td>");
			tbody.append("<td>"+blog.getVoteUp()+"</td>");	
			tbody.append("<td>"+blog.getVoteLow()+"</td>");	
			tbody.append("<td>"+blog.getShare()+"</td>");	
			tbody.append("<td>"+blog.getClick()+"</td>");	
			tbody.append("<td style='width:400px;'>"+(blog.getSummary().length()>50?blog.getSummary().substring(0, 50):blog.getSummary())+"</td>");	
			tbody.append("</tr>");
		}
		//构造分页
		tfoot = page(tfoot, Long.parseLong(parm[0]), pageBean.getPage(), pageBean.getPageSize(),11);
		return  new String[]{thead.toString(),tbody.toString(),tfoot.toString()};
	}
	/**
	 *信息后台代码 
	 */
	public static String[] messageCode(List<Message> messageList,
			PageBean pageBean, String[] parm) {
		StringBuffer thead = new StringBuffer();//表头
		StringBuffer tbody = new StringBuffer();//身体
		StringBuffer tfoot = new StringBuffer();//分页
		thead.append("<header class='listview-header media' id='thead'>");
		thead.append("<input type='checkbox' class='pull-left list-parent-check'>");
		thead.append("<ul class='list-inline pull-right m-t-5 m-b-0'>");
		thead.append("<li class='pagin-value hidden-xs' id='tfoot'></li>");
		thead.append("<li><a href='javascript:toPage("+(pageBean.getPage()-1>0?pageBean.getPage()-1:1)+");' title='上一页' class='tooltips'><i class='sa-list-back'></i></a></li>");
		thead.append("<li><a href='javascript:toPage("+(pageBean.getPage()+1<(Long.parseLong(parm[0])%pageBean.getPageSize()==0?Long.parseLong(parm[0])/pageBean.getPageSize():Long.parseLong(parm[0])/pageBean.getPageSize()+1)?pageBean.getPage():(Long.parseLong(parm[0])%pageBean.getPageSize()==0?Long.parseLong(parm[0])/pageBean.getPageSize():Long.parseLong(parm[0])/pageBean.getPageSize()+1))+");' title='下一页' class='tooltips'><i class='sa-list-forwad'></i></a></li>");
		thead.append("</ul><ul class='list-inline list-mass-actions pull-left'>");
		//发信箱 还是收信箱
		if("send".equals(parm[1])){
			thead.append("<li><a data-toggle='modal' href='javascript:write();' title='写信' class='tooltips'><i class='sa-list-add'></i></a></li>");
		}
		else{
			thead.append("<li><a href='javascript:ajaxDeleteOrComeback(\"get:true\");' title='删除' class='tooltips'><i class='sa-list-delete'></i></a></li>");
			thead.append("<li><a href='javascript:ajaxDeleteOrComeback(\"get:false\");' title='恢复' class='tooltips'><i class='sa-list-message'></i></a></li>");
		}
		thead.append("<li><a href='javascript:toMessage(\"rubbish\");' title='垃圾箱' class='tooltips'><i class='sa-list-unstable'></i></a></li>");
		thead.append("<li><a href='javascript:toMessage(\"send\");' title='发信箱' class='tooltips'><i class='sa-list-spam'></i></a></li>");
		thead.append("<li><a href='javascript:toMessage(\"get\");' title='收信箱' class='tooltips'><i class='sa-list-archive'></i></a></li></ul>");
		thead.append("<div class='clearfix'></div></header>");
		for(Message message:messageList){
//			System.out.println(messageList.size());
//			System.out.println(message.getGetId());
			tbody.append("<div class='media'>");
			tbody.append("<input type='checkbox' class='pull-left list-check' value='"+message.getId()+"'>");
			if("send".equals(parm[1]))
				tbody.append("<a class='media-body' href='javascript:edit("+message.getId()+");'>");
			else
				tbody.append("<a class='media-body' href='javascript:read("+message.getId()+");'>");
			tbody.append("<div class='pull-left list-title'>");
			//发信箱 还是收信箱
			if("send".equals(parm[1])){
				Record u = Db.findFirst("select u.*,f.src from user u,files f where u.id="+message.getGetId()+" and f.id=u.photo and u.del=false");
				tbody.append("<span class='t-overflow f-bold'>"+u.get("nickName")+"<img src='/DJSBlog/"+u.get("src")+"' class='img-circle img-shadowed' style='width:20px;height:20px'/></span>");
			}
			else{
				Record u = Db.findFirst("select u.*,f.src from user u,files f where u.id="+message.getSendId()+" and f.id=u.photo and u.del=false");
				tbody.append("<span class='t-overflow f-bold'>"+u.get("nickName")+"<img src='/DJSBlog/"+u.get("src")+"' class='img-circle img-shadowed' style='width:20px;height:20px'/></span>");
			}
			tbody.append("</div>");
			tbody.append("<div class='pull-right list-date'>"+DateUtil.DateToString(message.getCreated(), DateStyle.MM_DD_HH_MM_SS_EN)+"</div> ");
			tbody.append("<div class='media-body hidden-xs'>");
			tbody.append("<span class='t-overflow'>"+message.getTitle()+("get".equals(parm[1])?("unRead".equals(message.getState())?"<i class='n-count' style='margin-left:2px; margin-top:0px;'>N</i>":""):"")+"</span>");
			tbody.append("</div>");
			tbody.append("</a>");
			tbody.append("</div>");
		}
		tfoot.append(pageBean.getStart()+"--"+(pageBean.getStart()+pageBean.getPageSize()));
		return new String[]{thead.toString(),tbody.toString(),tfoot.toString()};
	}
	
	
	
	
	
	
}
