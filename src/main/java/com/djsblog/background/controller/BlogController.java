package com.djsblog.background.controller;

import java.util.Date;
import java.util.List;

import com.djsblog.entity.Blog;
import com.djsblog.entity.Files;
import com.djsblog.entity.Mapping;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;
import com.djsblog.enums.Options;
import com.djsblog.interceptor.AdminInterceptor;
import com.djsblog.service.BlogService;
import com.djsblog.service.impl.BlogServiceImpl;
import com.djsblog.util.CodeUtil;
import com.djsblog.util.DataMapUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
/**
 * 博客控制
 * @author 但劲松
 *
 */
@Before(AdminInterceptor.class)
public class BlogController extends Controller{
	
	private BlogService blogService = new BlogServiceImpl();
	/**
	 * 导航--缺省方法
	 */
	public void index(){
		setAttr("mainPage", "/back/blogManege/blogTable.jsp");
		setAttr("blog", true);
		setAttr("pathNow", "博客列表");
		render("/back/main.jsp");
	}
	/**
	 * 列表方法
	 */
	public void ajaxList(){
		PageBean pageBean = PageBean.creat(getPara("page"), Options.PAGESIZE+"");
		Blog s_blog = getModel(Blog.class,"s_blog");
		s_blog.setCreated(new Date());
		List<Blog> blogList = blogService.find(s_blog , pageBean); 
		long total = blogService.count(s_blog);
		String[] str = CodeUtil.blogCode(blogList, pageBean, new String[]{total+""});
		setAttr("total", total);
		setAttr("thead", str[0]);
		setAttr("tbody", str[1]);
		setAttr("tfoot", str[2]);
		renderJson();
	}
	/**
	 * id取实体
	 */
	public void ajaxGetById(){
		Blog blog = blogService.getById(getParaToLong("id"));
		setAttr("blog", blog);
		//获取分类映射
		List<Mapping> type = Mapping.dao.find("select * from mapping where fatherId="+blog.getId()
				+" and mapping_type='"+Options.BLOG_TYPE+"' and del='"+Options.DEL_FALSE+"'");
		setAttr("type", type);
		//获取标签映射
		List<Type> biaoqian = Type.dao.find("select * from type where id in(select sonId from mapping "
				+ "where fatherId="+blog.getId()+" and mapping_type='"+Options.BLOG_BIAOQIAN+"'"
						+ "and del='"+Options.DEL_FALSE+"')");
		setAttr("biaoqian", biaoqian);
		//获取专题映射
		Mapping zhuanti = Mapping.dao.findFirst("select * from mapping where fatherId="+blog.getId()
				+" and mapping_type='"+Options.BLOG_ZHUANTI+"' and del='"+Options.DEL_FALSE+"'");
		setAttr("zhuanti", zhuanti);
		renderJson();
	}
	/**
	 * 保存方法
	 */
	public void ajaxSave(){
		Blog blog = getModel(Blog.class,"blog");
		//获取分类
		String[] type=getPara("type").split(",");
		//获取标签
		String[] biaoqian = null;
		if(StrKit.notBlank(getPara("biaoqian")))
			biaoqian = getPara("biaoqian").split(" ");
		//获取专题
		Long zhuanti = getParaToLong("zhuanti");
		
 		if(blogService.save(blog,type,biaoqian,zhuanti))
			setAttr("success", true);
		else
			setAttr("success", false);
		//更新session中的统计
 		if(getSessionAttr("data_map")!=null){
 			setSessionAttr("data_map", DataMapUtil.updateDataMap());
 		}
 		//重载首页动图
 		if(getSessionAttr("bannerImg")!=null){
 			List<Files> bannerImg  = Files.dao.find("select * from files where id in (select photo from blog where del='false') order by created desc limit 0,3");
			setSessionAttr("bannerImg", bannerImg);
 		}
 		//重载载4条最新博客
		if(getSessionAttr("fourNewBlog")!=null){
			//Db Record 模式查询
			List<Record> fourNewBlog  = Db.find("SELECT u.nickName,f.src,b.* FROM blog b,files f,user u WHERE b.del='false' AND b.`photo`=f.`id` and b.userId=u.id ORDER BY b.created DESC LIMIT 0,4");
			setSessionAttr("fourNewBlog", fourNewBlog);
		}
		renderJson();
	}
	/**
	 * 批量恢复或者删除
	 */
	public void ajaxDeleteOrComeback(){
		String del = getPara("del");
		String[] ids=getPara("ids").split(",");
		boolean flag = false;
		for(String id:ids)
			flag=blogService.deleteOrComeback(Long.parseLong(id), del);
		if(flag)
			setAttr("success", true);
		else
			setAttr("success", false);
		renderJson();
	}
}
