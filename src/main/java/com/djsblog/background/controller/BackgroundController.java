package com.djsblog.background.controller;

import java.util.Date;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.User;
import com.djsblog.interceptor.AdminInterceptor;
import com.djsblog.service.DataDicService;
import com.djsblog.service.impl.DataDicServiceImpl;
import com.djsblog.service.impl.FilesServiceImpl;
import com.djsblog.util.DataMapUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;

/**
 * 前台页面控制器
 * @author 但劲松
 *
 */
@Before(AdminInterceptor.class)
public class BackgroundController extends Controller {
	/**
	 * 首页
	 */
	public void index(){
		setAttr("mainPage", "/back/common/content.jsp");
		//获取皮肤
		User user = getSessionAttr("currentUser");
		if(user!=null&&getSessionAttr("skin")==null){
			DataDicService dataDicService = new DataDicServiceImpl();
			String skin = dataDicService.getByKey(user.getId()+"_SKIN");
			setSessionAttr("skin", skin);
		}
		//获取照片墙
		if(getSessionAttr("photoWall")==null){
			Files s_files = new Files();s_files.setCreated(new Date());
			List<Files> photoWall = new FilesServiceImpl().find(s_files, PageBean.creat("1", "12"));
			setSessionAttr("photoWall", photoWall);
		}
//		  String q = "浩然";
//		  List<com.djsblog.entity.Blog> bs = new com.djsblog.util.LunceUtil().queryBlog(q);
//		  for(com.djsblog.entity.Blog b:bs) System.out.println(b);
		//获取查询数据映射
		if(getSessionAttr("data_map")==null){
			setSessionAttr("data_map", DataMapUtil.updateDataMap());
		}
		setAttr("home", true);
		setAttr("pathNow", "管理中心");
		render("/back/main.jsp");
	}
	/**
	 * 页面请求
	 */
	public void pageRequest() {   
		String view = getPara("view");
		String page = getPara("page");
		render("/page/" + view + "/" + page + ".jsp");
	}
}
