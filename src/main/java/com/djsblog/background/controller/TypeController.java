package com.djsblog.background.controller;

import java.util.Date;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;
import com.djsblog.enums.Options;
import com.djsblog.interceptor.AdminInterceptor;
import com.djsblog.service.TypeService;
import com.djsblog.service.impl.FilesServiceImpl;
import com.djsblog.service.impl.TypeServiceImpl;
import com.djsblog.util.CodeUtil;
import com.djsblog.util.DataMapUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

/**
 * 博客分类核心控制类
 * @author 但劲松
 *
 */
@Before(AdminInterceptor.class)
public class TypeController extends Controller{
	
	private TypeService typeService = new TypeServiceImpl();
	/**
	 * 缺省方法
	 */
	public void index(){
		setAttr("mainPage", "/back/blogManege/typeTable.jsp");
		setAttr("blog", true);
		setAttr("pathNow", "分类列表");
		render("/back/main.jsp");
	}
	/**
	 * 列表方法
	 */
	public void ajaxList(){
		PageBean pageBean = PageBean.creat(getPara("page"), Options.PAGESIZE+"");
		Type s_type = getModel(Type.class,"s_type");
		List<Type> typeList = typeService.find(s_type , pageBean); 
		long total = typeService.count(s_type);
		String[] str = CodeUtil.typeBackCode(typeList, pageBean, new String[]{total+""});
		setAttr("total", total);
		setAttr("thead", str[0]);
		setAttr("tbody", str[1]);
		setAttr("tfoot", str[2]);
		renderJson();
	}
	/**
	 * 添加方法
	 */
	public void ajaxSave(){
		Type type = getModel(Type.class,"type");
		//如果同时选择加入导航，也有父节点，那么，选择父节点，不加入导航
		if(StrKit.isBlank(type.getTypeType())||"0".equals(type.getTypeType())){
			if(StrKit.notBlank(getPara("joinState")))
				if(getParaToBoolean("joinState"))
					type.setTypeType(Options.G_FATHER);//祖父节点加入导航
		}
		//如果没有选择父节点 ，也没有加入导航
		if("0".equals(type.getTypeType()))
			type.setTypeType("");
		
		if(typeService.save(type))
			setAttr("success", true);
		else
			setAttr("success", false);
		//更新session中的图片
 		Files s_files = new Files();s_files.setCreated(new Date());
		List<Files> photoWall = new FilesServiceImpl().find(s_files, PageBean.creat("1", "12"));
		setSessionAttr("photoWall", photoWall);
		//更新session中的统计
 		setSessionAttr("data_map", DataMapUtil.updateDataMap());
		
		renderJson();
	}
	/**
	 * id取实体
	 */
	public void ajaxGetById(){
		Type type = typeService.getById(getParaToLong("id"));
		setAttr("type", type);
		renderJson();
	}
	/**
	 * 获取父节点列表
	 */
	public void ajaxFindFa(){
		Type s_type = new Type();
		s_type.setTypeType(Options.G_FATHER);
		s_type.setDel(Options.DEL_FALSE);
		List<Type> typeList = typeService.find(s_type  , null); 
		setAttr("typeList", typeList);
		renderJson();
	}
	/**
	 * 获取所有类别
	 */
	public void ajaxFindType(){
		Type s_type = new Type();
		s_type.setDel(Options.DEL_FALSE);
		s_type.setTypeType("TYPE");
		List<Type> typeList = typeService.find(s_type, null); 
		setAttr("typeList", typeList);
		renderJson();
	}
	/**
	 * 获取所有专题
	 */
	public void ajaxFindzhuanti(){
		List<Type> ztList = Type.dao.find("select * from type where type_type='"+Options.G_ZHUANTI+"' and del='"+Options.DEL_FALSE+"'");
		setAttr("ztList", ztList);
		renderJson();
	}
	/**
	 * 批量恢复或者删除
	 */
	public void ajaxDeleteOrComeback(){
		String del = getPara("del");
		String[] ids=getPara("ids").split(",");
		boolean flag = false;
		for(String id:ids)
			flag=typeService.deleteOrComeback(Long.parseLong(id), del);
		if(flag)
			setAttr("success", true);
		else
			setAttr("success", false);
		renderJson();
	}
}
