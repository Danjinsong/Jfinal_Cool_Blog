package com.djsblog.background.controller;

import java.util.List;

import com.djsblog.entity.PageBean;
import com.djsblog.entity.Datadic;
import com.djsblog.enums.Options;
import com.djsblog.interceptor.AdminInterceptor;
import com.djsblog.service.DataDicService;
import com.djsblog.service.impl.DataDicServiceImpl;
import com.djsblog.util.CodeUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
/**
 * 数据字典控制类
 * @author 但劲松
 *
 */
@Before(AdminInterceptor.class)
public class DataDicController extends Controller{
	private DataDicService dataDicService = new DataDicServiceImpl();
	/**
	 * 导航--缺省方法
	 */
	public void index(){
		setAttr("mainPage", "/back/dataDic/dataDicTable.jsp");
		setAttr("dataDic", true);
		setAttr("pathNow", "数据字典");
		render("/back/main.jsp");
	}
	/**
	 * 列表方法
	 */
	public void ajaxList(){
		PageBean pageBean = PageBean.creat(getPara("page"), Options.PAGESIZE+"");
		Datadic s_datadic = getModel(Datadic.class,"s_datadic");
		List<Datadic> datadicList = dataDicService.find(s_datadic , pageBean); 
		long total = dataDicService.count(s_datadic);
		String[] str = CodeUtil.datadicCode(datadicList, pageBean, new String[]{total+""});
		setAttr("total", total);
		setAttr("thead", str[0]);
		setAttr("tbody", str[1]);
		setAttr("tfoot", str[2]);
		renderJson();
	}
	/**
	 * id取实体
	 */
	public void ajaxGetById(){
		Datadic datadic = dataDicService.getById(getParaToLong("id"));
		setAttr("datadic", datadic);
		renderJson();
	}
	/**
	 * 保存方法
	 */
	public void ajaxSave(){
		Datadic datadic = getModel(Datadic.class,"datadic");
		if(dataDicService.save(datadic))
			setAttr("success", true);
		else
			setAttr("success", false);
		renderJson();
	}
	/**
	 * 批量恢复或者删除
	 */
	public void ajaxDeleteOrComeback(){
		String del = getPara("del");
		String[] ids=getPara("ids").split(",");
		boolean flag = false;
		for(String id:ids)
			flag=dataDicService.deleteOrComeback(Long.parseLong(id), del);
		if(flag)
			setAttr("success", true);
		else
			setAttr("success", false);
		renderJson();
	}
}
