package com.djsblog.background.controller;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.enums.Options;
import com.djsblog.interceptor.AdminInterceptor;
import com.djsblog.service.FilesService;
import com.djsblog.service.impl.FilesServiceImpl;
import com.djsblog.util.CodeUtil;
import com.djsblog.util.PicUtil;
//import com.djsblog.util.FileUtil;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.upload.UploadFile;
/**
 * 文件上传控制
 * @author 但劲松
 *
 */
@Before(AdminInterceptor.class)
public class FilesController extends Controller{
	
	private FilesService filesService = new FilesServiceImpl();
	/**
	 * 导航--缺省方法
	 */
	public void index(){
		setAttr("mainPage", "/back/files/filesTable.jsp");
		
		Files s_files = new Files();
		s_files.setCreated(new Date());
		//图片
		s_files.setType("image");
		List<Files> filesList = filesService.find(s_files , null); 
		setAttr("filesList", filesList);
		
		setAttr("pathNow", "文件列表");
		
		setAttr("files", true);
		render("/back/main.jsp");
	}
	/**
	 * 列表
	 */
	public void ajaxList(){
		PageBean pageBean = PageBean.creat(getPara("page"), Options.PAGESIZE+"");
		Files s_files = getModel(Files.class,"s_files");
		List<Files> filesList = filesService.find(s_files , pageBean); 
		long total = filesService.count(s_files);
		String[] str = CodeUtil.filesCode(filesList, pageBean, new String[]{total+""});
		setAttr("total", total);
		setAttr("thead", str[0]);
		setAttr("tbody", str[1]);
		setAttr("tfoot", str[2]);
		renderJson();
	}
	/**
	 * id取实体
	 */
	public void ajaxGetById(){
		Files files = filesService.getById(getParaToLong("id"));
		setAttr("files", files);
		renderJson();
	}
	/**
	 * 保存方法
	 */
	public void ajaxSave(){
		try {
		//获取请求参数
		Integer width = getParaToInt("width");
		Integer height = getParaToInt("height");
//		System.out.println(height);
//		System.out.println(width);
		UploadFile file = getFile("image","\\userImages")==null?getFile("user_image","\\userImages"):getFile("image","\\userImages");
		String type = file.getOriginalFileName().substring(file.getOriginalFileName().lastIndexOf("."));//文件类型
		String originFileName = file.getOriginalFileName();	//文件名
		//构造两份文件
		File file1 = file.getFile();
	//	File file2 = new File("D://newStarts/DJSBlog/src/main/webapp/static/userImages/", originFileName);
	//	FileUtil.Copy(file1, file2.getPath());
		//写入数据库
		Files files = new Files();
		files.setId(getParaToLong("files.id"));
		files.setName(file.getParameterName());
		files.setType(file.getContentType());
		files.setSrc("/static/userImages/"+originFileName);
		files.setSuffix(type);
		if(height != null && width != null){
			PicUtil.resize(file1.getPath(), file1.getPath(), width, height);//裁剪合适尺寸
//			Thumbnails.of(file1.getPath())  
//				.size(width, height).keepAspectRatio(false)
//				.toFile(file1.getPath()); 
		}
		if(filesService.save(files)){
			setAttr("success", true);
			setAttr("url",files.getSrc());
			setAttr("filesId",files.getId());
			Files s_files = new Files();
			s_files.setType("image");
			List<Files> photoWall = Files.dao.find("select * from files where type like '%image%' and del=false order by created desc limit 0,12");
			setSessionAttr("photoWall", photoWall);
		}
		else
			setAttr("success", false);
		renderJson();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * ajax 上传歌曲
	 */
	public void ajaxSaveSong(){
		try{
			UploadFile file = getFile("mp3", "\\voice",1024*1024*100);
			String type = file.getOriginalFileName().substring(file.getOriginalFileName().lastIndexOf("."));//文件类型
			String originFileName = file.getOriginalFileName();	//文件名
			//写入数据库
			Files files = new Files();
			files.setId(getParaToLong("files.id"));
			files.setName(file.getOriginalFileName());
			files.setType(file.getContentType());
			files.setSrc("/static/voice/"+originFileName);
			files.setSuffix(type);
			if(filesService.save(files)){
				setAttr("success", true);
				setAttr("name",file.getOriginalFileName());
				setAttr("filesId",files.getId());
			}
			else
				setAttr("success", false);
			renderJson();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
