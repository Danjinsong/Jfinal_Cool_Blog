package com.djsblog.routes;

import com.djsblog.background.controller.BackgroundController;
import com.djsblog.background.controller.BlogController;
import com.djsblog.background.controller.DataDicController;
import com.djsblog.background.controller.FilesController;
import com.djsblog.background.controller.MessageController;
import com.djsblog.background.controller.TypeController;
import com.djsblog.background.controller.UserController;
import com.jfinal.config.Routes;
/**
 * 后端路由
 * @author 但劲松
 *
 */
public class BackGroundRoutes extends Routes{

	@Override
	public void config() {
		add("/back",BackgroundController.class);
		add("/back/user",UserController.class);
		add("/back/type",TypeController.class);
		add("/back/datadic",DataDicController.class);
		add("/back/files",FilesController.class);
		add("/back/blog",BlogController.class);
		add("/back/message",MessageController.class);
	}

}
