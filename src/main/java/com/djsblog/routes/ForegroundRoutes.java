package com.djsblog.routes;

import com.djsblog.foreground.controller.BlogController;
import com.djsblog.foreground.controller.CommentController;
import com.djsblog.foreground.controller.ForegroundController;
import com.jfinal.config.Routes;
/**
 * 前段路由
 * @author 但劲松
 *
 */
public class ForegroundRoutes extends Routes {
	
	public void config() {
		add("/", ForegroundController.class);
		add("/blog", BlogController.class);
		add("/comment", CommentController.class);
	}
}