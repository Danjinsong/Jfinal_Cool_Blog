package com.djsblog.config;

import com.djsblog.entity._MappingKit;
import com.djsblog.interceptor.GlobalInterceptor;
import com.djsblog.routes.BackGroundRoutes;
import com.djsblog.routes.ForegroundRoutes;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.render.ViewType;
/**
 * 项目配置
 * @author 但劲松
 *
 */
public class APPConfig extends JFinalConfig{
	/**
	 * 常量配置
	 */
	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true); // 开发模式 （控制台打印出日志）
		me.setViewType(ViewType.JSP); // 配置视图 默认是freemarker
		PropKit.use("jdbc.txt"); // 数据库配置文件，发觉不管放到哪里都可以，没有路径
		me.setEncoding("UTF-8"); // 设置字符集
		me.setBaseUploadPath(PathKit.getWebRootPath()+"\\static");//设置文件上传路径
		
	}
	/**
	 * 路由配置
	 */
	@Override
	public void configRoute(Routes me) {
		 me.add(new ForegroundRoutes()); // 前端路由   
		 me.add(new BackGroundRoutes()); // 后端路由 
	}
	/**
	 * 插件配置
	 */
	@Override
	public void configPlugin(Plugins me) {
		// 配置Druid数据库连接池插件
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbcurl"),
				PropKit.get("username"), PropKit.get("password").trim());
		me.add(druidPlugin);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		me.add(arp);
		arp.setShowSql(true);
		//调用自动生成的映射方法
		_MappingKit.mapping(arp);
		
		// ehcache缓存插件
       // me.add(new EhCachePlugin());

	}
	/**
	 * 全局拦截器配置
	 */
	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new GlobalInterceptor());
	}
	/**
	 * 接管容器配置
	 */
	@Override
	public void configHandler(Handlers me) {
		me.add(new ContextPathHandler("contextPath")); // 设置上下文路径
	}

}
