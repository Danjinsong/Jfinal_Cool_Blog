package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;

/**
 * 文件事务接口
 * @author 但劲松
 *
 */
public interface FilesService {
	/**
	 * 列表
	 * @param s_files
	 * @param pageBean
	 * @return
	 */
	public List<Files> find(Files s_files,PageBean pageBean);
	/**
	 * 数量
	 * @param s_files
	 * @return
	 */
	public long count(Files s_files);
	/**
	 * 保存
	 * @param files
	 * @return
	 */
	public boolean save(Files files);
	/**
	 * 删除/恢复
	 * @param id
	 * @return
	 */
	public boolean deleteOrComeback(Long id,String del);
	/**
	 * id取对象
	 * @param id
	 * @return
	 */
	public Files getById(Long id);
}
