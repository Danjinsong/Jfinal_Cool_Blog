package com.djsblog.service.impl;

import java.sql.SQLException;
import java.util.List;

import com.djsblog.entity.Message;
import com.djsblog.entity.PageBean;
import com.djsblog.enums.Options;
import com.djsblog.service.MessageService;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.tx.Tx;
/**
 * 信息事务接口实现
 * @author 但劲松
 *
 */
@Before(Tx.class)
public class MessageServiceImpl implements MessageService{

	public List<Message> find(Message s_message, PageBean pageBean) {
		StringBuffer sql = new StringBuffer("select * from message ");
		if(s_message!=null){
			//收信箱还是发信箱
			if(s_message.getGetId()!=null)
				sql.append(" and getId='"+s_message.getGetId()+"'");
			if(s_message.getSendId()!=null)
				sql.append(" and sendId='"+s_message.getSendId()+"'");
			//信息阅读状态
			if(StrKit.notBlank(s_message.getState()))
				sql.append(" and state='"+s_message.getState()+"'");
			//发信件删除标识
			if(StrKit.notBlank(s_message.getSendDel()))
				sql.append(" and send_del='"+s_message.getSendDel()+"'");
			//收信件删除标识
			if(StrKit.notBlank(s_message.getGetDel()))
				sql.append(" and get_del='"+s_message.getGetDel()+"'");
			//构建模糊查询标题
			if(StrKit.notBlank(s_message.getTitle()))
				sql.append(" and title like '%"+s_message.getTitle()+"%'");
			//时间倒序排序	
			if(s_message.getCreated()!=null)
				sql.append(" order by created desc");
		}
		//分页
		if(pageBean != null) sql.append(" limit "+pageBean.getStart()+","+pageBean.getPageSize());
		return Message.dao.find(sql.toString().replaceFirst(" and", " where"));
	}

	public Long count(Message s_message) {
		StringBuffer sql = new StringBuffer("select count(*) as total from message ");
		if(s_message!=null){
			//收信箱还是发信箱
			if(s_message.getGetId()!=null)
				sql.append(" and getId='"+s_message.getGetId()+"'");
			if(s_message.getSendId()!=null)
				sql.append(" and sendId='"+s_message.getSendId()+"'");
			//信息阅读状态
			if(StrKit.notBlank(s_message.getState()))
				sql.append(" and state='"+s_message.getState()+"'");
			//发信件删除标识
			if(StrKit.notBlank(s_message.getSendDel()))
				sql.append(" and send_del='"+s_message.getSendDel()+"'");
			//收信件删除标识
			if(StrKit.notBlank(s_message.getGetDel()))
				sql.append(" and get_del='"+s_message.getGetDel()+"'");
			//构建模糊查询标题
			if(StrKit.notBlank(s_message.getTitle()))
				sql.append(" and title like '%"+s_message.getTitle()+"%'");
		}
		return Db.queryLong(sql.toString().replaceFirst(" and", " where"));
	}

	public boolean save(Message message) {
		try{
			if(message.getId() == null) return message.save();
			
			return message.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

	public Message getById(Long id,String read) {
		try{	
			if(StrKit.isBlank(read)){
				Message m = new Message();
				m.setId(id);
				m.setState(Options.MESSAGE_READ);
				m.update();
			}
			return Message.dao.findById(id);
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return null;
	}
	public boolean deleteOrComeback(Long id, String deleteOrUpdate) {
		try{
			String del = deleteOrUpdate.split(":")[1];
			Message message = new Message();
			message.setId(id);
			if(deleteOrUpdate.startsWith("send")) message.setSendDel(del);
			else if(deleteOrUpdate.startsWith("get")) message.setGetDel(del);
			return message.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

}
