package com.djsblog.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;
import com.djsblog.enums.Options;
import com.djsblog.service.TypeService;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.tx.Tx;
/**
 * 博客分类Service接口实现
 * @author 但劲松
 *
 */
@Before(Tx.class)
public class TypeServiceImpl implements TypeService{

	public List<Type> find(Type s_type, PageBean pageBean) {
		StringBuffer  sql = new StringBuffer("select * from type ");
		if(s_type!=null){
			if(StrKit.notBlank(s_type.getDel()))
				sql.append(" and del = '"+s_type.getDel()+"'");
			if(StrKit.notBlank(s_type.getName()))
				sql.append(" and name like '%"+s_type.getName()+"%'");
			if(StrKit.notBlank(s_type.getTypeType())&&s_type.getTypeType().equals("TYPE"))
				sql.append(" and type_type  <> 'g_zhuanti' and type_type <> 'g_biaoqian'");
			else if(StrKit.notBlank(s_type.getTypeType()))
					sql.append(" and type_type = '"+s_type.getTypeType()+"'");
		}
		if(pageBean != null)
			sql.append(" limit "+pageBean.getStart()+","+pageBean.getPageSize());
		return Type.dao.find(sql.toString().replaceFirst(" and", " where"));
	}

	public Long count(Type s_type) {
		StringBuffer  sql = new StringBuffer("select count(*) as total from type ");
		if(s_type!=null){
			if(StrKit.notBlank(s_type.getDel()))
				sql.append(" and del = '"+s_type.getDel()+"'");
			if(StrKit.notBlank(s_type.getName()))
				sql.append(" and name like '%"+s_type.getName()+"%'");
			if(StrKit.notBlank(s_type.getTypeType()))
				sql.append(" and type_type = '"+s_type.getTypeType()+"'");
		}
		return Db.queryLong(sql.toString().replaceFirst(" and", " where"));
	}

	public boolean save(Type type) {
		try{
			if(type.getId()==null){
				Type s_type = Type.dao.findFirst("select * from type where `name`='"+type.getName()+"'");
				//name不存在
				if(s_type == null)
					return type.save();
				//name存在
				return false;
			}
			if(type.getPhoto() != null){
				Files f = Files.dao.findById(type.getPhoto());
				f.setName(type.getName()+"的图片");
				f.setCreated(new Date());
				f.update();
			}
			return type.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

	public Type getById(Long id) {
		return Type.dao.findById(id);
	}

	public boolean deleteOrComeback(Long id, String deleteOrComeback) {
		try{
			Type type = new Type();
			type.setId(id);
			//恢复
			if(Options.DEL_FALSE.equals(deleteOrComeback)) type.setDel(Options.DEL_FALSE);
			//删除
			else  type.setDel(Options.DEL_TRUE);
			return type.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

}
