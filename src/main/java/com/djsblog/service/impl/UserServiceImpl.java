package com.djsblog.service.impl;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.djsblog.entity.Files;
import com.djsblog.entity.Mapping;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.User;
import com.djsblog.enums.Options;
import com.djsblog.service.UserService;
import com.jfinal.aop.Before;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.tx.Tx;

/**
 * 用户事务接口实现
 * @author 但劲松
 *
 */
@Before(Tx.class)
public class UserServiceImpl implements UserService{

	public boolean isExUserName(String userName) {
		String sql = "select * from user where userName='"+userName+"'";
		User u = User.dao.findFirst(sql);
		return u==null?true:false;
	}

	public boolean isExNickName(String nickName) {
		String sql = "select * from user where nickName='"+nickName+"'";
		User u = User.dao.findFirst(sql);
		return u==null?true:false;
	}

	public boolean saveOrUpdate(User user) {
		try{
			if(user.getId()==null) return user.save();
			return Db.update("update 'user' set 'realName'="+user.getRealName()+"")>0?true:false;
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

	public User login(User user) {
		String sql = "select * from user where userName='"+user.getUserName()+"'";
		User u = User.dao.findFirst(sql);
		return u;
	}

	public List<User> find(User s_user, PageBean pageBean) {
		StringBuffer sql = new StringBuffer("select * from user ");
		if(s_user!=null){
			//删除标识符
			if(StrKit.notBlank(s_user.getDel()))
				sql.append(" and del='"+s_user.getDel()+"'");
			//模糊查询用户名
			if(StrKit.notBlank(s_user.getUserName()))
				sql.append(" and userName like '%"+s_user.getUserName()+"%'");
			//时间倒序排序	
			if(s_user.getCreated()!=null)
				sql.append(" order by created desc");
		}
		if(pageBean != null)
			sql.append(" limit "+pageBean.getStart()+","+pageBean.getPageSize());
		return User.dao.find(sql.toString().replaceFirst(" and", " where"));
	}

	public long count(User s_user) {
		StringBuffer sql = new StringBuffer("select count(*) as total from user ");
		if(s_user!=null){
			if(StrKit.notBlank(s_user.getDel()))
				sql.append(" and del='"+s_user.getDel()+"'");
			if(StrKit.notBlank(s_user.getUserName()))
				sql.append(" and userName like '%"+s_user.getUserName()+"'");
		}
		return Db.queryLong(sql.toString().replaceFirst(" and", " where"));
	}

	public boolean deleteOrComeback(long id, String del) {
		try{
			User user = new User();
			user.setId(id);
			user.setDel(del);
		//	System.out.println(del);
			return user.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

	public boolean save(User user) {
		try{
			if(user.getId() == null){
				//检测用户名
				User s_user = User.dao.findFirst("select * from user where userName='"+user.getUserName()+"'");
				//用户名已存在
				if(s_user != null)
					return false;
				//用户名不存在
				return user.save();
			}
			if(user.getPhoto() != null){
				Files f = Files.dao.findById(user.getPhoto());
				f.setName(user.getNickName()+"的头像");
				f.setCreated(new Date());
				f.update();
			}
			return user.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

	public User getById(Long id) {
		return User.dao.findById(id);
	}

	public boolean foucus(Long currentUserId, Long id) {
		try{
			//不要关注自己
			if(currentUserId == id) return false;
			
			List<Mapping> myFoucus = Mapping.dao.find("select * from mapping where mapping_type='"+Options.USER_FOUCUS+"' and del='"+Options.DEL_FALSE+"' and fatherId="+currentUserId);
			//查看是否关注过
			for(Mapping m : myFoucus){
				if(m.getSonId() == id)
					return false;
			}
			//构建新的关注对象
			Mapping m  = new Mapping();
			m.setFatherId(currentUserId);
			m.setSonId(id);
			m.setMappingType(Options.USER_FOUCUS);
			return m.save();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

}
