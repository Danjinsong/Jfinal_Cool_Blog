package com.djsblog.service.impl;

import java.sql.SQLException;

import com.djsblog.entity.Comment;
import com.djsblog.entity.Mapping;
import com.djsblog.service.CommentService;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.tx.Tx;
/**
 * 博客评论事务接口
 * @author 但劲松
 *
 */
@Before(Tx.class)
public class CommentServiceImpl  implements CommentService{

	public boolean save(Comment comment,Long faId) {
		try{
			boolean flag = false;
			if(comment.getId() == null){
				flag = comment.save();
				//构建父映射
				if(faId != null){
					Mapping mapping = new Mapping();
					mapping.setFatherId(faId);
					mapping.setSonId(comment.getId());
					flag = mapping.save();
				}
				return flag;
			}
			return comment.update();
		}catch(Exception e){
			try {
				//执行失败回滚事务
				DbKit.getConfig().getConnection().rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			} 
		}
		return false;
	}

}
