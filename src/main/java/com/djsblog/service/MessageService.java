package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.PageBean;
import com.djsblog.entity.Message;

/**
 * 信息事务接口
 * @author 但劲松
 *
 */
public interface MessageService {

	/**
	 * 列表方法
	 * @param s_message
	 * @param pageBean
	 * @return
	 */
	public List<Message> find(Message s_message,PageBean pageBean);
	/**
	 * 查询数量
	 * @param s_message
	 * @return
	 */
	public Long count(Message s_message);
	/**
	 * 保存方法
	 * @param message
	 */
	public boolean save(Message message);
	/**
	 * 通过Id获取实体
	 * @param id
	 * @return
	 */
	public Message getById(Long id,String read);
	/**
	 * 删除或者恢复
	 * @param id
	 * @param deleteOrUpdate
	 * @return
	 */
	public boolean deleteOrComeback(Long id,String deleteOrUpdate);
}
