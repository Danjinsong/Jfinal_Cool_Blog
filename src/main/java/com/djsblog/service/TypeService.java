package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;

/**
 * 博客分类Service接口
 * @author 但劲松
 *
 */
public interface TypeService {
	/**
	 * 列表方法
	 * @param s_blogType
	 * @param pageBean
	 * @return
	 */
	public List<Type> find(Type s_blogType,PageBean pageBean);
	/**
	 * 查询数量
	 * @param s_blogType
	 * @return
	 */
	public Long count(Type s_blogType);
	/**
	 * 保存方法
	 * @param type
	 */
	public boolean save(Type type);
	/**
	 * 通过Id获取实体
	 * @param id
	 * @return
	 */
	public Type getById(Long id);
	/**
	 * 删除或者恢复
	 * @param id
	 * @param deleteOrUpdate
	 * @return
	 */
	public boolean deleteOrComeback(Long id,String deleteOrUpdate);
}
