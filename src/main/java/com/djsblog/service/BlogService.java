package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.PageBean;
import com.djsblog.entity.Blog;

/**
 * 博客事务接口
 * @author 但劲松
 *
 */
public interface BlogService {
	/**
	 * 列表方法
	 * @param s_blog
	 * @param pageBean
	 * @return
	 */
	public List<Blog> find(Blog s_blog,PageBean pageBean);
	/**
	 * 查询数量
	 * @param s_blog
	 * @return
	 */
	public Long count(Blog s_blog);
	/**
	 * 保存方法
	 * @param blog
	 */
	public boolean save(Blog blog,String[] type,String[] biaoqian,Long zhuanti);
	/**
	 * 通过Id获取实体
	 * @param id
	 * @return
	 */
	public Blog getById(Long id);
	/**
	 * 删除或者恢复
	 * @param id
	 * @param deleteOrUpdate
	 * @return
	 */
	public boolean deleteOrComeback(Long id,String deleteOrUpdate);
	/**
	 * 更新博客点击 赞或者踩
	 * @return
	 */
	public boolean updateThings(Long id,String uOlOc);
	/**
	 * 添加一条评论
	 * @param blogId
	 */
	public boolean addComment(Long blogId);
}
