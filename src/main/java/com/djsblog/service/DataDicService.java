package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.Datadic;
import com.djsblog.entity.PageBean;
/**
 * 数据字典事务接口
 * @author 但劲松
 *
 */
public interface DataDicService {
	/**
	 * 列表
	 * @param s_datadic
	 * @param pageBean
	 * @return
	 */
	public List<Datadic> find(Datadic s_datadic,PageBean pageBean);
	/**
	 * 数量
	 * @param s_datadic
	 * @return
	 */
	public long count(Datadic s_datadic);
	/**
	 * 保存
	 * @param datadic
	 * @return
	 */
	public boolean save(Datadic datadic);
	/**
	 * 删除/恢复
	 * @param id
	 * @return
	 */
	public boolean deleteOrComeback(Long id,String del);
	/**
	 * id取对象
	 * @param id
	 * @return
	 */
	public Datadic getById(Long id);
	/**
	 * 通过key查询val
	 * @param key
	 * @return
	 */
	public String getByKey(String key);
}
