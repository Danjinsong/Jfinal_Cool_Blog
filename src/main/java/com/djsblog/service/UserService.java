package com.djsblog.service;

import java.util.List;

import com.djsblog.entity.PageBean;
import com.djsblog.entity.User;


/**
 * 用户事务接口
 * @author 但劲松
 *
 */
public interface UserService {
	/**
	 * 验证用户名
	 * @param userName
	 * @return
	 */
	public boolean isExUserName(String userName);
	/**
	 * 验证昵称
	 * @param nickName
	 * @return
	 */
	public boolean isExNickName(String nickName);
	/**
	 * 保存
	 * @param user
	 * @return
	 */
	public boolean saveOrUpdate(User user);
	/**
	 * 登录
	 * @param user
	 * @return
	 */
	public User login(User user);
	/**
	 * 列表
	 * @param s_user
	 * @param pageBean
	 * @return
	 */
	public List<User> find(User s_user, PageBean pageBean);
	/**
	 * 数量
	 * @param s_user
	 * @return
	 */
	public long count(User s_user);
	/**
	 *删除恢复 
	 */
	public boolean deleteOrComeback(long parseLong, String del);
	/**
	 * 保存
	 */
	public boolean save(User user);
	/**
	 * Id取得实体
	 * @param id
	 * @return
	 */
	public User getById(Long id);
	/**
	 * 关注某人
	 * @param currentUserId
	 * @param id
	 * @return
	 */
	public boolean foucus(Long currentUserId,Long id);

}
