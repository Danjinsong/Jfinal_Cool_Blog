package com.djsblog.service;

import com.djsblog.entity.Comment;


/**
 * 评论事务接口
 * @author 但劲松
 *
 */
public interface CommentService {

	/**
	 * 保存方法
	 * @param comment
	 */
	public boolean save(Comment comment,Long faId);
	
	
}
