package com.djsblog.foreground.controller;

import java.util.List;

import com.djsblog.entity.Comment;
import com.djsblog.service.BlogService;
import com.djsblog.service.CommentService;
import com.djsblog.service.impl.BlogServiceImpl;
import com.djsblog.service.impl.CommentServiceImpl;
import com.djsblog.util.DemoUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 评论控制
 * @author 但劲松
 *
 */
public class CommentController extends Controller{
	//评论事务接口实现
	private CommentService commentService  = new CommentServiceImpl();
	//博客事务接口实现
	private BlogService blogService = new BlogServiceImpl();
	/**
	 * 添加评论
	 */
	public void addComment(){
		Long blogId = getParaToLong("blogId");
		//对应博客的评论数量+1
		boolean flag = blogService.addComment(blogId);
		
		Comment comment = getModel(Comment.class,"comment");
		flag  = commentService.save(comment,getParaToLong("faId"));
		if(flag){
			setAttr("success", true);
			//重载评论列表
			List<Record> comments = Db.find("SELECT * FROM COMMENT WHERE del='false' AND blogId="+blogId+" ORDER BY created DESC");
			setAttr("comments", DemoUtil.getComments(comments).toString());
		}
		else
			setAttr("success", false);
		
		renderJson();
	}
}
