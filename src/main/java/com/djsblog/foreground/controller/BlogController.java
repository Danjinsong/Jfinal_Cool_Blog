package com.djsblog.foreground.controller;

import java.util.List;

import com.djsblog.entity.Blog;
import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.Type;
import com.djsblog.enums.Options;
import com.djsblog.service.BlogService;
import com.djsblog.service.impl.BlogServiceImpl;
import com.djsblog.util.DataMapUtil;
import com.djsblog.util.DemoUtil;
import com.djsblog.util.LunceUtil;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
/**
 * 前台博客控制
 * @author 但劲松
 *
 */
public class BlogController extends Controller{
	//获取事务接口
	private BlogService blogService = new BlogServiceImpl();
	/**
	 * 缺省方法
	 */
	public void index(){
		
	}
	/**
	 * 为前台请求ajax随机列表
	 */
	@SuppressWarnings("unchecked")
	public void randomList(){
		//采用Db Record
		if(getSessionAttr("blogRandomList")==null)
			setSessionAttr("blogRandomList", Db.find("SELECT b.*,f1.src AS src,f2.`src` AS userSrc,u.nickName FROM files f1,files f2,blog b,user u WHERE  b.`photo`=f1.`id` AND f2.`id`=u.photo and u.id=b.`userId` AND b.`del`='false' GROUP BY b.`id` limit 0,100"));
		setAttr("blogs", DemoUtil.blogCode((List<Record>)getSessionAttr("blogRandomList"),new String[]{"random"}));
		renderJson();
	}
	/**
	 * 根据月份加载列表
	 */
	public void mothlyList(){
		String mothly = getPara("mothly");
		//采用Db Record
		List<Record> blogList = Db.find("SELECT b.*,f1.src,f2.src AS userSrc,u.nickName  FROM blog b,files f1,files f2,`user` u "
				+"WHERE b.`del`='false'"
				+"AND DATE_FORMAT(b.`created`,'%Y年%m月')='"+mothly+"'"
				+"AND f1.`id`=b.`photo`"
				+"AND u.id=b.`userId`"
				+"AND f2.`id`=u.`photo`"
				+"ORDER BY b.`created` DESC "
				+"LIMIT "+(getPara("page")==null?"0,5":PageBean.creat(getPara("page"), "5").getStart()+",5"));
		String[] str =  DemoUtil.blogCode(blogList,new String[]{"mothly"});
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null)
		{
			setAttr("typeName", mothly);
			//类别传导到界面
			setAttr("mothly",mothly);
			setAttr("banner", false);
			setAttr("contentBox", false);
			setAttr("mainPage", "/fore/blog/typeList.jsp");
			render("/index.jsp");
		}
		else{
			renderJson();
		}
	}
	/**
	 * 根据专题加载列表
	 */
	public void tagList(){
		Long biaoqian = getParaToLong("biaoqian");
		//采用Db Record
		List<Record> blogList = Db.find("SELECT b.*,f1.src,f2.src AS userSrc,u.nickName  FROM blog b,files f1,files f2,`user` u,mapping m "
				+" WHERE b.`del`='false'"
				+" AND b.id=m.`fatherId` "
				+" AND m.`mapping_type`='"+Options.BLOG_BIAOQIAN+"' "
				+" AND m.`sonId`="+biaoqian
				+" AND f1.`id`=b.`photo`"
				+" AND u.id=b.`userId`"
				+" AND f2.`id`=u.`photo`"
				+" ORDER BY b.`created` DESC "
				+"LIMIT "+(getPara("page")==null?"0,5":PageBean.creat(getPara("page"), "5").getStart()+",5"));
		String[] str =  DemoUtil.blogCode(blogList,new String[]{"biaoqian"});
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null)
		{
			String biaoqianName = getPara("biaoqianName");
			setAttr("typeName", biaoqianName);
			//类别传导到界面
			setAttr("biaoqian",biaoqian);
			setAttr("banner", false);
			setAttr("contentBox", false);
			setAttr("mainPage", "/fore/blog/typeList.jsp");
			render("/index.jsp");
		}
		else{
			renderJson();
		}
	}
	/**
	 * 根据专题加载列表
	 */
	public void topicList(){
		Long zhuanti = getParaToLong("zhuanti");
		//采用Db Record
		List<Record> blogList = Db.find("SELECT b.*,f1.src,f2.src AS userSrc,u.nickName  FROM blog b,files f1,files f2,`user` u,mapping m "
				+" WHERE b.`del`='false'"
				+" AND b.id=m.`fatherId` "
				+" AND m.`mapping_type`='"+Options.BLOG_ZHUANTI+"' "
				+" AND m.`sonId`="+zhuanti
				+" AND f1.`id`=b.`photo`"
				+" AND u.id=b.`userId`"
				+" AND f2.`id`=u.`photo`"
				+" ORDER BY b.`created` DESC "
				+"LIMIT "+(getPara("page")==null?"0,5":PageBean.creat(getPara("page"), "5").getStart()+",5"));
		String[] str =  DemoUtil.blogCode(blogList,new String[]{"zhuanti"});
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null)
		{
			String topicName = getPara("topicName");
			setAttr("typeName", topicName);
			//类别传导到界面
			setAttr("zhuanti",zhuanti);
			setAttr("banner", false);
			setAttr("contentBox", false);
			setAttr("mainPage", "/fore/blog/typeList.jsp");
			render("/index.jsp");
		}
		else{
			renderJson();
		}
	}
	/**
	 * 按类别查询ajax列表
	 */
	public void typeList(){
		Long typeId = getParaToLong()==null?getParaToLong("typeId"):getParaToLong();
		//采用Db Record
		List<Record> blogList = Db.find("SELECT b.*,f1.src,f2.src AS userSrc,u.nickName  FROM blog b,files f1,files f2,`user` u "
				+"WHERE b.`del`='false'"
				+"AND b.`id`IN(SELECT fatherId FROM mapping WHERE sonId="+typeId+" AND mapping_type='"+Options.BLOG_TYPE+"')"
				+"AND f1.`id`=b.`photo`"
				+"AND u.id=b.`userId`"
				+"AND f2.`id`=u.`photo`"
				+"ORDER BY b.`created` DESC "
				+"LIMIT "+(getPara("page")==null?"0,5":PageBean.creat(getPara("page"), "5").getStart()+",5"));
		String[] str =  DemoUtil.blogCode(blogList,null);
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null)
		{
			StringBuffer typeName = new StringBuffer();
			Type t= Type.dao.findById(typeId);
			//查找是否有父映射
			if(t.hasFa()) {
				Type t_f = Type.dao.findById(t.getTypeType());
				typeName.append(t_f.getName()+"--");
			}
			typeName.append(t.getName());
			setAttr("typeName", typeName.toString());
			//类别传导到界面
			setAttr("typeId",typeId);
			setAttr("banner", false);
			setAttr("contentBox", false);
			setAttr("mainPage", "/fore/blog/typeList.jsp");
			render("/index.jsp");
		}
		else{
			renderJson();
		}
	}
	/**
	 * 获取博客具体项目
	 */
	public void getDetails(){
		Long blogId = getParaToLong();
		//更新博客点击
		blogService.updateThings(blogId,"click");
		//采用Db Record
		Record blog = Db.findFirst("SELECT b.*,f1.src,f2.src AS userSrc,u.nickName  FROM blog b,files f1,files f2,`user` u "
				+"WHERE b.`del`='false'"
				+"AND b.`id`="+blogId
				+" AND f1.`id`=b.`photo`"
				+"AND u.id=b.`userId`"
				+"AND f2.`id`=u.`photo`");
		setAttr("blog", blog);
		//获取上一篇和下一篇列表
		Blog blogPre = Blog.dao.findFirst("SELECT * FROM blog WHERE del='false' AND id<"+blogId+" ORDER BY created desc");
		Blog blogNex = Blog.dao.findFirst("SELECT * FROM blog WHERE del='false' AND id>"+blogId+" ORDER BY created");
		setAttr("blogPre", blogPre);
		setAttr("blogNex", blogNex);
		//加载评论列表
		List<Record> comments = Db.find("SELECT * FROM COMMENT WHERE del='false' AND blogId="+blogId+" ORDER BY created DESC limit 0,10");
		if(comments.size() > 10)
			setAttr("comments", DemoUtil.getComments(comments).append("<button class='btn btn-block btn-alt' onclick='loadAll();' id='more' >点击加载全部</button>").toString());
		else
			setAttr("comments", DemoUtil.getComments(comments).toString());
		//加载标签
		List<Record> tags = Db.find("SELECT * FROM TYPE WHERE id IN (SELECT sonId FROM mapping WHERE fatherId="+blogId+" AND mapping_type='b2b') AND del='false'");
		setAttr("tags", tags);
		
		setAttr("banner", false);
		setAttr("contentBox", false);
		setAttr("mainPage", "/fore/blog/detail.jsp");
		render("/index.jsp");
		//更新session数据
		getSessionAttr();
	}
	/**
	 * 赞一下或者踩一脚
	 */
	public void uOl(){
		Long blogId = getParaToLong("blogId");
		boolean flag = false;
		//更新
		if("up".equals(getPara("uOl")))
			flag=blogService.updateThings(blogId,"up");
		else
			flag=blogService.updateThings(blogId,"low");
		if(flag)
			setAttr("success", true);
		else
			setAttr("success", false);
		renderJson();
		//更新session数据
		getSessionAttr();
	}
	/**
	 * 关键字查询
	 */
	public void getQList(){
		String q = getPara("q");
		if(StrKit.isBlank(q))
			q = "空白 空格";
		LunceUtil lunceUtil = new LunceUtil();
		List<Record> blogs = lunceUtil.queryBlog(q,PageBean.creat((getPara("page")==null?"1":getPara("page")), "5"));
		System.out.println(blogs.size());
		String[] str = DemoUtil.blogCode(blogs,new String[]{"q"});
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null){
			setAttr("q", q);
			setAttr("typeName", "搜索:<font style='color:red;'>"+q+"</font>");
			//类别传导到界面
			setAttr("banner", false);
			setAttr("contentBox", false);
			setAttr("mainPage", "/fore/blog/typeList.jsp");
			render("/index.jsp");
		}
		else{
			renderJson();
		}
	}
	/**
	 * 更新session数据
	 */
	public void getSessionAttr(){
		//采用Db Record
		if(getSessionAttr("blogRandomList")!=null)
			setSessionAttr("blogRandomList", Db.find("SELECT b.*,f1.src AS src,f2.`src` AS userSrc,u.nickName FROM files f1,files f2,blog b,user u WHERE  b.`photo`=f1.`id` AND f2.`id`=u.photo and u.id=b.`userId` AND b.`del`='false' GROUP BY b.`id` limit 1,100"));
		//更新session中的图片
 		if(getSessionAttr("photoWall")!=null){
 			List<Files> photoWall = Files.dao.find("select * from files where type like '%image%' and del=false order by created desc limit 0,12");
			setSessionAttr("photoWall", photoWall);
 		}
		//更新session中的统计
 		if(getSessionAttr("data_map")!=null){
 			setSessionAttr("data_map", DataMapUtil.updateDataMap());
 		}
 		//重载首页动图
 		if(getSessionAttr("bannerImg")!=null){
 			List<Files> bannerImg  = Files.dao.find("select * from files where id in (select photo from blog where del='false') order by created desc limit 0,3");
			setSessionAttr("bannerImg", bannerImg);
 		}
 		//重载载4条最新博客
		if(getSessionAttr("fourNewBlog")!=null){
			//Db Record 模式查询
			List<Record> fourNewBlog  = Db.find("SELECT u.nickName,f.src,b.* FROM blog b,files f,user u WHERE b.del='false' AND b.`photo`=f.`id` and b.userId=u.id ORDER BY b.created DESC LIMIT 0,4");
			setSessionAttr("fourNewBlog", fourNewBlog);
		}
	}
	
}
