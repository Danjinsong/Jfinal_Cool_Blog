package com.djsblog.foreground.controller;

import java.util.List;

import com.djsblog.entity.Datadic;
import com.djsblog.entity.Files;
import com.djsblog.entity.PageBean;
import com.djsblog.entity.User;
import com.djsblog.enums.Options;
import com.djsblog.service.DataDicService;
import com.djsblog.service.impl.DataDicServiceImpl;
import com.djsblog.service.impl.UserServiceImpl;
import com.djsblog.util.DemoUtil;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * 前台页面控制器
 * @author 但劲松
 *
 */
public class ForegroundController extends Controller {

	/**
	 * 缺省页
	 */
	public void index(){
		setAttr("banner", true);
		setAttr("contentBox", true);
		setAttr("mainPage", "/fore/blog/list.jsp");
		render("/index.jsp");
	}
	/**
	 * 用户更换皮肤方法
	 */
	public void skin(){
		User user = getSessionAttr("currentUser");
		if(user != null){
			String skin = getPara("skin");
			Datadic datadic = new Datadic();
			datadic.setKey(user.getId()+"_SKIN");
			datadic.setVal(skin);
			DataDicService dataDicService = new DataDicServiceImpl();
			dataDicService.save(datadic);
			setSessionAttr("skin", skin);
		}
		renderJson();
	}
	/**
	 * 观光某人的空间  
	 */
	public void heCenter(){
		Long heId = getParaToLong(); 
		if(heId != null){
			User he = User.dao.findById(heId);
			//加载我的博客列表
			PageBean pageBean = null;
			if(getPara("page")!=null)
				pageBean = PageBean.creat(getPara("page"), "5");
			else
				pageBean = PageBean.creat("1", "5");
			List<Record> heBlogs = Db.find("SELECT b.*,f1.src AS src,f2.`src` AS userSrc,u.nickName FROM files f1,files f2,blog b,USER u WHERE  b.`photo`=f1.`id` AND f2.`id`=u.photo AND u.id=b.`userId` AND b.`del`='false' AND b.userId="+he.getId()+" ORDER BY b.created DESC "
					+ "LIMIT "+pageBean.getStart()+",5");
			String str[] = DemoUtil.blogCode(heBlogs,new String[]{"he"});
			setAttr("blogs", str[0]);
			setAttr("foot", str[1]);
			if(getPara("page")==null){
				//图片
				setAttr("heFiles",Files.dao.findById(he.getPhoto()));
				setAttr("he", he);
				setAttr("heId", heId);
				setAttr("banner", false);
				setAttr("contentBox", false);
				//用户中心确定博客列表样式
				setAttr("userCenter",true);
				setAttr("mainPage", "/fore/he/heCenter.jsp");
				render("/index.jsp");
			}else{
				renderJson();
			}
		}
		
	}
	/**
	 *  自己的管理中心
	 */
	public void userCenter(){
		//加载我的博客列表
		PageBean pageBean = null;
		if(getPara("page")!=null)
			pageBean = PageBean.creat(getPara("page"), "5");
		else
			pageBean = PageBean.creat("1", "5");
		List<Record> myBlogs = Db.find("SELECT b.*,f1.src AS src,f2.`src` AS userSrc,u.nickName FROM files f1,files f2,blog b,USER u WHERE  b.`photo`=f1.`id` AND f2.`id`=u.photo AND u.id=b.`userId` AND b.`del`='false' AND b.userId="+((User)getSessionAttr("currentUser")).getId()+" ORDER BY b.created DESC "
				+ "LIMIT "+pageBean.getStart()+",5");
		String str[] = DemoUtil.blogCode(myBlogs,new String[]{"my"});
		setAttr("blogs", str[0]);
		setAttr("foot", str[1]);
		if(getPara("page")==null){
			//图片
			setAttr("userFile",Files.dao.findById(((User)getSessionAttr("currentUser")).getPhoto()));
			setAttr("banner", false);
			setAttr("contentBox", false);
			//用户中心确定博客列表样式
			setAttr("userCenter",true);
			setAttr("mainPage", "/fore/user/userCenter.jsp");
			render("/index.jsp");
		}else{
			renderJson();
		}
	}
	/**
	 * 关注某人
	 */
	public void foucus(){
		Long currentUserId = getParaToLong("currentUserId");
		Long id = getParaToLong("id");
		if(new UserServiceImpl().foucus(currentUserId, id)){
			setAttr("success", true);
		}else{
			setAttr("success", false);
		}
		renderJson();
	}
	/**
	 * 加载关注列表
	 */
	public void foucusList(){
		Long id = getParaToLong("id");
		String page = getPara("page");
		PageBean pageBean = PageBean.creat(page, "10");
		List<Record> foucusList = Db.find("SELECT u.*,f.src AS userSrc FROM USER u,files f,mapping m "
				+ "	WHERE u.`id`=m.`sonId`"
				+ " AND f.`id`=u.`photo`"
				+ " AND m.`mapping_type`='"+Options.USER_FOUCUS+"'"
				+ " AND m.`fatherId`="+id
				+ " AND u.`del`='false'"
				+ " limit "+pageBean.getStart()+","+pageBean.getPageSize());
		long total = Db.queryLong("SELECT COUNT(*) FROM mapping WHERE fatherId = "+id+" AND mapping_type='"+Options.USER_FOUCUS+"' AND del='false'");
		String str[] = DemoUtil.getFoucus(foucusList, pageBean,total);
		setAttr("body", str[0]);
		setAttr("foot", str[1]);
		renderJson();
	}
	/**
	 * 登录页
	 */
	public void login(){
		
		render("/login.jsp");
	}
}
